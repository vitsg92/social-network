package com.getjavajob.training.socialnetwork.service.validate.account;

import com.getjavajob.training.socialnetwork.common.Account;

public interface AccountCheck {
    String[] check(Account account);
}
