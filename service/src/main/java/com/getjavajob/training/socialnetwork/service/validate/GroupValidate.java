package com.getjavajob.training.socialnetwork.service.validate;

import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.service.validate.group.GroupCheck;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupValidate {
    private List<GroupCheck> listGroupCheck;

    public GroupValidate(List<GroupCheck> listGroupCheck) {
        this.listGroupCheck = listGroupCheck;
    }

    public Map<String, String> validate(Group group) {
        Map<String, String> errors = new HashMap<>();
        String[] error;
        for (GroupCheck gc : listGroupCheck) {
            error = gc.check(group);
            if (error != null) {
                errors.put(error[0], error[1]);
            }
        }
        return errors;
    }
}
