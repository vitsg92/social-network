package com.getjavajob.training.socialnetwork.service.validate.group;

import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.dao.GroupDao;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class CheckGroupName implements GroupCheck {
    private static final String NAME_ERROR_GROUP = "errorGroupName";

    private final GroupDao groupDao;

    public CheckGroupName(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Override
    public String[] check(Group checkedGroup) {
        String groupName = checkedGroup.getGroupName();
        Group foundGroup = groupDao.findByGroupName(groupName);
        boolean searchResult = nonNull(foundGroup);
        int checkedGroupId = checkedGroup.getId();
        String[] errorGroupName = null;
        if (groupName.isEmpty()) {
            errorGroupName = new String[]{NAME_ERROR_GROUP, "Введите имя группы"};
        } else if (isNull(checkedGroupId) ? searchResult : searchResult && foundGroup.getId() != checkedGroupId) {
            errorGroupName = new String[]{NAME_ERROR_GROUP, "Такое имя группы уже существует"};
        }
        return errorGroupName;
    }
}
