package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRole;
import com.getjavajob.training.socialnetwork.common.Phone;
import com.getjavajob.training.socialnetwork.dao.*;
import com.getjavajob.training.socialnetwork.service.validate.AccountValidate;
import com.getjavajob.training.socialnetwork.service.validate.account.*;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class AccountService {
    private final BCryptPasswordEncoder passwordEncoder;
    private final MessageOnWallDao messageOnWallDao;
    private final GroupDao groupDao;
    private final FriendRelationDao friendRelationDao;
    private final GroupRelationDao groupRelationDao;
    private AccountDao accountDao;
    private PrivateMessageDao privateMessageDao;

    @Autowired
    public AccountService(AccountDao accountDao, PrivateMessageDao privateMessageDao, BCryptPasswordEncoder passwordEncoder,
                          MessageOnWallDao messageOnWallDao, GroupDao groupDao, FriendRelationDao friendRelationDao,
                          GroupRelationDao groupRelationDao) {
        this.accountDao = accountDao;
        this.privateMessageDao = privateMessageDao;
        this.passwordEncoder = passwordEncoder;
        this.messageOnWallDao = messageOnWallDao;
        this.groupDao = groupDao;
        this.friendRelationDao = friendRelationDao;
        this.groupRelationDao = groupRelationDao;
    }

    @Transactional
    public Account getAccountById(int id) {
        Account account = accountDao.findById(id);
        initAccountFields(account);
        return account;
    }

    @Transactional
    public Account createAccount(Account account) {
        addOwnerAndDeleteNullPhones(account);
        return accountDao.save(account);
    }

    @Transactional
    public Account authorization(String email) {
        Account account = accountDao.findByEmail(email);
        if (isNull(account)) {
            throw new UsernameNotFoundException("Пользователь с таким email не найден");
        }
        account.setUnacceptedMessages(privateMessageDao.getByReaderIdAndAccepted(account.getId(), false));
        initAccountFields(account);
        return account;
    }

    @Transactional
    public Account updateAccount(Account account) {
        addOwnerAndDeleteNullPhones(account);
        return accountDao.save(account);
    }

    @Transactional
    public List<Account> searchAccountByString(String searchName) {
        return accountDao.searchAccountsByString(searchName);
    }

    @Transactional
    public List<Account> searchAccountByStringPagination(String searchName, int pageNumber, int pageSize) {
        return accountDao.searchAccountsByStringPagination(searchName, PageRequest.of(pageNumber - 1, pageSize, Sort.by("firstName")));
    }

    @Transactional
    public int getCountAccountsWithSearchByString(String searchName) {
        return accountDao.getCountAccountsWithSearchByString(searchName);
    }

    @Transactional
    public byte[] getAccountPhoto(int id) {
        return accountDao.getAccountPhoto(id);
    }

    @Transactional
    public int removeAccount(int id) {
        privateMessageDao.removeOnAccountId(id);
        messageOnWallDao.removeOnAccountId(id);
        groupRelationDao.removeAllByAccountId(id);
        friendRelationDao.removeOnAccountId(id);
        groupDao.removeAllByCreatorId(id);
        accountDao.removeById(id);
        return id;
    }

    @Transactional
    public AccountRole makeAdmin(int id) {
        Account account = accountDao.findById(id);
        AccountRole accountRole = AccountRole.ADMIN;
        account.setRole(accountRole);
        return accountRole;
    }

    public void setMissingFieldsIntoUpdatedAccount(Account updatedAccount, Account account) {
        String password = updatedAccount.getPassword();
        if (isNull(password) || password.isEmpty()) {
            updatedAccount.setPassword(account.getPassword());
        } else {
            updatedAccount.setPassword(passwordEncoder.encode(password));
        }
        byte[] photo = updatedAccount.getPhoto();
        if (isNull(photo) || photo.length == 0) {
            updatedAccount.setPhoto(account.getPhoto());
        }
        updatedAccount.setDateRegistration(account.getDateRegistration());
        updatedAccount.setRole(account.getRole());
    }

    private void initAccountFields(Account account) {
        if (nonNull(account)) {
            Hibernate.initialize(account.getPhones());
        }
    }


    private void addOwnerAndDeleteNullPhones(Account account) {
        List<Phone> phones = account.getPhones();
        Iterator<Phone> iterator = phones.iterator();
        Phone p;
        while (iterator.hasNext()) {
            p = iterator.next();
            if (isNull(p.getPhoneNumber()) || p.getPhoneNumber().isEmpty()) {
                iterator.remove();
            } else {
                p.setOwner(account);
            }
        }
    }

    public Map<String, String> registrationAccountValidate(Account account) {
        List<AccountCheck> checks = new ArrayList<>();
        checks.add(new CheckFirstName());
        checks.add(new CheckLastName());
        checks.add(new CheckEmail(accountDao));
        checks.add(new CheckPassword());
        AccountValidate accountValidate = new AccountValidate(checks);
        return accountValidate.validate(account);
    }

    public Map<String, String> updateAccountValidate(Account account) {
        List<AccountCheck> checks = new ArrayList<>();
        checks.add(new CheckFirstName());
        checks.add(new CheckLastName());
        checks.add(new CheckEmail(accountDao));
        AccountValidate accountValidate = new AccountValidate(checks);
        return accountValidate.validate(account);
    }
}
