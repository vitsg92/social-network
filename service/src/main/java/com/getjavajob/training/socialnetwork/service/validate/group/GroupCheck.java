package com.getjavajob.training.socialnetwork.service.validate.group;

import com.getjavajob.training.socialnetwork.common.Group;

public interface GroupCheck {
    String[] check(Group group);
}
