package com.getjavajob.training.socialnetwork.service.validate.account;

import com.getjavajob.training.socialnetwork.common.Account;

public class CheckFirstName implements AccountCheck {
    @Override
    public String[] check(Account account) {
        String[] errorFirstName = null;
        if (account.getFirstName().isEmpty()) {
            errorFirstName = new String[]{"errorFirstName", "Введите имя"};
        }
        return errorFirstName;
    }
}
