package com.getjavajob.training.socialnetwork.service.validate;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.service.validate.account.AccountCheck;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountValidate {
    private List<AccountCheck> listAccountCheck;

    public AccountValidate(List<AccountCheck> listAccountCheck) {
        this.listAccountCheck = listAccountCheck;
    }

    public Map<String, String> validate(Account account) {
        Map<String, String> errors = new HashMap<>();
        String[] error;
        for (AccountCheck ac : listAccountCheck) {
            error = ac.check(account);
            if (error != null) {
                errors.put(error[0], error[1]);
            }
        }
        return errors;
    }
}
