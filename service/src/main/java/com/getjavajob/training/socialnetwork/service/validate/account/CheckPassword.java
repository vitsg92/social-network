package com.getjavajob.training.socialnetwork.service.validate.account;

import com.getjavajob.training.socialnetwork.common.Account;

public class CheckPassword implements AccountCheck {
    @Override
    public String[] check(Account account) {
        String[] errorPassword = null;
        if (account.getPassword().isEmpty()) {
            errorPassword = new String[]{"errorPassword", "Введите пароль"};
        }
        return errorPassword;
    }
}
