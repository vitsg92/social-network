package com.getjavajob.training.socialnetwork.service.validate.account;

import com.getjavajob.training.socialnetwork.common.Account;

public class CheckLastName implements AccountCheck {
    @Override
    public String[] check(Account account) {
        String[] errorLastName = null;
        if (account.getLastName().isEmpty()) {
            errorLastName = new String[]{"errorLastName", "Введите фамилию"};
        }
        return errorLastName;
    }
}
