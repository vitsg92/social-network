package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.dto.Dialog;
import com.getjavajob.training.socialnetwork.common.dto.UserDialogDetail;
import com.getjavajob.training.socialnetwork.common.message.Message;
import com.getjavajob.training.socialnetwork.common.message.MessageOnWall;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.MessageOnWallDao;
import com.getjavajob.training.socialnetwork.dao.PrivateMessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.socialnetwork.service.utils.Util.START_PAGE;

@Service
public class MessageService {
    private static final int PAGE_SIZE = 20;

    private final PrivateMessageDao privateMessageDao;
    private final MessageOnWallDao messageOnWallDao;
    private final AccountDao accountDao;
    private Pageable page;

    @Autowired
    public MessageService(PrivateMessageDao privateMessageDao, MessageOnWallDao messageOnWallDao, AccountDao accountDao) {
        this.privateMessageDao = privateMessageDao;
        this.messageOnWallDao = messageOnWallDao;
        this.accountDao = accountDao;
        page = PageRequest.of(START_PAGE, PAGE_SIZE, Sort.by(Sort.Direction.DESC, "id"));
    }

    @Transactional
    public List<PrivateMessage> getMessagesFromDialog(int mainAccountId, int companionId) {
        return privateMessageDao.getMessagesFromDialog(mainAccountId, companionId, PAGE_SIZE);
    }

    @Transactional
    public List<PrivateMessage> getMessagesFromDialog(int mainAccountId, int companionId, int lastElementId) {
        return privateMessageDao.getMessagesFromDialog(mainAccountId, companionId, PAGE_SIZE, lastElementId);
    }

    @Transactional
    public Message save(Message message) {
        if (message instanceof PrivateMessage) {
            return privateMessageDao.save((PrivateMessage) message);
        } else {
            return messageOnWallDao.save((MessageOnWall) message);
        }
    }

    @Transactional
    public PrivateMessage acceptMessage(int id) {
        PrivateMessage message = privateMessageDao.findById(id);
        message.setAccepted(true);
        return message;
    }

    @Transactional
    public void acceptMessages(List<Integer> acceptedMessagesId) {
        privateMessageDao.updateAcceptedMessages(acceptedMessagesId);
    }

    @Transactional
    public void loadUnacceptedMessages(Account mainAccount) {
        mainAccount.setUnacceptedMessages(privateMessageDao.getByReaderIdAndAccepted(mainAccount.getId(), false));
    }

    @Transactional
    public void removeMessagesFromUnaccepted(Account mainAccount, int companionId) {
        List<Integer> acceptedMessages = new ArrayList<>();
        if (mainAccount.getUnacceptedMessages().removeIf(message -> {
            if (message.getWriter().getId() == companionId) {
                acceptedMessages.add(message.getId());
                return true;
            } else {
                return false;
            }
        })) {
            acceptMessages(acceptedMessages);
        }
    }

    @Transactional
    public List<UserDialogDetail> getUserDialogDetails(Account mainAccount) {
        return getUserDialogDetailFromDialog(privateMessageDao.getDialogs(mainAccount.getId(), PAGE_SIZE), mainAccount);
    }

    @Transactional
    public List<UserDialogDetail> getUserDialogDetails(Account mainAccount, int lastElementId) {
        return getUserDialogDetailFromDialog(privateMessageDao.getDialogs(mainAccount.getId(), PAGE_SIZE,
                lastElementId), mainAccount);
    }

    @Transactional
    public List<MessageOnWall> getMessageOnWall(int id, MessageType messageType) {
        return messageOnWallDao.getByDestinationIdAndMessageType(id, messageType, page);
    }

    @Transactional
    public List<MessageOnWall> getMessageOnWall(int id, MessageType messageType, int lastElementId) {
        return messageOnWallDao.getByDestinationIdAndMessageTypeAndIdLessThan(id, messageType, lastElementId, page);
    }

    private List<UserDialogDetail> getUserDialogDetailFromDialog(List<Dialog> dialogs, Account mainAccount) {
        List<UserDialogDetail> detailDialogs = new ArrayList<>();
        int mainAccountId = mainAccount.getId();
        for (Dialog dialog : dialogs) {
            UserDialogDetail userDialogDetail = new UserDialogDetail();
            userDialogDetail.setMainAccount(mainAccount);
            int companionId = dialog.getAccountId();
            Account companion = accountDao.findById(companionId);
            userDialogDetail.setCompanion(companion);
            userDialogDetail.setLastMessageInDialog((privateMessageDao.findById(dialog.getLastMessageId())));
            userDialogDetail.setCountUnacceptedMessages(privateMessageDao.getCountUnacceptedMessagesOnDialog(mainAccountId, companionId));
            detailDialogs.add(userDialogDetail);
        }
        return detailDialogs;
    }
}
