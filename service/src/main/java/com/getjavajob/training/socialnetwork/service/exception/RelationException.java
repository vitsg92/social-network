package com.getjavajob.training.socialnetwork.service.exception;

public class RelationException extends RuntimeException {

    public RelationException(String message) {
        super(message);
    }
}
