package com.getjavajob.training.socialnetwork.service.validate.account;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.dao.AccountDao;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class CheckEmail implements AccountCheck {
    private static final String NAME_ERROR_EMAIL = "errorEmail";

    private final AccountDao accountDao;

    public CheckEmail(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public String[] check(Account checkedAccount) {
        String email = checkedAccount.getEmail();
        int checkedAccountId = checkedAccount.getId();
        Account foundAccount = accountDao.findByEmail(email);
        boolean searchResult = nonNull(foundAccount);
        String[] errorEmail = null;
        if (email.isEmpty()) {
            errorEmail = new String[]{NAME_ERROR_EMAIL, "Введите email"};
        } else if (isNull(checkedAccountId) ? searchResult : searchResult && foundAccount.getId() != checkedAccountId) {
            errorEmail = new String[]{NAME_ERROR_EMAIL, "Такой email уже существует"};
        }
        return errorEmail;
    }
}
