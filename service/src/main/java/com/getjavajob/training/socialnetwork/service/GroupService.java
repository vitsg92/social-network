package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import com.getjavajob.training.socialnetwork.common.ralation.GroupPrivilege;
import com.getjavajob.training.socialnetwork.common.ralation.GroupRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.GroupDao;
import com.getjavajob.training.socialnetwork.dao.GroupRelationDao;
import com.getjavajob.training.socialnetwork.dao.MessageOnWallDao;
import com.getjavajob.training.socialnetwork.service.exception.RelationException;
import com.getjavajob.training.socialnetwork.service.validate.GroupValidate;
import com.getjavajob.training.socialnetwork.service.validate.group.CheckGroupName;
import com.getjavajob.training.socialnetwork.service.validate.group.GroupCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.getjavajob.training.socialnetwork.service.utils.Util.START_PAGE;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class GroupService {
    private static final String USER_NOT_IN_GROUP_MESSAGE = "Пользователь не в группе";
    private static final int PAGE_SIZE = 30;

    private final GroupRelationDao groupRelationDao;
    private final MessageOnWallDao messageOnWallDao;
    private final AccountDao accountDao;
    private GroupDao groupDao;
    private Pageable page;

    @Autowired
    public GroupService(GroupDao groupDao, GroupRelationDao groupRelationDao, MessageOnWallDao messageOnWallDao, AccountDao accountDao) {
        this.groupDao = groupDao;
        this.groupRelationDao = groupRelationDao;
        this.messageOnWallDao = messageOnWallDao;
        this.accountDao = accountDao;
        page = PageRequest.of(START_PAGE, PAGE_SIZE, Sort.by(Sort.Direction.DESC, "id"));
    }

    @Transactional
    public Group addGroup(Account mainAccount, Group group) {
        Group createdGroup = groupDao.save(group);
        GroupRelation groupRelation = new GroupRelation();
        groupRelation.setAccount(mainAccount);
        groupRelation.setGroup(group);
        groupRelation.setRelationStatus(RelationStatus.FULL);
        groupRelation.setGroupPrivilege(GroupPrivilege.MODER);
        groupRelationDao.save(groupRelation);
        return createdGroup;
    }

    @Transactional
    public Group updateGroup(Group group) {
        return groupDao.save(group);
    }

    @Transactional
    public Group getGroupById(int id) {
        return groupDao.findById(id);
    }

    @Transactional
    public List<GroupRelation> getAccountGroups(int accountId, RelationStatus relationStatus) {
        return groupRelationDao.getAccountGroupRelations(accountId, relationStatus, page);
    }

    @Transactional
    public List<GroupRelation> getAccountGroups(int accountId, RelationStatus relationStatus, int lastElementId) {
        return groupRelationDao.getAccountGroupRelations(accountId, relationStatus, lastElementId, page);
    }

    @Transactional
    public List<GroupRelation> getAccountGroupsRequests(int accountId, RelationStatus relationStatus) {
        return groupRelationDao.getAccountGroupRelations(accountId, relationStatus, page);
    }

    @Transactional
    public List<GroupRelation> getAccountGroupsRequests(int accountId, RelationStatus relationStatus, int lastElementId) {
        return groupRelationDao.getAccountGroupRelations(accountId, relationStatus, lastElementId, page);
    }

    @Transactional
    public List<Group> getCreatedGroups(int accountId) {
        return groupDao.findByCreatorId(accountId, page);
    }

    @Transactional
    public List<Group> getCreatedGroups(int accountId, int lastElementId) {
        return groupDao.findByCreatorIdAndIdLessThan(accountId, lastElementId, page);
    }

    @Transactional
    public int getCountGroupsWithSearchByString(String searchName) {
        return groupDao.countByGroupNameIgnoreCaseContaining(searchName);
    }

    @Transactional
    public List<Group> searchGroupByStringPagination(String searchName, int pageNumber, int pageSize) {
        return groupDao.findByGroupNameContainingIgnoreCase(searchName, PageRequest.of(pageNumber - 1, pageSize, Sort.by("id")));
    }

    @Transactional
    public List<Group> searchGroupByString(String searchName) {
        return groupDao.findByGroupNameContainingIgnoreCase(searchName);
    }

    @Transactional
    public byte[] getGroupPhoto(int id) {
        return groupDao.getGroupPhoto(id);
    }

    public Map<String, String> groupValidate(Group group) {
        List<GroupCheck> checks = new ArrayList<>();
        checks.add(new CheckGroupName(groupDao));
        GroupValidate accountValidate = new GroupValidate(checks);
        return accountValidate.validate(group);
    }

    @Transactional
    public int removeGroup(int groupId) {
        groupRelationDao.removeAllByGroupId(groupId);
        messageOnWallDao.removeAllByDestinationIdAndMessageType(groupId, MessageType.GROUP_WALL);
        groupDao.removeById(groupId);
        return groupId;
    }

    @Transactional
    public GroupRelation getGroupRelation(int mainAccountId, int groupId) {
        GroupRelation groupRelation = groupRelationDao.getGroupRelation(mainAccountId, groupId);
        if (isNull(groupRelation)) {
            return new GroupRelation(accountDao.findById(mainAccountId), groupDao.findById(groupId), RelationStatus.EMPTY, GroupPrivilege.USER);
        } else {
            return groupRelation;
        }
    }

    @Transactional
    public RelationStatus joinOnGroup(int mainAccountId, int groupId) {
        if (isNull(groupRelationDao.getGroupRelation(mainAccountId, groupId))) {
            return groupRelationDao.save(new GroupRelation(accountDao.findById(mainAccountId), groupDao.findById(groupId),
                    RelationStatus.REQUEST_OUT, GroupPrivilege.USER)).getRelationStatus();
        } else {
            throw new RelationException("Заявка в группу уже существует");
        }
    }

    @Transactional
    public RelationStatus outOfGroup(int mainAccount, int groupId) {
        Group group = groupDao.findById(groupId);
        if (group.getCreator().getId() == mainAccount) {
            throw new RelationException("Вы не можете выйти из созданной Вами группы");
        }
        GroupRelation groupRelation = groupRelationDao.getGroupRelation(mainAccount, groupId);
        if (nonNull(groupRelation)) {
            groupRelationDao.remove(groupRelation);
            return RelationStatus.EMPTY;
        } else {
            throw new RelationException("Вы не участник группы");
        }
    }

    @Transactional
    public RelationStatus removeAccountFromGroup(int groupId, int removedAccountId) {
        Group group = groupDao.findById(groupId);
        if (group.getCreator().getId() == removedAccountId) {
            throw new RelationException("Нельзя удалить создателя группы");
        }
        GroupRelation groupRelationRemovedAccount = groupRelationDao.getGroupRelation(removedAccountId, groupId);
        if (nonNull(groupRelationRemovedAccount) && groupRelationRemovedAccount.getRelationStatus().equals(RelationStatus.FULL)) {
            groupRelationDao.remove(groupRelationRemovedAccount);
            return RelationStatus.EMPTY;
        } else {
            throw new RelationException(USER_NOT_IN_GROUP_MESSAGE);
        }
    }

    @Transactional
    public RelationStatus acceptAccountToGroup(int groupId, int acceptedAccountId) {
        GroupRelation groupRelationAcceptedAccount = groupRelationDao.getGroupRelation(acceptedAccountId, groupId);
        if (isNull(groupRelationAcceptedAccount)) {
            throw new RelationException("Заявки в группу не было");
        } else if (groupRelationAcceptedAccount.getRelationStatus().equals(RelationStatus.REQUEST_OUT)) {
            RelationStatus relationStatusAccepted = RelationStatus.FULL;
            groupRelationAcceptedAccount.setRelationStatus(relationStatusAccepted);
            return relationStatusAccepted;
        } else {
            throw new RelationException("Пользователь уже в группе");
        }
    }

    @Transactional
    public List<GroupRelation> getGroupMembers(int groupId) {
        return groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.FULL, page);
    }

    @Transactional
    public List<GroupRelation> getGroupMembers(int groupId, int lastElementId) {
        return groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.FULL, lastElementId, page);
    }

    @Transactional
    public List<GroupRelation> getJoinRequestAccounts(int groupId) {
        return groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.REQUEST_OUT, page);
    }

    @Transactional
    public List<GroupRelation> getJoinRequestAccounts(int groupId, int lastElementId) {
        return groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.REQUEST_OUT, lastElementId, page);
    }

    @Transactional
    public List<GroupRelation> getGroupMembersOnPrivilege(int groupId, GroupPrivilege groupPrivilege) {
        return groupRelationDao.getGroupMembersOnPrivilege(groupId, groupPrivilege, page);
    }

    @Transactional
    public List<GroupRelation> getGroupMembersOnPrivilege(int groupId, int lastElementId, GroupPrivilege groupPrivilege) {
        return groupRelationDao.getGroupMembersOnPrivilege(groupId, groupPrivilege, lastElementId, page);
    }

    @Transactional
    public GroupPrivilege addModerator(int groupId, int accountId) {
        GroupRelation groupRelationUpdated = groupRelationDao.getGroupRelation(accountId, groupId);
        if (isNull(groupRelationUpdated)) {
            throw new RelationException(USER_NOT_IN_GROUP_MESSAGE);
        } else if (groupRelationUpdated.getRelationStatus().equals(RelationStatus.FULL)) {
            if (groupRelationUpdated.getGroupPrivilege().equals(GroupPrivilege.USER)) {
                GroupPrivilege groupPrivilege = GroupPrivilege.MODER;
                groupRelationUpdated.setGroupPrivilege(groupPrivilege);
                return groupPrivilege;
            } else {
                throw new RelationException("Пользователь уже является модератором");
            }
        } else {
            throw new RelationException(USER_NOT_IN_GROUP_MESSAGE);
        }
    }

    @Transactional
    public GroupPrivilege removeModerator(int groupId, int accountId) {
        Group group = groupDao.findById(groupId);
        if (group.getCreator().getId() == accountId) {
            throw new RelationException("Нельзя удалить из модераторов создателя группы");
        }
        GroupRelation groupRelationUpdated = groupRelationDao.getGroupRelation(accountId, groupId);
        if (isNull(groupRelationUpdated)) {
            throw new RelationException(USER_NOT_IN_GROUP_MESSAGE);
        } else if (groupRelationUpdated.getRelationStatus().equals(RelationStatus.FULL)) {
            if (groupRelationUpdated.getGroupPrivilege().equals(GroupPrivilege.MODER)) {
                GroupPrivilege groupPrivilege = GroupPrivilege.USER;
                groupRelationUpdated.setGroupPrivilege(groupPrivilege);
                return groupPrivilege;
            } else {
                throw new RelationException("Пользователь не является модератором");
            }
        } else {
            throw new RelationException(USER_NOT_IN_GROUP_MESSAGE);
        }
    }

    public void setMissingFieldsIntoUpdatedGroup(Group updatedGroup, Group group) {
        updatedGroup.setCreator(group.getCreator());
        updatedGroup.setDateRegistration(group.getDateRegistration());
        byte[] photo = updatedGroup.getGroupPhoto();
        if (isNull(photo) || photo.length == 0) {
            updatedGroup.setGroupPhoto(group.getGroupPhoto());
        }
    }
}
