package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.ralation.FriendRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.FriendRelationDao;
import com.getjavajob.training.socialnetwork.service.exception.RelationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

import static com.getjavajob.training.socialnetwork.service.utils.Util.START_PAGE;
import static java.util.Objects.isNull;

@Service
public class FriendService {
    private static final int PAGE_SIZE = 30;

    private final FriendRelationDao friendRelationDao;
    private final AccountDao accountDao;
    private Pageable page;

    public FriendService(FriendRelationDao friendRelationDao, AccountDao accountDao) {
        this.friendRelationDao = friendRelationDao;
        this.accountDao = accountDao;
        page = PageRequest.of(START_PAGE, PAGE_SIZE, Sort.by(Sort.Direction.DESC, "id"));
    }

    @Transactional
    public RelationStatus getFriendRelationStatus(int mainAccountId, int companionId) {
        FriendRelation friendRelation = friendRelationDao.getFriendRelation(mainAccountId, companionId);
        if (isNull(friendRelation)) {
            return RelationStatus.EMPTY;
        } else {
            recoveryPositionAccountsId(Collections.singletonList(friendRelation), mainAccountId);
            return friendRelation.getRelationStatus();
        }
    }

    @Transactional
    public List<FriendRelation> getAccountFriends(int id) {
        List<FriendRelation> friends = friendRelationDao.getFriendRelationsFriend(id, page);
        recoveryPositionAccountsId(friends, id);
        return friends;
    }

    @Transactional
    public List<FriendRelation> getAccountFriends(int id, int lastElementId) {
        List<FriendRelation> friends = friendRelationDao.getFriendRelationsFriend(id, lastElementId, page);
        recoveryPositionAccountsId(friends, id);
        return friends;
    }

    @Transactional
    public List<FriendRelation> getRequestOutFriendshipAccounts(int id) {
        List<FriendRelation> requestOutFriendships = friendRelationDao.getFriendRelationsRequestOutFriendship(id, page);
        recoveryPositionAccountsId(requestOutFriendships, id);
        return requestOutFriendships;
    }

    @Transactional
    public List<FriendRelation> getRequestOutFriendshipAccounts(int id, int lastElementId) {
        List<FriendRelation> requestOutFriendships = friendRelationDao.getFriendRelationsRequestOutFriendship(id, lastElementId, page);
        recoveryPositionAccountsId(requestOutFriendships, id);
        return requestOutFriendships;
    }

    @Transactional
    public List<FriendRelation> getRequestInFriendshipAccounts(int id) {
        List<FriendRelation> requestInFriendships = friendRelationDao.getFriendRelationsRequestInFriendship(id, page);
        recoveryPositionAccountsId(requestInFriendships, id);
        return requestInFriendships;
    }

    @Transactional
    public List<FriendRelation> getRequestInFriendshipAccounts(int id, int lastElementId) {
        List<FriendRelation> requestInFriendships = friendRelationDao.getFriendRelationsRequestInFriendship(id, lastElementId, page);
        recoveryPositionAccountsId(requestInFriendships, id);
        return requestInFriendships;
    }

    @Transactional
    public RelationStatus addFriend(int mainAccountId, int companionId) {
        FriendRelation friendRelation = friendRelationDao.getFriendRelation(mainAccountId, companionId);
        RelationStatus relationStatusAfterAddedFriend;
        if (isNull(friendRelation)) {
            FriendRelation newFriendRelation = new FriendRelation();
            newFriendRelation.setAccount1(accountDao.findById(mainAccountId));
            newFriendRelation.setAccount2(accountDao.findById(companionId));
            relationStatusAfterAddedFriend = RelationStatus.REQUEST_OUT;
            newFriendRelation.setRelationStatus(relationStatusAfterAddedFriend);
            friendRelationDao.save(newFriendRelation);
        } else {
            recoveryPositionAccountsId(Collections.singletonList(friendRelation), mainAccountId);
            RelationStatus relationStatus = friendRelation.getRelationStatus();
            if (relationStatus == RelationStatus.REQUEST_IN) {
                relationStatusAfterAddedFriend = RelationStatus.FULL;
                friendRelation.setRelationStatus(relationStatusAfterAddedFriend);
            } else {
                throw new RelationException("Исходящий запрос на дружбу уже отправлен");
            }
        }
        return relationStatusAfterAddedFriend;
    }

    @Transactional
    public RelationStatus removeFriend(int mainAccountId, int companionId) {
        FriendRelation friendRelation = friendRelationDao.getFriendRelation(mainAccountId, companionId);
        if (isNull(friendRelation)) {
            throw new RelationException("Исходящий запрос на дружбу не найден");
        }
        recoveryPositionAccountsId(Collections.singletonList(friendRelation), mainAccountId);
        RelationStatus relationStatus = friendRelation.getRelationStatus();
        if (relationStatus == RelationStatus.FULL) {
            RelationStatus relationStatusAfterRemoveFriend = RelationStatus.REQUEST_IN;
            friendRelation.setRelationStatus(relationStatusAfterRemoveFriend);
            return relationStatusAfterRemoveFriend;
        } else if (relationStatus == RelationStatus.REQUEST_OUT) {
            friendRelationDao.remove(friendRelation);
            return RelationStatus.EMPTY;
        } else {
            throw new RelationException("Исходящий запрос на дружбу не найден");
        }
    }

    private void recoveryPositionAccountsId(List<FriendRelation> friendRelations, int mainAccountId) {
        for (FriendRelation friendRelation : friendRelations) {
            if (friendRelation.getAccount2().getId() == mainAccountId) {
                Account tmpAccount = friendRelation.getAccount1();
                friendRelation.setAccount1(friendRelation.getAccount2());
                friendRelation.setAccount2(tmpAccount);
                if (friendRelation.getRelationStatus() == RelationStatus.REQUEST_IN) {
                    friendRelation.setRelationStatus(RelationStatus.REQUEST_OUT);
                } else if (friendRelation.getRelationStatus() == RelationStatus.REQUEST_OUT) {
                    friendRelation.setRelationStatus(RelationStatus.REQUEST_IN);
                }
            }
        }
    }
}
