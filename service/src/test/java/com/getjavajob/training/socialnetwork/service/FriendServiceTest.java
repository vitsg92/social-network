package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.ralation.FriendRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.FriendRelationDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FriendServiceTest {
    private static final String FIRST_ACCOUNT_NAME = "Vasya";
    private static final String SECOND_ACCOUNT_NAME = "Petr";
    private static final int FIRST_ACCOUNT_ID = 1;
    private static final int SECOND_ACCOUNT_ID = 10;
    @Mock
    private AccountDao accountDao;

    @Mock
    private FriendRelationDao friendRelationDao;
    private Account account1;
    private Account account2;
    private FriendRelation friendRelation;

    @InjectMocks
    private FriendService friendService;

    @Before
    public void onBefore() {
        account1 = new Account();
        account1.setId(FIRST_ACCOUNT_ID);
        account1.setFirstName(FIRST_ACCOUNT_NAME);
        account1.setLastName("Petrov");
        account1.setEmail("vasya@yandex.ru");
        account1.setPassword("12345");
        account2 = new Account();
        account2.setId(SECOND_ACCOUNT_ID);
        account2.setFirstName(SECOND_ACCOUNT_NAME);
        account2.setLastName("Ivanov");
        account2.setEmail("petr@yandex.ru");
        account2.setPassword("12345");
        friendRelation = new FriendRelation();
        friendRelation.setAccount1(account1);
        friendRelation.setAccount2(account2);
        friendRelation.setRelationStatus(RelationStatus.REQUEST_OUT);
    }

    @Test
    public void testGetFriendRelationStatus() {
        when(friendRelationDao.getFriendRelation(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID)).thenReturn(friendRelation);
        assertEquals(RelationStatus.REQUEST_OUT, friendService.getFriendRelationStatus(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID));
        when(friendRelationDao.getFriendRelation(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID)).thenReturn(friendRelation);
        assertEquals(RelationStatus.REQUEST_IN, friendService.getFriendRelationStatus(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID));
    }

    @Test
    public void testGetAccountFriends() {
        when(friendRelationDao.getFriendRelationsFriend(eq(FIRST_ACCOUNT_ID), any(Pageable.class))).thenReturn(Collections.singletonList(friendRelation));
        assertEquals(SECOND_ACCOUNT_NAME, friendService.getAccountFriends(FIRST_ACCOUNT_ID).get(0).getAccount2().getFirstName());
        when(friendRelationDao.getFriendRelationsFriend(eq(SECOND_ACCOUNT_ID), any(Pageable.class))).thenReturn(Collections.singletonList(friendRelation));
        assertEquals(FIRST_ACCOUNT_NAME, friendService.getAccountFriends(SECOND_ACCOUNT_ID).get(0).getAccount2().getFirstName());
    }

    @Test
    public void testGetRequestOutFriendshipAccounts() {
        when(friendRelationDao.getFriendRelationsRequestOutFriendship(eq(FIRST_ACCOUNT_ID), any(Pageable.class))).thenReturn(Collections.singletonList(friendRelation));
        assertEquals(SECOND_ACCOUNT_NAME, friendService.getRequestOutFriendshipAccounts(FIRST_ACCOUNT_ID).get(0).getAccount2().getFirstName());
    }

    @Test
    public void testGetRequestInFriendshipAccounts() {
        when(friendRelationDao.getFriendRelationsRequestOutFriendship(eq(SECOND_ACCOUNT_ID), any(Pageable.class))).thenReturn(Collections.singletonList(friendRelation));
        assertEquals(FIRST_ACCOUNT_NAME, friendService.getRequestOutFriendshipAccounts(SECOND_ACCOUNT_ID).get(0).getAccount2().getFirstName());
    }

    @Test
    public void testAddFriend1() {
        when(friendRelationDao.getFriendRelation(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID)).thenReturn(null);
        assertEquals(RelationStatus.REQUEST_OUT, friendService.addFriend(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID));
    }

    @Test
    public void testAddFriend2() {
        when(friendRelationDao.getFriendRelation(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID)).thenReturn(friendRelation);
        assertEquals(RelationStatus.FULL, friendService.addFriend(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID));
    }

    @Test
    public void testAddFriend3() {
        friendRelation.setRelationStatus(RelationStatus.FULL);
        when(friendRelationDao.getFriendRelation(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID)).thenReturn(friendRelation);
        try {
            friendService.addFriend(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Исходящий запрос на дружбу уже отправлен", e.getMessage());
        }
    }

    @Test
    public void testAddFriend4() {
        friendRelation.setRelationStatus(RelationStatus.REQUEST_IN);
        when(friendRelationDao.getFriendRelation(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID)).thenReturn(friendRelation);
        try {
            friendService.addFriend(SECOND_ACCOUNT_ID, FIRST_ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Исходящий запрос на дружбу уже отправлен", e.getMessage());
        }
    }

    @Test
    public void testRemoveFriend1() {
        when(friendRelationDao.getFriendRelation(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID)).thenReturn(null);
        try {
            friendService.removeFriend(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Исходящий запрос на дружбу не найден", e.getMessage());
        }
    }

    @Test
    public void testRemoveFriend2() {
        friendRelation.setRelationStatus(RelationStatus.REQUEST_IN);
        when(friendRelationDao.getFriendRelation(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID)).thenReturn(friendRelation);
        try {
            friendService.removeFriend(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Исходящий запрос на дружбу не найден", e.getMessage());
        }
    }

    @Test
    public void testRemoveFriend3() {
        friendRelation.setRelationStatus(RelationStatus.FULL);
        when(friendRelationDao.getFriendRelation(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID)).thenReturn(friendRelation);
        assertEquals(RelationStatus.REQUEST_IN, friendService.removeFriend(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID));
    }

    @Test
    public void testRemoveFriend4() {
        when(friendRelationDao.getFriendRelation(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID)).thenReturn(friendRelation);
        assertEquals(RelationStatus.EMPTY, friendService.removeFriend(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID));
    }
}
