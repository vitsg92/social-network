package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.ralation.GroupPrivilege;
import com.getjavajob.training.socialnetwork.common.ralation.GroupRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.GroupDao;
import com.getjavajob.training.socialnetwork.dao.GroupRelationDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    private static final int ACCOUNT_ID = 1;
    private static final int GROUP_ID = 10;
    @Mock
    private AccountDao accountDao;

    @Mock
    private GroupDao groupDao;

    @Mock
    private GroupRelationDao groupRelationDao;
    private Account account;
    private Group group;
    private GroupRelation groupRelation;

    @InjectMocks
    private GroupService groupService;

    @Before
    public void onBefore() {
        account = new Account();
        account.setId(ACCOUNT_ID);
        account.setFirstName("Петр");
        account.setLastName("Иванов");
        account.setEmail("pert@yandex.ru");
        account.setPassword("12345");
        group = new Group();
        group.setId(GROUP_ID);
        group.setCreator(account);
        group.setGroupName("Cars");
        groupRelation = new GroupRelation(account, group, RelationStatus.FULL, GroupPrivilege.MODER);
    }

    @Test
    public void testAddGroup() {
        when(groupDao.save(group)).thenReturn(group);
        when(groupRelationDao.save(any(GroupRelation.class))).then((Answer<GroupRelation>) invocationOnMock -> {
            GroupRelation savedGroupRelation = invocationOnMock.getArgument(0);
            assertEquals(RelationStatus.FULL, savedGroupRelation.getRelationStatus());
            assertEquals(ACCOUNT_ID, savedGroupRelation.getAccount().getId());
            assertEquals(GROUP_ID, savedGroupRelation.getGroup().getId());
            assertEquals(GroupPrivilege.MODER, savedGroupRelation.getGroupPrivilege());
            return savedGroupRelation;
        });
        assertEquals("Cars", groupService.addGroup(account, group).getGroupName());
        verify(groupRelationDao).save(any(GroupRelation.class));
        doThrow(new RuntimeException("Тест пройден")).when(groupDao).save(any(Group.class));
        try {
            groupService.addGroup(account, group);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Тест пройден", e.getMessage());
        }
        verify(groupRelationDao, times(1)).save(any(GroupRelation.class));
    }

    @Test
    public void testUpdateGroup() {
        group.setGroupName("Books");
        when(groupDao.save(group)).thenReturn(group);
        assertEquals("Books", groupService.updateGroup(group).getGroupName());
    }

    @Test
    public void testGetGroupById() {
        when(groupDao.findById(GROUP_ID)).thenReturn(group);
        assertEquals("Cars", groupService.getGroupById(GROUP_ID).getGroupName());
    }

    @Test
    public void testGetAccountGroups() {
        when(groupRelationDao.getAccountGroupRelations(eq(ACCOUNT_ID), eq(RelationStatus.FULL), any(Pageable.class)))
                .thenReturn(Collections.singletonList(groupRelation));
        assertEquals(1, groupService.getAccountGroups(ACCOUNT_ID, RelationStatus.FULL).size());
    }

    @Test
    public void testGetAccountGroupsRequests() {
        when(groupRelationDao.getAccountGroupRelations(eq(ACCOUNT_ID), eq(RelationStatus.REQUEST_OUT), any(Pageable.class)))
                .thenReturn(new ArrayList<>());
        assertEquals(0, groupService.getAccountGroups(ACCOUNT_ID, RelationStatus.REQUEST_OUT).size());
    }

    @Test
    public void testGetCreatedGroups() {
        when(groupDao.findByCreatorId(eq(ACCOUNT_ID), any(Pageable.class))).thenReturn(Collections.singletonList(group));
        assertEquals(1, groupService.getCreatedGroups(ACCOUNT_ID).size());
    }

    @Test
    public void testGetCountGroupsWithSearchByString() {
        when(groupDao.countByGroupNameIgnoreCaseContaining("asd")).thenReturn(2);
        assertEquals(2, groupService.getCountGroupsWithSearchByString("asd"));
    }

    @Test
    public void testSearchGroupByStringPagination() {
        Group group1 = new Group();
        group1.setGroupName("Tars");
        when(groupDao.findByGroupNameContainingIgnoreCase(eq("asd"), any(Pageable.class)))
                .thenReturn(Arrays.asList(group, group1));
        assertEquals(2, groupService.searchGroupByStringPagination("asd", 1, 10).size());
    }

    @Test
    public void testSearchGroupByString() {
        Group group1 = new Group();
        group1.setGroupName("Tars");
        when(groupDao.findByGroupNameContainingIgnoreCase(eq("asd"))).thenReturn(Arrays.asList(group, group1));
        assertEquals(2, groupService.searchGroupByString("asd").size());
    }

    @Test
    public void testRemoveGroup() {
        doThrow(new RuntimeException("Тест пройден")).when(groupRelationDao).removeAllByGroupId(eq(GROUP_ID));
        try {
            groupService.removeGroup(GROUP_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Тест пройден", e.getMessage());
        }
        verify(groupDao, never()).removeById(GROUP_ID);
    }

    @Test
    public void testGetGroupRelation() {
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(groupRelation);
        assertEquals(GroupPrivilege.MODER, groupService.getGroupRelation(ACCOUNT_ID, GROUP_ID).getGroupPrivilege());
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(null);
        when(accountDao.findById(ACCOUNT_ID)).thenReturn(account);
        when(groupDao.findById(GROUP_ID)).thenReturn(group);
        assertEquals(RelationStatus.EMPTY, groupService.getGroupRelation(ACCOUNT_ID, GROUP_ID).getRelationStatus());
    }

    @Test
    public void testJoinOnGroup() {
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(null);
        when(accountDao.findById(ACCOUNT_ID)).thenReturn(account);
        when(groupDao.findById(GROUP_ID)).thenReturn(group);
        when(groupRelationDao.save(any(GroupRelation.class))).then((Answer<GroupRelation>) invocationOnMock ->
                invocationOnMock.getArgument(0));
        assertEquals(RelationStatus.REQUEST_OUT, groupService.joinOnGroup(ACCOUNT_ID, GROUP_ID));
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(groupRelation);
        try {
            groupService.joinOnGroup(ACCOUNT_ID, GROUP_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Заявка в группу уже существует", e.getMessage());
        }
    }

    @Test
    public void testOutOfGroup() {
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(groupRelation);
        group.setCreator(new Account());
        when(groupDao.findById(GROUP_ID)).thenReturn(group);
        assertEquals(RelationStatus.EMPTY, groupService.outOfGroup(ACCOUNT_ID, GROUP_ID));
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(null);
        try {
            groupService.outOfGroup(ACCOUNT_ID, GROUP_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Вы не участник группы", e.getMessage());
        }
    }

    @Test
    public void testRemoveAccountFromGroup() {
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(groupRelation);
        group.setCreator(new Account());
        when(groupDao.findById(GROUP_ID)).thenReturn(group);
        assertEquals(RelationStatus.EMPTY, groupService.removeAccountFromGroup(GROUP_ID, ACCOUNT_ID));
        groupRelation.setRelationStatus(RelationStatus.REQUEST_OUT);
        try {
            groupService.removeAccountFromGroup(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь не в группе", e.getMessage());
        }
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(null);
        try {
            groupService.removeAccountFromGroup(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь не в группе", e.getMessage());
        }
    }

    @Test
    public void testAcceptAccountToGroup() {
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(groupRelation);
        try {
            groupService.acceptAccountToGroup(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь уже в группе", e.getMessage());
        }
        groupRelation.setGroupPrivilege(GroupPrivilege.USER);
        groupRelation.setRelationStatus(RelationStatus.REQUEST_OUT);
        assertEquals(RelationStatus.FULL, groupService.acceptAccountToGroup(GROUP_ID, ACCOUNT_ID));
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(null);
        try {
            groupService.acceptAccountToGroup(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Заявки в группу не было", e.getMessage());
        }
    }

    @Test
    public void testGetGroupMembers() {
        when(groupRelationDao.getGroupMembersDetails(eq(GROUP_ID), eq(RelationStatus.FULL), any(Pageable.class)))
                .thenReturn(Collections.singletonList(groupRelation));
        assertEquals(1, groupService.getGroupMembers(GROUP_ID).size());
    }

    @Test
    public void testGetJoinRequestAccounts() {
        when(groupRelationDao.getGroupMembersDetails(eq(GROUP_ID), eq(RelationStatus.REQUEST_OUT), any(Pageable.class)))
                .thenReturn(new ArrayList<>());
        assertEquals(0, groupService.getJoinRequestAccounts(GROUP_ID).size());
    }

    @Test
    public void testGetGroupMembersOnRole() {
        when(groupRelationDao.getGroupMembersOnPrivilege(eq(GROUP_ID), eq(GroupPrivilege.MODER), any(Pageable.class)))
                .thenReturn(Collections.singletonList(groupRelation));
        assertEquals(1, groupService.getGroupMembersOnPrivilege(GROUP_ID, GroupPrivilege.MODER).size());
        when(groupRelationDao.getGroupMembersOnPrivilege(eq(GROUP_ID), eq(GroupPrivilege.USER), any(Pageable.class)))
                .thenReturn(new ArrayList<>());
        assertEquals(0, groupService.getGroupMembersOnPrivilege(GROUP_ID, GroupPrivilege.USER).size());
    }

    @Test
    public void testAddModerator() {
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(groupRelation);
        try {
            groupService.addModerator(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь уже является модератором", e.getMessage());
        }
        groupRelation.setGroupPrivilege(GroupPrivilege.USER);
        assertEquals(GroupPrivilege.MODER, groupService.addModerator(GROUP_ID, ACCOUNT_ID));
        groupRelation.setRelationStatus(RelationStatus.REQUEST_OUT);
        try {
            groupService.addModerator(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь не в группе", e.getMessage());
        }
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(null);
        try {
            groupService.addModerator(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь не в группе", e.getMessage());
        }
    }

    @Test
    public void testRemoveModerator() {
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(groupRelation);
        group.setCreator(new Account());
        when(groupDao.findById(GROUP_ID)).thenReturn(group);
        assertEquals(GroupPrivilege.USER, groupService.removeModerator(GROUP_ID, ACCOUNT_ID));
        groupRelation.setGroupPrivilege(GroupPrivilege.USER);
        try {
            groupService.removeModerator(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь не является модератором", e.getMessage());
        }
        groupRelation.setGroupPrivilege(GroupPrivilege.USER);
        groupRelation.setRelationStatus(RelationStatus.REQUEST_OUT);
        try {
            groupService.removeModerator(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь не в группе", e.getMessage());
        }
        when(groupRelationDao.getGroupRelation(ACCOUNT_ID, GROUP_ID)).thenReturn(null);
        try {
            groupService.removeModerator(GROUP_ID, ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Пользователь не в группе", e.getMessage());
        }
    }
}
