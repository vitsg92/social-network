package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Phone;
import com.getjavajob.training.socialnetwork.common.PhoneType;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.PrivateMessageDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    private static final int ACCOUNT_ID = 1;

    @Mock
    private AccountDao accountDao;

    @Mock
    private PrivateMessageDao privateMessageDao;
    private Account insertAccount;
    private Account resultAccount;

    @InjectMocks
    private AccountService accountService;

    @Before
    public void onBefore() {
        insertAccount = new Account();
        insertAccount.setFirstName("Петр");
        insertAccount.setLastName("Иванов");
        insertAccount.setEmail("petr@yandex.ru");
        insertAccount.setPassword("12345");
        insertAccount.setPhones(new ArrayList<>());
        resultAccount = new Account();
        resultAccount.setFirstName("Петр");
        resultAccount.setLastName("Иванов");
        resultAccount.setPhones(new ArrayList<>());
        resultAccount.setId(ACCOUNT_ID);
        resultAccount.setEmail("pert@yandex.ru");
        resultAccount.setPassword("12345");
        List<Phone> phones = new ArrayList<>();
        Phone a = new Phone();
        a.setId(1);
        a.setOwner(resultAccount);
        a.setPhoneNumber("11111111111");
        a.setPhoneType(PhoneType.HOME);
        Phone b = new Phone();
        b.setId(2);
        b.setOwner(resultAccount);
        b.setPhoneNumber("22222222222");
        b.setPhoneType(PhoneType.MOBILE);
        phones.add(a);
        phones.add(b);
        resultAccount.setPhones(phones);
    }

    @Test
    public void testGetAccountById() {
        when(accountDao.findById(ACCOUNT_ID)).thenReturn(resultAccount);
        assertEquals("Петр", accountService.getAccountById(ACCOUNT_ID).getFirstName());
    }

    @Test
    public void testCreateAccount() {
        when(accountDao.save(insertAccount)).thenReturn(resultAccount);
        assertEquals(1, accountService.createAccount(insertAccount).getId());
    }

    @Test
    public void testAuthorization() {
        when(accountDao.findByEmail("petr@yandex.ru")).thenReturn(resultAccount);
        when(accountDao.findByEmail("asdf")).thenReturn(null);
        when(privateMessageDao.getByReaderIdAndAccepted(ACCOUNT_ID, false)).thenReturn(new ArrayList<>());
        assertEquals(1, accountService.authorization("petr@yandex.ru").getId());
        try {
            accountService.authorization("asdf");
            fail();
        } catch (UsernameNotFoundException e) {
            assertEquals("Пользователь с таким email не найден", e.getMessage());
        } catch (RuntimeException e) {
            fail();
        }
    }

    @Test
    public void testUpdateAccount() {
        when(accountDao.save(resultAccount)).thenReturn(resultAccount);
        Phone phone = new Phone();
        phone.setPhoneNumber("");
        resultAccount.getPhones().add(phone);
        Account account = accountService.updateAccount(resultAccount);
        assertEquals(2, account.getPhones().size());
        assertEquals(1, account.getPhones().get(0).getOwner().getId());
    }

    @Test
    public void testRemoveAccount() {
        doThrow(new RuntimeException("Тест пройден")).when(privateMessageDao).removeOnAccountId(ACCOUNT_ID);
        try {
            accountService.removeAccount(ACCOUNT_ID);
            fail();
        } catch (RuntimeException e) {
            assertEquals("Тест пройден", e.getMessage());
        }
    }
}
