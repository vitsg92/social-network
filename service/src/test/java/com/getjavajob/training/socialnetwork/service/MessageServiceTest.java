package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.dto.Dialog;
import com.getjavajob.training.socialnetwork.common.dto.UserDialogDetail;
import com.getjavajob.training.socialnetwork.common.message.Message;
import com.getjavajob.training.socialnetwork.common.message.MessageOnWall;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.MessageOnWallDao;
import com.getjavajob.training.socialnetwork.dao.PrivateMessageDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {
    private static final String FIRST_ACCOUNT_NAME = "Vasya";
    private static final String SECOND_ACCOUNT_NAME = "Petr";
    private static final int FIRST_ACCOUNT_ID = 1;
    private static final int SECOND_ACCOUNT_ID = 5;
    private static final int GROUP_ID = 10;
    private static final int MESSAGE_ON_GROUP_WALL_ID = 15;
    private static final int PRIVATE_MESSAGE_ID = 20;
    private static final int PAGE_SIZE = 20;

    @InjectMocks
    private MessageService messageService;

    @Mock
    private AccountDao accountDao;

    @Mock
    private MessageOnWallDao messageOnWallDao;

    @Mock
    private PrivateMessageDao privateMessageDao;

    private Account account1;
    private Account account2;
    private MessageOnWall messageOnGroupWall;
    private PrivateMessage privateMessage;

    @Before
    public void onBefore() {
        account1 = new Account();
        account1.setId(FIRST_ACCOUNT_ID);
        account1.setFirstName(FIRST_ACCOUNT_NAME);
        account1.setLastName("Petrov");
        account1.setEmail("vasya@yandex.ru");
        account1.setPassword("12345");
        account2 = new Account();
        account2.setId(SECOND_ACCOUNT_ID);
        account2.setFirstName(SECOND_ACCOUNT_NAME);
        account2.setLastName("Ivanov");
        account2.setEmail("petr@yandex.ru");
        account2.setPassword("12345");
        messageOnGroupWall = new MessageOnWall();
        messageOnGroupWall.setId(MESSAGE_ON_GROUP_WALL_ID);
        messageOnGroupWall.setWriter(account1);
        messageOnGroupWall.setDestinationId(GROUP_ID);
        messageOnGroupWall.setMessageType(MessageType.GROUP_WALL);
        privateMessage = new PrivateMessage();
        privateMessage.setId(PRIVATE_MESSAGE_ID);
        privateMessage.setWriter(account1);
        privateMessage.setReader(account2);
        privateMessage.setAccepted(false);
    }

    @Test
    public void testGetMessagesFromDialog() {
        when(privateMessageDao.getMessagesFromDialog(eq(FIRST_ACCOUNT_ID), eq(SECOND_ACCOUNT_ID), eq(PAGE_SIZE)))
                .thenReturn(Collections.singletonList(privateMessage));
        assertEquals(1, messageService.getMessagesFromDialog(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID).size());
    }

    @Test
    public void testSave() {
        when(privateMessageDao.save(privateMessage)).thenReturn(privateMessage);
        when(messageOnWallDao.save(messageOnGroupWall)).thenReturn(messageOnGroupWall);
        Message message1 = messageService.save(privateMessage);
        if (message1 instanceof PrivateMessage) {
            assertEquals(account2, ((PrivateMessage) message1).getReader());
        } else {
            fail();
        }
        Message message2 = messageService.save(messageOnGroupWall);
        if (message2 instanceof MessageOnWall) {
            assertEquals(MessageType.GROUP_WALL, ((MessageOnWall) message2).getMessageType());
        } else {
            fail();
        }
    }

    @Test
    public void testAcceptMessage() {
        when(privateMessageDao.findById(PRIVATE_MESSAGE_ID)).thenReturn(privateMessage);
        assertTrue(messageService.acceptMessage(PRIVATE_MESSAGE_ID).isAccepted());
    }

    @Test
    public void testRemoveMessagesFromUnaccepted() {
        new ArrayList<>(Collections.singletonList(PRIVATE_MESSAGE_ID));
        account2.setUnacceptedMessages(new ArrayList<>(Collections.singletonList(privateMessage)));
        doAnswer((Answer<Void>) invocationOnMock -> {
            List<Number> messagesId = invocationOnMock.getArgument(0);
            assertEquals(1, messagesId.size());
            assertEquals(PRIVATE_MESSAGE_ID, messagesId.get(0).intValue());
            return null;
        }).when(privateMessageDao).updateAcceptedMessages(any());
        messageService.removeMessagesFromUnaccepted(account2, FIRST_ACCOUNT_ID);
        assertEquals(0, account2.getUnacceptedMessages().size());
    }

    @Test
    public void testGetUserDialogDetails() {
        when(privateMessageDao.getDialogs(FIRST_ACCOUNT_ID, PAGE_SIZE)).thenReturn(Collections.singletonList(new Dialog() {
            @Override
            public int getAccountId() {
                return SECOND_ACCOUNT_ID;
            }

            @Override
            public int getLastMessageId() {
                return PRIVATE_MESSAGE_ID;
            }
        }));
        when(accountDao.findById(SECOND_ACCOUNT_ID)).thenReturn(account2);
        when(privateMessageDao.findById(PRIVATE_MESSAGE_ID)).thenReturn(privateMessage);
        when(privateMessageDao.getCountUnacceptedMessagesOnDialog(FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID)).thenReturn(0);
        List<UserDialogDetail> userDialogDetails = messageService.getUserDialogDetails(account1);
        assertEquals(1, userDialogDetails.size());
        assertEquals(account2, userDialogDetails.iterator().next().getCompanion());
    }

    @Test
    public void testGetMessageOnWall() {
        when(messageOnWallDao.getByDestinationIdAndMessageType(eq(GROUP_ID), eq(MessageType.GROUP_WALL), any(Pageable.class)))
                .thenReturn(Collections.singletonList(messageOnGroupWall));
        assertEquals(account1, messageService.getMessageOnWall(GROUP_ID, MessageType.GROUP_WALL).get(0).getWriter());
    }
}
