package com.getjavajob.training.socialnetwork.common.ralation;

public enum RelationStatus {
    FULL,
    REQUEST_OUT,
    REQUEST_IN,
    EMPTY
}
