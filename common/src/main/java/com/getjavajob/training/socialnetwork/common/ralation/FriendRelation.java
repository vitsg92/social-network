package com.getjavajob.training.socialnetwork.common.ralation;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Element;

import javax.persistence.*;

@Entity
@Table(name = "sn_friend_relations")
public class FriendRelation implements Element {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "account1Id")
    private Account account1;

    @ManyToOne
    @JoinColumn(name = "account2Id")
    private Account account2;

    @Enumerated(EnumType.STRING)
    private RelationStatus relationStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount1() {
        return account1;
    }

    public void setAccount1(Account account1) {
        this.account1 = account1;
    }

    public Account getAccount2() {
        return account2;
    }

    public void setAccount2(Account account2) {
        this.account2 = account2;
    }

    public RelationStatus getRelationStatus() {
        return relationStatus;
    }

    public void setRelationStatus(RelationStatus relationStatus) {
        this.relationStatus = relationStatus;
    }
}
