package com.getjavajob.training.socialnetwork.common;

public interface Element {
    int getId();

    void setId(int id);
}
