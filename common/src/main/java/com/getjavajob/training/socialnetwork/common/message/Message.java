package com.getjavajob.training.socialnetwork.common.message;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Element;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public class Message implements Element, Comparable<Message> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "writerId")
    private Account writer;
    private String dateOfWriting;
    private String messageContent;

    public Account getWriter() {
        return writer;
    }

    public void setWriter(Account writer) {
        this.writer = writer;
    }

    public String getDateOfWriting() {
        return dateOfWriting;
    }

    public void setDateOfWriting(String dateOfWriting) {
        this.dateOfWriting = dateOfWriting;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String message) {
        this.messageContent = message;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Message o) {
        return Integer.compare(this.id, o.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return id == message.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
