package com.getjavajob.training.socialnetwork.common;

public enum AccountRole {
    USER, ADMIN
}
