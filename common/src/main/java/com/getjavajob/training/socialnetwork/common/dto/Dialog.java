package com.getjavajob.training.socialnetwork.common.dto;

public interface Dialog {

    int getAccountId();

    int getLastMessageId();
}
