package com.getjavajob.training.socialnetwork.common.message;

public enum MessageType {
    ACCOUNT_WALL, GROUP_WALL
}
