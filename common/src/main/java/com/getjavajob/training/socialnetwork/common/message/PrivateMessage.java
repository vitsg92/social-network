package com.getjavajob.training.socialnetwork.common.message;

import com.getjavajob.training.socialnetwork.common.Account;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sn_private_messages")
public class PrivateMessage extends Message {

    @ManyToOne
    @JoinColumn(name = "readerId")
    private Account reader;

    private boolean accepted;

    public Account getReader() {
        return reader;
    }

    public void setReader(Account reader) {
        this.reader = reader;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
}
