package com.getjavajob.training.socialnetwork.common.message;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "sn_messages_on_wall")
public class MessageOnWall extends Message {

    private int destinationId;

    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }
}
