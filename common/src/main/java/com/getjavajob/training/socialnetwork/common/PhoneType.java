package com.getjavajob.training.socialnetwork.common;

public enum PhoneType {
    WORK("Рабочий"), MOBILE("Мобильный"), HOME("Домашний");

    private String localName;

    PhoneType(String localName) {
        this.localName = localName;
    }

    public String getLocalName() {
        return localName;
    }
}
