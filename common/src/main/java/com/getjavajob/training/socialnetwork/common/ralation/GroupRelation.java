package com.getjavajob.training.socialnetwork.common.ralation;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Element;
import com.getjavajob.training.socialnetwork.common.Group;

import javax.persistence.*;

@Entity
@Table(name = "sn_group_relations")
public class GroupRelation implements Element {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "accountId")
    private Account account;
    @ManyToOne
    @JoinColumn(name = "groupId")
    private Group group;
    @Enumerated(EnumType.STRING)
    private RelationStatus relationStatus;
    @Enumerated(EnumType.STRING)
    private GroupPrivilege groupPrivilege;

    public GroupRelation() {
    }

    public GroupRelation(Account account, Group group, RelationStatus relationStatus, GroupPrivilege groupPrivilege) {
        this.account = account;
        this.group = group;
        this.relationStatus = relationStatus;
        this.groupPrivilege = groupPrivilege;
    }

    public GroupPrivilege getGroupPrivilege() {
        return groupPrivilege;
    }

    public void setGroupPrivilege(GroupPrivilege groupPrivilege) {
        this.groupPrivilege = groupPrivilege;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public RelationStatus getRelationStatus() {
        return relationStatus;
    }

    public void setRelationStatus(RelationStatus relationStatus) {
        this.relationStatus = relationStatus;
    }
}
