package com.getjavajob.training.socialnetwork.common;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "sn_groups")
public class Group implements Element, Comparable<Group> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String groupName;
    private String description;
    private String dateRegistration;

    @ManyToOne
    @JoinColumn(name = "creator")
    private Account creator;

    @Basic(fetch = FetchType.LAZY)
    private byte[] groupPhoto;

    public byte[] getGroupPhoto() {
        return groupPhoto;
    }

    public void setGroupPhoto(byte[] groupPhoto) {
        this.groupPhoto = groupPhoto;
    }

    public String getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(String dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public int compareTo(Group o) {
        return Integer.compare(o.id, this.id);
    }

    @Override
    public String toString() {
        return groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Group group = (Group) o;
        return id == group.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
