package com.getjavajob.training.socialnetwork.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.*;

@Entity
@Table(name = "sn_accounts")
@XmlRootElement
@XmlSeeAlso({Phone.class})
@XmlAccessorType(XmlAccessType.FIELD)
public class Account implements Element, Comparable<Account>, UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlAttribute
    private int id;

    private String firstName;
    private String lastName;
    private String patronymic;
    private String dateOfBirth;

    @JsonIgnore
    @XmlElementWrapper
    @XmlAnyElement(lax = true)
    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Phone> phones = new ArrayList<>();
    private String email;
    private String icq;
    private String skype;

    @XmlTransient
    @JsonIgnore
    private String password;

    @XmlTransient
    @JsonIgnore
    private String dateRegistration;

    @XmlTransient
    @JsonIgnore
    @Basic(fetch = FetchType.LAZY)
    private byte[] photo;

    @XmlTransient
    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private AccountRole role;

    @Transient
    private List<PrivateMessage> unacceptedMessages;

    public List<PrivateMessage> getUnacceptedMessages() {
        return unacceptedMessages;
    }

    public void setUnacceptedMessages(List<PrivateMessage> unacceptedMessages) {
        this.unacceptedMessages = unacceptedMessages;
    }


    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_" + role));
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(String dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    @Override
    public int compareTo(Account o) {
        return Integer.compare(o.id, this.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Account account = (Account) o;
        return id == account.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }
}
