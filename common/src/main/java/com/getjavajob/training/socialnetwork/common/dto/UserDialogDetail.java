package com.getjavajob.training.socialnetwork.common.dto;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;

import java.util.Objects;

public class UserDialogDetail implements Comparable<UserDialogDetail> {

    private Account mainAccount;
    private Account companion;
    private PrivateMessage lastMessageInDialog;
    private int countUnacceptedMessages;

    public Account getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(Account mainAccount) {
        this.mainAccount = mainAccount;
    }

    public Account getCompanion() {
        return companion;
    }

    public void setCompanion(Account companion) {
        this.companion = companion;
    }

    public PrivateMessage getLastMessageInDialog() {
        return lastMessageInDialog;
    }

    public void setLastMessageInDialog(PrivateMessage lastMessageInDialog) {
        this.lastMessageInDialog = lastMessageInDialog;
    }

    public int getCountUnacceptedMessages() {
        return countUnacceptedMessages;
    }

    public void setCountUnacceptedMessages(int countUnacceptedMessages) {
        this.countUnacceptedMessages = countUnacceptedMessages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDialogDetail that = (UserDialogDetail) o;
        return lastMessageInDialog.equals(that.lastMessageInDialog);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastMessageInDialog);
    }

    @Override
    public int compareTo(UserDialogDetail o) {
        return Integer.compare(o.getLastMessageInDialog().getId(), this.lastMessageInDialog.getId());
    }
}
