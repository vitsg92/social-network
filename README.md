# Social Network  
  
** Functionality: **  
  
+ registration  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ profile export to xml  
+ create/update groups  
+ participation in groups  
+ exchange of messages(private, on wall) in real time  
+ notice of private messages  
+ addition of users in friends   
  
** Tools: **  
JDK 8, Spring 5, JPA 2 / Hibernate 5, JAXB, jQuery 3, Twitter  Bootstrap 4, JUnit 4, Mockito 2, JMS 2 / ActiveMQ 5, Maven 3, Git / Bitbucket, Tomcat 8,  MySQL, MongoDb, IntelliJIDEA.  
  
  Demo: https://vitsg92.herokuapp.com  
  test log/pass: test@test.ru/qwerty
  
** Notes: **  
SQL ddl is located in the dao module `resources/schema.sql`  
    
_  
Гончаренко Виталий  
Тренинг getJavaJob  
http://www.getjavajob.com  
