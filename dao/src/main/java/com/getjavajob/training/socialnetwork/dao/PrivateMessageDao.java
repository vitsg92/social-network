package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.dto.Dialog;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PrivateMessageDao extends JpaRepository<PrivateMessage, Integer>, GenericDao<PrivateMessage> {

    PrivateMessage findById(int id);

    @Query(value = "select * from (select m.* from sn_private_messages m where m.writerId = :mainAccountId " +
            "and m.readerId = :companionId or m.writerId = :companionId and m.readerId = :mainAccountId " +
            "order by m.id desc limit 0, :pageSize) a order by a.id", nativeQuery = true)
    List<PrivateMessage> getMessagesFromDialog(@Param("mainAccountId") int mainAccountId, @Param("companionId") int companionId,
                                               @Param("pageSize") int pageSize);

    @Query(value = "select * from (select m.* from sn_private_messages m where (m.writerId = :mainAccountId " +
            "and m.readerId = :companionId or m.writerId = :companionId and m.readerId = :mainAccountId) " +
            "and m.id < :lastElementId order by m.id desc limit 0, :pageSize) a order by a.id",
            nativeQuery = true)
    List<PrivateMessage> getMessagesFromDialog(@Param("mainAccountId") int mainAccountId, @Param("companionId") int companionId,
                                               @Param("pageSize") int pageSize, @Param("lastElementId") int lastElementId);

    List<PrivateMessage> getByReaderIdAndAccepted(int readerId, boolean isAccepted);

    @Modifying
    @Query("update PrivateMessage m set m.accepted = true where m.id in :acceptedMessagesId")
    void updateAcceptedMessages(@Param("acceptedMessagesId") List<Integer> acceptedMessagesId);

    @Query(nativeQuery = true, value = "select tmp.companion as accountId, max(tmp.id) as lastMessageId from " +
            "(select id, case when m.writerId = :mainAccountId then m.readerId else m.writerId end as companion from sn_private_messages m" +
            " where m.readerId = :mainAccountId or m.writerId = :mainAccountId) tmp group by tmp.companion order by " +
            "lastMessageId desc limit 0, :size")
    List<Dialog> getDialogs(@Param("mainAccountId") int mainAccountId, @Param("size") int pageSize);

    @Query(nativeQuery = true, value = "select tmp.companion as accountId, max(tmp.id) as lastMessageId from " +
            "(select id, case when m.writerId = :mainAccountId then m.readerId else m.writerId end as companion from sn_private_messages m" +
            " where (m.readerId = :mainAccountId or m.writerId = :mainAccountId) and m.id < :lastElementId) tmp group by tmp.companion order by " +
            "lastMessageId desc limit 0, :size")
    List<Dialog> getDialogs(@Param("mainAccountId") int mainAccountId, @Param("size") int pageSize,
                            @Param("lastElementId") int lastElementId);

    @Query("select count(m) from PrivateMessage m where m.reader.id = :mainAccountId and m.writer.id = :companionId and m.accepted = false")
    int getCountUnacceptedMessagesOnDialog(@Param("mainAccountId") int mainAccountId, @Param("companionId") int companionId);

    @Modifying
    @Query("delete from PrivateMessage m where m.writer.id = :id or m.reader.id = :id")
    void removeOnAccountId(@Param("id") int id);
}
