package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.ralation.FriendRelation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FriendRelationDao extends JpaRepository<FriendRelation, Integer>, GenericDao<FriendRelation> {

    @Query("select fr from FriendRelation fr where fr.account1.id = :mainAccountId and fr.account2.id = :companionId or " +
            "fr.account1.id = :companionId and fr.account2.id = :mainAccountId")
    FriendRelation getFriendRelation(@Param("mainAccountId") int mainAccountId, @Param("companionId") int companionId);

    @Query("select fr from FriendRelation fr where (fr.account1.id = :id or fr.account2.id = :id) and fr.relationStatus = \'FULL\'")
    List<FriendRelation> getFriendRelationsFriend(@Param("id") int id, Pageable pageable);

    @Query("select fr from FriendRelation fr where (fr.account1.id = :id or fr.account2.id = :id) and fr.relationStatus = \'FULL\' " +
            "and fr.id < :lastElementId")
    List<FriendRelation> getFriendRelationsFriend(@Param("id") int id, @Param("lastElementId") int lastElementId, Pageable pageable);

    @Query("select fr from FriendRelation fr where fr.account1.id = :id and fr.relationStatus = \'REQUEST_OUT\' or " +
            "fr.account2.id = :id and fr.relationStatus = \'REQUEST_IN\'")
    List<FriendRelation> getFriendRelationsRequestOutFriendship(@Param("id") int id, Pageable pageable);

    @Query("select fr from FriendRelation fr where (fr.account1.id = :id and fr.relationStatus = \'REQUEST_OUT\' or " +
            "fr.account2.id = :id and fr.relationStatus = \'REQUEST_IN\') and fr.id < :lastElementId")
    List<FriendRelation> getFriendRelationsRequestOutFriendship(@Param("id") int id, @Param("lastElementId") int lastElementId,
                                                                Pageable pageable);

    @Query("select fr from FriendRelation fr where fr.account1.id = :id and fr.relationStatus = \'REQUEST_IN\' or " +
            "fr.account2.id = :id and fr.relationStatus = \'REQUEST_OUT\'")
    List<FriendRelation> getFriendRelationsRequestInFriendship(@Param("id") int id, Pageable pageable);

    @Query("select fr from FriendRelation fr where (fr.account1.id = :id and fr.relationStatus = \'REQUEST_IN\' or " +
            "fr.account2.id = :id and fr.relationStatus = \'REQUEST_OUT\') and fr.id < :lastElementId")
    List<FriendRelation> getFriendRelationsRequestInFriendship(@Param("id") int id, @Param("lastElementId") int lastElementId,
                                                               Pageable pageable);

    @Modifying
    @Query("delete from FriendRelation fr where fr.account1.id = :id or fr.account2.id = :id")
    void removeOnAccountId(@Param("id") int id);
}
