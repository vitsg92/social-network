package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Group;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupDao extends JpaRepository<Group, Integer>, GenericDao<Group> {

    Group findById(int id);

    List<Group> findByCreatorId(int id, Pageable pageable);

    List<Group> findByCreatorIdAndIdLessThan(int id, int lastElementId, Pageable pageable);

    Group findByGroupName(String groupName);

    List<Group> findByGroupNameContainingIgnoreCase(String searchName, Pageable pageable);

    List<Group> findByGroupNameContainingIgnoreCase(String searchName);

    int countByGroupNameIgnoreCaseContaining(String searchName);

    @Query("select g.groupPhoto from Group g where g.id = :id")
    byte[] getGroupPhoto(@Param("id") int id);

    @Modifying
    void removeById(int groupId);

    @Modifying
    void removeAllByCreatorId(int id);
}
