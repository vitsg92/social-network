package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.message.MessageOnWall;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageOnWallDao extends JpaRepository<MessageOnWall, Integer> {

    List<MessageOnWall> getByDestinationIdAndMessageType(int id, MessageType messageType, Pageable pageable);

    List<MessageOnWall> getByDestinationIdAndMessageTypeAndIdLessThan(int id, MessageType messageType, int lastElementId, Pageable pageable);

    @Modifying
    @Query("delete from MessageOnWall m where m.writer.id = :id or m.destinationId = :id and m.messageType = \'ACCOUNT_WALL\'")
    void removeOnAccountId(@Param("id") int id);

    void removeAllByDestinationIdAndMessageType(int destinationId, MessageType messageType);
}
