package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Element;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class GenericDaoImpl<T extends Element> implements GenericDao<T> {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void detach(T element) {
        entityManager.detach(element);
    }

    @Override
    public void clear() {
        entityManager.clear();
    }

    @Override
    public void remove(T element) {
        entityManager.remove(element);
    }
}
