package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.ralation.GroupPrivilege;
import com.getjavajob.training.socialnetwork.common.ralation.GroupRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupRelationDao extends JpaRepository<GroupRelation, Integer>, GenericDao<GroupRelation> {

    @Query("select gr from GroupRelation gr where gr.account.id = :mainAccountId and gr.group.id = :groupId")
    GroupRelation getGroupRelation(@Param("mainAccountId") int mainAccountId, @Param("groupId") int groupId);

    @Query("select gr from GroupRelation gr join fetch gr.account where gr.group.id = :groupId and gr.relationStatus = :relationStatus")
    List<GroupRelation> getGroupMembersDetails(@Param("groupId") int groupId, @Param("relationStatus") RelationStatus relationStatus,
                                               Pageable pageable);

    @Query("select gr from GroupRelation gr join fetch gr.account where gr.group.id = :groupId and gr.relationStatus = :relationStatus " +
            "and gr.id < :lastElementId")
    List<GroupRelation> getGroupMembersDetails(@Param("groupId") int groupId, @Param("relationStatus") RelationStatus relationStatus,
                                               @Param("lastElementId") int lastElementId, Pageable pageable);

    @Query("select gr from GroupRelation gr join fetch gr.account where gr.group.id = :groupId and gr.relationStatus = \'FULL\' " +
            "and gr.groupPrivilege = :groupPrivilege")
    List<GroupRelation> getGroupMembersOnPrivilege(@Param("groupId") int groupId, @Param("groupPrivilege")
            GroupPrivilege groupPrivilege, Pageable pageable);

    @Query("select gr from GroupRelation gr join fetch gr.account where gr.group.id = :groupId and gr.relationStatus = \'FULL\' " +
            "and gr.groupPrivilege = :groupPrivilege and gr.id < :lastElementId")
    List<GroupRelation> getGroupMembersOnPrivilege(@Param("groupId") int groupId, @Param("groupPrivilege")
            GroupPrivilege groupPrivilege, @Param("lastElementId") int lastElementId, Pageable pageable);

    @Query("select gr from GroupRelation gr join fetch gr.group where gr.account.id = :accountId and gr.relationStatus = :relationStatus")
    List<GroupRelation> getAccountGroupRelations(@Param("accountId") int accountId, @Param("relationStatus") RelationStatus relationStatus, Pageable pageable);

    @Query("select gr from GroupRelation gr join fetch gr.group where gr.account.id = :accountId and gr.relationStatus = :relationStatus " +
            "and gr.id < :lastElementId")
    List<GroupRelation> getAccountGroupRelations(@Param("accountId") int accountId, @Param("relationStatus") RelationStatus relationStatus,
                                                 @Param("lastElementId") int lastElementId, Pageable pageable);

    @Modifying
    void removeAllByGroupId(int groupId);

    @Modifying
    void removeAllByAccountId(int id);
}
