package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountDao extends JpaRepository<Account, Integer>, GenericDao<Account> {

    @Modifying
    void removeById(int id);

    Account findById(int id);

    Account findByEmailAndPassword(String email, String password);

    Account findByEmail(String email);

    @Query("select a from Account a where lower(a.firstName) like lower(concat('%',:searchName,'%')) or lower(a.lastName)" +
            " like lower(concat('%',:searchName,'%'))")
    List<Account> searchAccountsByStringPagination(@Param("searchName") String searchName, Pageable pageable);

    @Query("select a from Account a where lower(a.firstName) like lower(concat('%',:searchName,'%')) or lower(a.lastName)" +
            " like lower(concat('%',:searchName,'%'))")
    List<Account> searchAccountsByString(@Param("searchName") String searchName);

    @Query("select count(a) from Account a where lower(a.firstName) like lower(concat('%',:searchName,'%')) or lower(a.lastName)" +
            " like lower(concat('%',:searchName,'%'))")
    int getCountAccountsWithSearchByString(@Param("searchName") String searchName);

    @Query("select a.photo from Account a where a.id = :id")
    byte[] getAccountPhoto(@Param("id") int id);
}
