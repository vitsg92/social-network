package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Element;

public interface GenericDao<T extends Element> {

    void detach(T element);

    void clear();

    void remove(T element);
}
