drop table if exists sn_private_messages;

drop table if exists sn_messages_on_wall;

drop table if exists sn_friend_relations;

drop table if exists sn_group_relations;

drop table if exists sn_phones;

drop table if exists sn_groups;

drop table if exists sn_accounts;

create table sn_accounts
(
    id               int auto_increment primary key,
    firstName        varchar(255),
    lastName         varchar(255),
    patronymic       varchar(255),
    email            varchar(255),
    dateOfBirth      varchar(255),
    dateRegistration varchar(255),
    icq              varchar(255),
    password         varchar(255),
    photo            longblob,
    role             varchar(255),
    skype            varchar(255)
);

create table sn_groups
(
    id               int auto_increment primary key,
    creator          int,
    dateRegistration varchar(255),
    description      varchar(255),
    groupName        varchar(255),
    groupPhoto       longblob
);

create table sn_friend_relations
(
    id             int auto_increment primary key,
    account1Id     int,
    account2Id     int,
    relationStatus varchar(255),
    foreign key (account1Id) references sn_accounts(id),
    foreign key (account2Id) references sn_accounts(id)
);

create table sn_messages_on_wall
(
    id             int auto_increment primary key,
    writerId       int,
    destinationId  int,
    dateOfWriting  varchar(255),
    messageContent varchar(255),
    messageType    varchar(255),
    foreign key (writerId) references sn_accounts(id)
);

create table sn_private_messages
(
    id             int auto_increment primary key,
    writerId       int,
    readerId       int,
    dateOfWriting  varchar(255),
    messageContent varchar(255),
    accepted       bit,
    foreign key (writerId) references sn_accounts(id),
    foreign key (readerId) references sn_accounts(id)
);

create table sn_phones
(
    id          int auto_increment primary key,
    ownerId     int,
    phoneNumber varchar(255),
    phoneType   varchar(255),
    foreign key (ownerId) references sn_accounts(id)
);

create table sn_group_relations
(
    id int not null auto_increment primary key,
    accountId           int,
    groupId             int,
    relationStatus      varchar(255),
    groupPrivilege  varchar(255),
    foreign key (accountId) references sn_accounts(id),
    foreign key (groupId) references sn_groups(id)
);



