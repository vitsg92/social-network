package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testDao.xml"})
public class GroupDaoTest {

    @Autowired
    private AccountDao ad;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private DataSource dataSource;
    private Pageable pageable = PageRequest.of(0, 10, Sort.by("id"));
    private Pageable pageableByGroupName = PageRequest.of(0, 2, Sort.by("groupName"));

    @Before
    public void onBefore() throws SQLException {
        Connection conn = dataSource.getConnection();
        RunScript.execute(conn, new InputStreamReader(requireNonNull(GroupDaoTest.class.getClassLoader().getResourceAsStream("schema.sql"))));
        conn.close();
        Group gr1 = new Group();
        Group gr2 = new Group();
        Account creator = new Account();
        creator.setFirstName("asd");
        creator.setLastName("asd");
        creator.setEmail("sad");
        creator.setPassword("asd");
        ad.save(creator);
        gr1.setGroupName("Books");
        gr1.setDateRegistration("asd");
        gr1.setCreator(creator);
        gr1.setDescription("fsdf");
        gr2.setGroupName("Cars");
        gr2.setDateRegistration("asd");
        gr2.setCreator(creator);
        gr2.setDescription("dfs");
        groupDao.save(gr1);
        groupDao.save(gr2);
        groupDao.flush();
        groupDao.clear();
    }

    @Test
    @Transactional
    public void testFindAll() {
        List<Group> groups = groupDao.findAll();
        assertEquals(2, groups.size());
    }

    @Test
    @Transactional
    public void testFindByIdTrue() {
        Group group = groupDao.findById(2);
        assertEquals("Cars", group.getGroupName());
    }

    @Test
    @Transactional
    public void testFindByIdFalse() {
        Group group = groupDao.findById(20);
        if (nonNull(group)) {
            fail();
        }
    }

    @Test
    @Transactional
    public void testSave1() {
        assertEquals(2, groupDao.findAll().size());
    }

    @Test
    @Transactional
    public void testSave2() {
        List<Group> groups = groupDao.findAll();
        assertEquals(2, groups.get(1).getId());
    }

    @Test
    @Transactional
    public void testSave3() {
        Account creator = new Account();
        creator.setFirstName("asd");
        creator.setLastName("asd");
        creator.setEmail("asd");
        creator.setPassword("asd");
        creator.setDateRegistration("asd");
        ad.save(creator);
        Group gr3 = new Group();
        gr3.setGroupName("TV");
        gr3.setCreator(creator);
        gr3.setDateRegistration("asd");
        Group result = groupDao.save(gr3);
        groupDao.detach(result);
        assertEquals(3, result.getId());
    }

    @Test
    @Transactional
    public void testSave4() {
        Group group = groupDao.findById(2);
        group.setGroupName("TV");
        groupDao.detach(groupDao.saveAndFlush(group));
        Group updatedGroup = groupDao.findById(2);
        if (nonNull(updatedGroup)) {
            assertEquals("TV", updatedGroup.getGroupName());
        } else {
            fail();
        }
    }

    @Test
    @Transactional
    public void testDelete() {
        Group group = groupDao.findById(2);
        if (nonNull(group)) {
            groupDao.delete(group);
            groupDao.flush();
        }
        groupDao.clear();
        assertEquals(1, groupDao.findAll().size());
    }

    @Test
    @Transactional
    public void testGetByCreator1() {
        Group gr1 = new Group();
        Account creator = new Account();
        creator.setDateRegistration("asdasd");
        creator.setFirstName("asdasd");
        creator.setLastName("asdasd");
        creator.setEmail("sadasd");
        creator.setPassword("asdasd");
        int creatorId = ad.save(creator).getId();
        gr1.setGroupName("Books1");
        gr1.setDateRegistration("asdasd");
        gr1.setCreator(creator);
        gr1.setDescription("fsdfasd");
        groupDao.save(gr1);
        List<Group> groups = groupDao.findByCreatorId(creatorId, pageable);
        assertEquals(gr1.getGroupName(), groups.get(0).getGroupName());
        assertEquals(0, groupDao.findByCreatorIdAndIdLessThan(creatorId, 1, pageable).size());
    }

    @Test
    @Transactional
    public void testGetByCreator2() {
        Account creator = new Account();
        creator.setDateRegistration("asdasd");
        creator.setFirstName("asdasd");
        creator.setLastName("asdasd");
        creator.setEmail("sadasd");
        creator.setPassword("asdasd");
        List<Group> groups = groupDao.findByCreatorId(ad.save(creator).getId(), pageable);
        assertEquals(0, groups.size());
    }

    @Test
    @Transactional
    public void testFindByGroupNameTrue() {
        Group group = groupDao.findByGroupName("Cars");
        if (isNull(group)) {
            fail();
        } else {
            assertEquals(2, group.getId());
        }
    }

    @Test
    @Transactional
    public void testFindByGroupNameFalse1() {
        Group group = groupDao.findByGroupName("cars");
        if (nonNull(group)) {
            fail();
        }
    }

    @Test
    @Transactional
    public void testFindByGroupNameFalse2() {
        Group group = groupDao.findByGroupName("ars");
        if (nonNull(group)) {
            fail();
        }
    }

    @Test
    @Transactional
    public void testFindByGroupNameContainingIgnoreCase1() {
        List<Group> groups = groupDao.findByGroupNameContainingIgnoreCase("s", pageableByGroupName);
        assertEquals(2, groups.size());
    }

    @Test
    @Transactional
    public void testFindByGroupNameContainingIgnoreCase2() {
        List<Group> groups = groupDao.findByGroupNameContainingIgnoreCase("rs", pageableByGroupName);
        assertEquals(1, groups.size());
    }

    @Test
    @Transactional
    public void testFindByGroupNameContainingIgnoreCase3() {
        List<Group> groups = groupDao.findByGroupNameContainingIgnoreCase("s", PageRequest.of(1, 2, Sort.by("groupName")));
        assertEquals(0, groups.size());
    }

    @Test
    @Transactional
    public void testFindByGroupNameContainingIgnoreCase4() {
        List<Group> groups = groupDao.findByGroupNameContainingIgnoreCase("S", pageableByGroupName);
        assertEquals(2, groups.size());
    }

    @Test
    @Transactional
    public void testCountByGroupNameIgnoreCaseContaining1() {
        int count = groupDao.countByGroupNameIgnoreCaseContaining("s");
        assertEquals(2, count);
    }

    @Test
    @Transactional
    public void testCountByGroupNameIgnoreCaseContaining2() {
        int count = groupDao.countByGroupNameIgnoreCaseContaining("rs");
        assertEquals(1, count);
    }

    @Test
    @Transactional
    public void testCountByGroupNameIgnoreCaseContaining3() {
        int count = groupDao.countByGroupNameIgnoreCaseContaining("S");
        assertEquals(2, count);
    }

    @Test
    @Transactional
    public void testGetGroupPhoto() {
        byte[] photo = groupDao.getGroupPhoto(33);
        if (nonNull(photo)) {
            fail();
        }
    }

    @Test
    @Transactional
    public void testRemoveById() {
        List<Group> groups = groupDao.findAll();
        int removingGroupId = groups.get(0).getId();
        groupDao.removeById(removingGroupId);
        List<Group> groupsAfterRemoving = groupDao.findAll();
        assertEquals(1, groupsAfterRemoving.size());
        assertTrue(groupsAfterRemoving.get(0).getId() != removingGroupId);
    }

    @Test
    @Transactional
    public void testRemoveAllByCreator() {
        List<Group> groups = groupDao.findAll();
        Account creator1 = groups.get(0).getCreator();
        Account creator2 = groups.get(1).getCreator();
        assertSame(creator1, creator2);
        groupDao.removeAllByCreatorId(creator1.getId());
        assertEquals(0, groupDao.findAll().size());
    }
}
