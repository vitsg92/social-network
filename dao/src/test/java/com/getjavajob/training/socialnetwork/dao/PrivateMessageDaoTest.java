package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.dto.Dialog;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testDao.xml"})
public class PrivateMessageDaoTest {
    private static final String FIRST_ACCOUNT_NAME = "Vasya";
    private static final int PAGE_SIZE = 10;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PrivateMessageDao privateMessageDao;

    @Autowired
    private AccountDao accountDao;

    @Before
    public void onBefore() throws SQLException {
        Connection conn = dataSource.getConnection();
        RunScript.execute(conn, new InputStreamReader(requireNonNull(AccountDaoTest.class.getClassLoader().getResourceAsStream("schema.sql"))));
        conn.close();
        Account acc1 = new Account();
        acc1.setFirstName(FIRST_ACCOUNT_NAME);
        acc1.setLastName("Petrov");
        acc1.setEmail("vasya@yandex.ru");
        acc1.setPassword("12345");
        accountDao.save(acc1);
        Account acc2 = new Account();
        acc2.setFirstName("Dima");
        acc2.setLastName("Ivanov");
        acc2.setEmail("dima@yandex.ru");
        acc2.setPassword("12345");
        accountDao.save(acc2);
        PrivateMessage privateMessageOut = new PrivateMessage();
        privateMessageOut.setWriter(acc1);
        privateMessageOut.setReader(acc2);
        privateMessageOut.setMessageContent("message out");
        privateMessageOut.setAccepted(true);
        privateMessageDao.save(privateMessageOut);
        PrivateMessage privateMessageIn = new PrivateMessage();
        privateMessageIn.setWriter(acc2);
        privateMessageIn.setReader(acc1);
        privateMessageIn.setMessageContent("message in");
        privateMessageIn.setAccepted(false);
        privateMessageDao.save(privateMessageIn);
        accountDao.flush();
        accountDao.clear();
    }

    @Test
    @Transactional
    public void testFindById() {
        assertEquals("message out", privateMessageDao.findById(1).getMessageContent());
    }

    @Test
    @Transactional
    public void testGetMessagesFromDialog() {
        List<Account> accounts = accountDao.findAll();
        assertEquals(2, privateMessageDao.getMessagesFromDialog(accounts.get(0).getId(), accounts.get(1).getId(),
                PAGE_SIZE).size());
        assertEquals(2, privateMessageDao.getMessagesFromDialog(accounts.get(1).getId(), accounts.get(0).getId(),
                PAGE_SIZE).size());
        assertEquals(1, privateMessageDao.getMessagesFromDialog(accounts.get(1).getId(), accounts.get(0).getId(),
                PAGE_SIZE, 2).size());
        assertEquals(0, privateMessageDao.getMessagesFromDialog(accounts.get(1).getId(), accounts.get(0).getId(),
                PAGE_SIZE, 1).size());
    }

    @Test
    @Transactional
    public void testGetByReaderAndAccepted() {
        List<Account> accounts = accountDao.findAll();
        int account1Id = accounts.get(0).getId();
        int account2Id = accounts.get(1).getId();
        List<PrivateMessage> messages1 = privateMessageDao.getByReaderIdAndAccepted(account2Id, true);
        assertEquals(1, messages1.size());
        assertEquals("message out", messages1.get(0).getMessageContent());
        List<PrivateMessage> messages2 = privateMessageDao.getByReaderIdAndAccepted(account2Id, false);
        assertEquals(0, messages2.size());
        List<PrivateMessage> messages3 = privateMessageDao.getByReaderIdAndAccepted(account1Id, false);
        assertEquals(1, messages3.size());
        assertEquals("message in", messages3.get(0).getMessageContent());
        List<PrivateMessage> messages4 = privateMessageDao.getByReaderIdAndAccepted(account1Id, true);
        assertEquals(0, messages4.size());
    }

    @Test
    @Transactional
    public void testUpdateAcceptedMessages() {
        List<PrivateMessage> messages = privateMessageDao.findAll();
        List<Integer> messagesId = new ArrayList<>();
        for (PrivateMessage message : messages) {
            messagesId.add(message.getId());
        }
        privateMessageDao.updateAcceptedMessages(messagesId);
        List<Account> accounts = accountDao.findAll();
        assertEquals(0, privateMessageDao.getByReaderIdAndAccepted(accounts.get(0).getId(), false).size());
        assertEquals(0, privateMessageDao.getByReaderIdAndAccepted(accounts.get(1).getId(), false).size());
    }

    @Test
    @Transactional
    public void testGetDialogs() {
        List<Account> accounts = accountDao.findAll();
        List<Dialog> dialogs = privateMessageDao.getDialogs(accounts.get(0).getId(), PAGE_SIZE);
        assertEquals(1, dialogs.size());
        assertEquals(2, dialogs.get(0).getAccountId());
        assertEquals(0, privateMessageDao.getDialogs(accounts.get(0).getId(), PAGE_SIZE, 1).size());
    }

    @Test
    @Transactional
    public void testGetCountUnacceptedMessagesOnDialog() {
        List<Account> accounts = accountDao.findAll();
        int account1Id = accounts.get(0).getId();
        int account2Id = accounts.get(1).getId();
        assertEquals(1, privateMessageDao.getCountUnacceptedMessagesOnDialog(account1Id, account2Id));
        assertEquals(0, privateMessageDao.getCountUnacceptedMessagesOnDialog(account2Id, account1Id));
    }

    @Test
    @Transactional
    public void testRemoveOnAccountId() {
        List<Account> accounts = accountDao.findAll();
        int account1Id = accounts.get(0).getId();
        int account2Id = accounts.get(1).getId();
        privateMessageDao.removeOnAccountId(account1Id);
        assertEquals(0, privateMessageDao.getMessagesFromDialog(account1Id, account2Id, PAGE_SIZE).size());
    }
}
