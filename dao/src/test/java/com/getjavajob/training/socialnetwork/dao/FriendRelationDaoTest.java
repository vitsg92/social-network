package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.ralation.FriendRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testDao.xml"})
public class FriendRelationDaoTest {
    private static final String FIRST_ACCOUNT_NAME = "Vasya";

    @Autowired
    private DataSource dataSource;

    @Autowired
    private FriendRelationDao friendRelationDao;

    @Autowired
    private AccountDao accountDao;
    private Pageable pageable = PageRequest.of(0, 10, Sort.by("id"));

    @Before
    public void onBefore() throws SQLException {
        Connection conn = dataSource.getConnection();
        RunScript.execute(conn, new InputStreamReader(requireNonNull(AccountDaoTest.class.getClassLoader().getResourceAsStream("schema.sql"))));
        conn.close();
        Account acc1 = new Account();
        Account acc2 = new Account();
        acc1.setFirstName(FIRST_ACCOUNT_NAME);
        acc1.setLastName("Petrov");
        acc1.setEmail("vasya@yandex.ru");
        acc1.setPassword("12345");
        acc2.setFirstName("Petya");
        acc2.setLastName("Ivanov");
        acc2.setEmail("petya@gmail.com");
        acc2.setPassword("123");
        accountDao.save(acc1);
        accountDao.save(acc2);
        FriendRelation friendRelation = new FriendRelation();
        friendRelation.setAccount1(acc1);
        friendRelation.setAccount2(acc2);
        friendRelation.setRelationStatus(RelationStatus.REQUEST_OUT);
        friendRelationDao.save(friendRelation);
        accountDao.flush();
        accountDao.clear();
    }

    @Test
    @Transactional
    public void testGetFriendRelation() {
        List<Account> accounts = accountDao.findAll();
        int accountId1 = accounts.get(0).getId();
        int accountId2 = accounts.get(1).getId();
        FriendRelation friendRelation1 = friendRelationDao.getFriendRelation(accountId1, accountId2);
        assertNotNull(friendRelation1);
        assertEquals(FIRST_ACCOUNT_NAME, friendRelation1.getAccount1().getFirstName());
        assertEquals(RelationStatus.REQUEST_OUT, friendRelation1.getRelationStatus());
        FriendRelation friendRelation2 = friendRelationDao.getFriendRelation(accountId2, accountId1);
        assertNotNull(friendRelation2);
        assertEquals(FIRST_ACCOUNT_NAME, friendRelation2.getAccount1().getFirstName());
        assertEquals(RelationStatus.REQUEST_OUT, friendRelation2.getRelationStatus());
    }

    @Test
    @Transactional
    public void testGetFriendRelationsFriend() {
        List<Account> accounts = accountDao.findAll();
        int accountId1 = accounts.get(0).getId();
        int accountId2 = accounts.get(1).getId();
        FriendRelation friendRelation = friendRelationDao.getFriendRelation(accountId1, accountId2);
        friendRelation.setRelationStatus(RelationStatus.FULL);
        List<FriendRelation> friends = friendRelationDao.getFriendRelationsFriend(accountId1, pageable);
        assertNotNull(friends);
        assertEquals(accountId2, friends.get(0).getAccount2().getId());
        assertEquals(0, friendRelationDao.getFriendRelationsFriend(accountId1, 1, pageable).size());
        assertEquals(1, friendRelationDao.getFriendRelationsFriend(accountId1, 2, pageable).size());
    }

    @Test
    @Transactional
    public void testGetFriendRelationsRequestOutFriendship() {
        List<Account> accounts = accountDao.findAll();
        int accountId1 = accounts.get(0).getId();
        int accountId2 = accounts.get(1).getId();
        List<FriendRelation> friendsId = friendRelationDao.getFriendRelationsRequestOutFriendship(accountId1, pageable);
        assertNotNull(friendsId);
        assertEquals(accountId2, friendsId.get(0).getAccount2().getId());
        assertEquals(0, friendRelationDao.getFriendRelationsRequestOutFriendship(accountId1, 1, pageable).size());
        assertEquals(1, friendRelationDao.getFriendRelationsRequestOutFriendship(accountId1, 2, pageable).size());
    }

    @Test
    @Transactional
    public void testGetFriendRelationsRequestInFriendship() {
        List<Account> accounts = accountDao.findAll();
        int accountId1 = accounts.get(0).getId();
        int accountId2 = accounts.get(1).getId();
        List<FriendRelation> friendsId = friendRelationDao.getFriendRelationsRequestInFriendship(accountId2, pageable);
        assertNotNull(friendsId);
        assertEquals(accountId1, friendsId.get(0).getAccount1().getId());
        assertEquals(0, friendRelationDao.getFriendRelationsRequestInFriendship(accountId2, 1, pageable).size());
        assertEquals(1, friendRelationDao.getFriendRelationsRequestInFriendship(accountId2, 2, pageable).size());
    }

    @Test
    @Transactional
    public void testRemoveOnAccount() {
        List<Account> accounts = accountDao.findAll();
        int accountId1 = accounts.get(0).getId();
        int accountId2 = accounts.get(1).getId();
        friendRelationDao.removeOnAccountId(accountId1);
        FriendRelation friendRelation = friendRelationDao.getFriendRelation(accountId1, accountId2);
        assertTrue(isNull(friendRelation));
    }
}
