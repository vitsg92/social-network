package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Phone;
import com.getjavajob.training.socialnetwork.common.PhoneType;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testDao.xml"})
public class AccountDaoTest {
    private static final String NAME_TEST = "Dima";
    private static final String LAST_NAME_TEST = "Vasilyev";
    private static final String PATRONYMIC_TEST = "Viktorovich";
    private static final String TEST_NAME = "Vasya";
    private static final String TEST_EMAIL = "vasya@gmail.com";
    private static final String TEST_PWD = "1234";

    private Pageable pageable = PageRequest.of(0, 2, Sort.by("firstName"));

    @Autowired
    private DataSource dataSource;

    @Autowired
    private AccountDao ad;

    @Before
    public void onBefore() throws SQLException {
        Connection conn = dataSource.getConnection();
        RunScript.execute(conn, new InputStreamReader(requireNonNull(AccountDaoTest.class.getClassLoader().getResourceAsStream("schema.sql"))));
        conn.close();
        Account acc1 = new Account();
        acc1.setFirstName(TEST_NAME);
        acc1.setLastName("Petrov");
        acc1.setEmail(TEST_EMAIL);
        acc1.setPassword(TEST_PWD);
        List<Phone> phonesAcc1 = new ArrayList<>();
        Phone a = new Phone();
        a.setOwner(acc1);
        a.setPhoneNumber("11111111111");
        a.setPhoneType(PhoneType.HOME);
        phonesAcc1.add(a);
        acc1.setPhones(phonesAcc1);
        Account acc2 = new Account();
        acc2.setFirstName("Petya");
        acc2.setLastName("Ivanov");
        acc2.setEmail("petya@gmail.com");
        acc2.setPassword("123");
        List<Phone> phonesAcc2 = new ArrayList<>();
        Phone b = new Phone();
        b.setOwner(acc1);
        b.setPhoneNumber("11111111111");
        b.setPhoneType(PhoneType.HOME);
        phonesAcc2.add(b);
        acc1.setPhones(phonesAcc2);
        ad.save(acc1);
        ad.save(acc2);
        ad.flush();
        ad.clear();
    }

    @Test
    @Transactional
    public void testFindById1() {
        Account account = ad.findById(2);
        assertEquals("petya@gmail.com", account.getEmail());
    }

    @Test
    @Transactional
    public void testFindById2() {
        Account acc = ad.findById(100);
        assertNull(acc);
    }


    @Test
    @Transactional
    public void testInsertAccount1() {
        assertEquals(2, ad.findAll().size());
    }

    @Test
    @Transactional
    public void testInsertAccount2() {
        assertEquals(2, ad.findAll().get(1).getId());
    }

    @Test
    @Transactional
    public void testInsertAccount3() {
        Account acc3 = new Account();
        acc3.setFirstName(NAME_TEST);
        acc3.setLastName(LAST_NAME_TEST);
        acc3.setPatronymic(PATRONYMIC_TEST);
        acc3.setEmail("asd");
        acc3.setPassword("ged");
        acc3.setDateRegistration("rger");
        Account result = ad.save(acc3);
        assertEquals(3, result.getId());
        assertNull(result.getIcq());
    }

    @Test
    @Transactional
    public void testUpdateAccount1() {
        Account acc = ad.findById(2);
        acc.setEmail("petya-new@gmail.com");
        ad.save(acc);
        ad.flush();
        ad.clear();
        assertEquals("petya-new@gmail.com", ad.findById(2).getEmail());
    }

    @Test
    @Transactional
    public void testDeleteAccount1() {
        Account acc = ad.findById(2);
        ad.delete(acc);
        ad.flush();
        ad.clear();
        assertEquals(1, ad.findAll().size());
    }

    @Test
    @Transactional
    public void testFindByEmailAndPassword1() {
        Account account = ad.findByEmailAndPassword(TEST_EMAIL, TEST_PWD);
        assertEquals(TEST_NAME, account.getFirstName());
    }

    @Test
    @Transactional
    public void testFindByEmailAndPassword2() {
        Account account = ad.findByEmailAndPassword("asdfvcx", "dfasfd");
        assertTrue(isNull(account));
    }

    @Test
    @Transactional
    public void testFindByEmailTrue() {
        Account account = ad.findByEmail("dfasd");
        if (nonNull(account)) {
            fail();
        }
    }

    @Test
    @Transactional
    public void testFindByEmailFalse() {
        assertEquals(1, ad.findByEmail(TEST_EMAIL).getId());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination1() {
        List<Account> accounts = ad.searchAccountsByStringPagination("petya", pageable);
        assertEquals(1, accounts.size());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination2() {
        List<Account> accounts = ad.searchAccountsByStringPagination("etya", pageable);
        assertEquals(1, accounts.size());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination3() {
        List<Account> accounts = ad.searchAccountsByStringPagination("ya", pageable);
        assertEquals(2, accounts.size());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination4() {
        List<Account> accounts = ad.searchAccountsByStringPagination("ya", PageRequest.of(1, 2, Sort.by("firstName")));
        assertEquals(0, accounts.size());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination5() {
        List<Account> accounts = ad.searchAccountsByStringPagination("asd", pageable);
        assertEquals(0, accounts.size());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination6() {
        List<Account> accounts = ad.searchAccountsByStringPagination("ya", PageRequest.of(0, 1, Sort.by("firstName")));
        assertEquals(1, accounts.size());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination7() {
        List<Account> accounts = ad.searchAccountsByStringPagination("ya", PageRequest.of(1, 1, Sort.by("firstName")));
        assertEquals(1, accounts.size());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination8() {
        List<Account> accounts = ad.searchAccountsByStringPagination("ya", PageRequest.of(1, 1, Sort.by("firstName")));
        assertEquals(TEST_NAME, accounts.get(0).getFirstName());
    }

    @Test
    @Transactional
    public void testSearchAccountsByStringPagination9() {
        List<Account> accounts = ad.searchAccountsByStringPagination("ya", PageRequest.of(0, 1, Sort.by("firstName")));
        assertEquals("Petya", accounts.get(0).getFirstName());
    }

    @Test
    @Transactional
    public void testGetCountAccountsWithSearchByString1() {
        assertEquals(1, ad.getCountAccountsWithSearchByString("petya"));
    }

    @Test
    @Transactional
    public void testGetCountAccountsWithSearchByString2() {
        assertEquals(1, ad.getCountAccountsWithSearchByString("etya"));
    }

    @Test
    @Transactional
    public void testGetCountAccountsWithSearchByString3() {
        assertEquals(2, ad.getCountAccountsWithSearchByString("ya"));
    }
}
