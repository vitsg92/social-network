package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.ralation.GroupPrivilege;
import com.getjavajob.training.socialnetwork.common.ralation.GroupRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testDao.xml"})
public class GroupRelationDaoTest {
    private static final String FIRST_ACCOUNT_NAME = "Vasya";
    private static final String GROUP_NAME = "First group";
    private Pageable pageable = PageRequest.of(0, 10, Sort.by("id"));

    @Autowired
    private DataSource dataSource;

    @Autowired
    private GroupRelationDao groupRelationDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private GroupDao groupDao;

    @Before
    public void onBefore() throws SQLException {
        Connection conn = dataSource.getConnection();
        RunScript.execute(conn, new InputStreamReader(requireNonNull(AccountDaoTest.class.getClassLoader().getResourceAsStream("schema.sql"))));
        conn.close();
        Account acc1 = new Account();
        acc1.setFirstName(FIRST_ACCOUNT_NAME);
        acc1.setLastName("Petrov");
        acc1.setEmail("vasya@yandex.ru");
        acc1.setPassword("12345");
        accountDao.save(acc1);
        Group group = new Group();
        group.setGroupName(GROUP_NAME);
        groupDao.save(group);
        groupRelationDao.save(new GroupRelation(acc1, group, RelationStatus.REQUEST_OUT, GroupPrivilege.USER));
        groupRelationDao.flush();
        groupRelationDao.clear();
    }

    @Test
    @Transactional
    public void testGetGroupRelation() {
        int accountId = accountDao.findAll().get(0).getId();
        int groupId = groupDao.findAll().get(0).getId();
        GroupRelation groupRelation = groupRelationDao.getGroupRelation(accountId, groupId);
        assertNotNull(groupRelation);
        assertEquals(RelationStatus.REQUEST_OUT, groupRelation.getRelationStatus());
    }

    @Test
    @Transactional
    public void testGetGroupMembersDetails() {
        int groupId = groupDao.findAll().get(0).getId();
        Account account = groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.REQUEST_OUT, pageable).get(0).getAccount();
        assertNotNull(account);
        assertEquals(FIRST_ACCOUNT_NAME, account.getFirstName());
        List<GroupRelation> groupRelations = groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.FULL, pageable);
        assertNotNull(groupRelations);
        assertEquals(0, groupRelations.size());
        int accountId = accountDao.findAll().get(0).getId();
        groupRelationDao.getGroupRelation(accountId, groupId).setRelationStatus(RelationStatus.FULL);
        assertEquals(FIRST_ACCOUNT_NAME, groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.FULL, pageable)
                .get(0).getAccount().getFirstName());
        assertEquals(0, groupRelationDao.getGroupMembersDetails(groupId, RelationStatus.FULL, 1, pageable).size());
    }

    @Test
    @Transactional
    public void testGetGroupMembersOnRole() {
        int groupId = groupDao.findAll().get(0).getId();
        int accountId = accountDao.findAll().get(0).getId();
        GroupRelation groupRelation = groupRelationDao.getGroupRelation(accountId, groupId);
        groupRelation.setRelationStatus(RelationStatus.FULL);
        assertEquals(FIRST_ACCOUNT_NAME, groupRelationDao.getGroupMembersOnPrivilege(groupId, GroupPrivilege.USER, pageable)
                .get(0).getAccount().getFirstName());
        List<GroupRelation> groupRelations = groupRelationDao.getGroupMembersOnPrivilege(groupId, GroupPrivilege.MODER, pageable);
        assertNotNull(groupRelations);
        assertEquals(0, groupRelations.size());
        groupRelation.setGroupPrivilege(GroupPrivilege.MODER);
        assertEquals(FIRST_ACCOUNT_NAME, groupRelationDao.getGroupMembersOnPrivilege(groupId, GroupPrivilege.MODER, pageable)
                .get(0).getAccount().getFirstName());
        assertEquals(0, groupRelationDao.getGroupMembersOnPrivilege(groupId, GroupPrivilege.MODER, 1, pageable).size());
    }

    @Test
    @Transactional
    public void testGetAccountGroups() {
        int groupId = groupDao.findAll().get(0).getId();
        Account account = accountDao.findAll().get(0);
        int accountId = account.getId();
        assertEquals(GROUP_NAME, groupRelationDao.getAccountGroupRelations(accountId, RelationStatus.REQUEST_OUT, pageable)
                .get(0).getGroup().getGroupName());
        assertEquals(0, groupRelationDao.getAccountGroupRelations(accountId, RelationStatus.FULL, pageable).size());
        groupRelationDao.getGroupRelation(accountId, groupId).setRelationStatus(RelationStatus.FULL);
        assertEquals(0, groupRelationDao.getAccountGroupRelations(accountId, RelationStatus.REQUEST_OUT, pageable).size());
        assertEquals(GROUP_NAME, groupRelationDao.getAccountGroupRelations(accountId, RelationStatus.FULL, pageable)
                .get(0).getGroup().getGroupName());
        assertEquals(0, groupRelationDao.getGroupMembersOnPrivilege(groupId, GroupPrivilege.MODER, 1, pageable).size());
    }

    @Test
    @Transactional
    public void testRemoveByGroupId() {
        int groupId = groupDao.findAll().get(0).getId();
        int accountId = accountDao.findAll().get(0).getId();
        groupRelationDao.removeAllByGroupId(groupId);
        assertTrue(isNull(groupRelationDao.getGroupRelation(accountId, groupId)));
    }

    @Test
    @Transactional
    public void testRemoveAllByAccountId() {
        int groupId = groupDao.findAll().get(0).getId();
        int accountId = accountDao.findAll().get(0).getId();
        groupRelationDao.removeAllByAccountId(accountId);
        assertTrue(isNull(groupRelationDao.getGroupRelation(accountId, groupId)));
    }
}
