package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.message.MessageOnWall;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testDao.xml"})
public class MessageOnWallDaoTest {
    private static final String FIRST_ACCOUNT_NAME = "Vasya";
    private Pageable pageable = PageRequest.of(0, 10, Sort.by("id"));

    @Autowired
    private DataSource dataSource;

    @Autowired
    private MessageOnWallDao messageOnWallDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private GroupDao groupDao;

    @Before
    public void onBefore() throws SQLException {
        Connection conn = dataSource.getConnection();
        RunScript.execute(conn, new InputStreamReader(requireNonNull(AccountDaoTest.class.getClassLoader().getResourceAsStream("schema.sql"))));
        conn.close();
        Account acc1 = new Account();
        acc1.setFirstName(FIRST_ACCOUNT_NAME);
        acc1.setLastName("Petrov");
        acc1.setEmail("vasya@yandex.ru");
        acc1.setPassword("12345");
        accountDao.save(acc1);
        Account acc2 = new Account();
        acc2.setFirstName("Dima");
        acc2.setLastName("Ivanov");
        acc2.setEmail("dima@yandex.ru");
        acc2.setPassword("12345");
        accountDao.save(acc2);
        Group group = new Group();
        group.setGroupName("First group");
        groupDao.save(group);
        MessageOnWall messageOnWallGroup = new MessageOnWall();
        messageOnWallGroup.setWriter(acc1);
        messageOnWallGroup.setDestinationId(group.getId());
        messageOnWallGroup.setMessageType(MessageType.GROUP_WALL);
        messageOnWallGroup.setMessageContent("message on group");
        messageOnWallDao.save(messageOnWallGroup);
        MessageOnWall messageOnWallAccount = new MessageOnWall();
        messageOnWallAccount.setWriter(acc1);
        messageOnWallAccount.setDestinationId(acc2.getId());
        messageOnWallAccount.setMessageType(MessageType.ACCOUNT_WALL);
        messageOnWallAccount.setMessageContent("message on account");
        messageOnWallDao.save(messageOnWallAccount);
        accountDao.flush();
        accountDao.clear();
    }

    @Test
    @Transactional
    public void testGetByDestinationIdAndMessageType() {
        List<Account> accounts = accountDao.findAll();
        int destinationAccountId;
        Account firstAccountInDb = accounts.get(0);
        Account secondAccountInDb = accounts.get(1);
        if (firstAccountInDb.getFirstName().equals(FIRST_ACCOUNT_NAME)) {
            destinationAccountId = secondAccountInDb.getId();
        } else {
            destinationAccountId = firstAccountInDb.getId();
        }
        List<MessageOnWall> messagesOnAccountWall = messageOnWallDao.getByDestinationIdAndMessageType(destinationAccountId, MessageType.ACCOUNT_WALL, pageable);
        assertEquals(1, messagesOnAccountWall.size());
        assertNotNull(messagesOnAccountWall.get(0));
        assertEquals("message on account", messagesOnAccountWall.get(0).getMessageContent());
        List<MessageOnWall> testMessagesAccountWall = messageOnWallDao.getByDestinationIdAndMessageType(destinationAccountId, MessageType.GROUP_WALL, pageable);
        if (!testMessagesAccountWall.isEmpty() && !testMessagesAccountWall.get(0).getMessageContent().equals("message on group")) {
            fail();
        }
        int groupId = groupDao.findAll().get(0).getId();
        List<MessageOnWall> messagesOnGroupWall = messageOnWallDao.getByDestinationIdAndMessageType(groupId, MessageType.GROUP_WALL, pageable);
        assertEquals(1, messagesOnGroupWall.size());
        assertNotNull(messagesOnGroupWall.get(0));
        assertEquals("message on group", messagesOnGroupWall.get(0).getMessageContent());
        List<MessageOnWall> testMessagesGroupWall = messageOnWallDao.getByDestinationIdAndMessageType(groupId, MessageType.ACCOUNT_WALL, pageable);
        if (!testMessagesGroupWall.isEmpty() && !testMessagesGroupWall.get(0).getMessageContent().equals("message on account")) {
            fail();
        }
    }

    @Test
    @Transactional
    public void testRemoveOnAccountId1() {
        messageOnWallDao.removeOnAccountId(accountDao.findAll().get(0).getId());
        assertEquals(0, messageOnWallDao.findAll().size());
    }

    @Test
    @Transactional
    public void testRemoveOnAccountId2() {
        messageOnWallDao.removeOnAccountId(accountDao.findAll().get(1).getId());
        assertEquals(1, messageOnWallDao.findAll().size());
    }

    @Test
    @Transactional
    public void testRemoveAllByDestinationIdAndAndMessageType1() {
        int groupId = groupDao.findAll().get(0).getId();
        messageOnWallDao.removeAllByDestinationIdAndMessageType(groupId, MessageType.GROUP_WALL);
        assertEquals(0, messageOnWallDao.getByDestinationIdAndMessageType(groupId, MessageType.GROUP_WALL, pageable).size());
        assertEquals("message on account", messageOnWallDao.findAll().get(0).getMessageContent());
    }

    @Test
    @Transactional
    public void testRemoveAllByDestinationIdAndAndMessageType2() {
        int accountId = accountDao.findAll().get(1).getId();
        messageOnWallDao.removeAllByDestinationIdAndMessageType(accountId, MessageType.ACCOUNT_WALL);
        assertEquals(0, messageOnWallDao.getByDestinationIdAndMessageType(accountId, MessageType.ACCOUNT_WALL, pageable).size());
        assertEquals("message on group", messageOnWallDao.findAll().get(0).getMessageContent());
    }
}
