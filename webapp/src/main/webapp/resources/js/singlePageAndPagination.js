$(function () {
    var lock = false;
    var browserWindow = $(window);
    var endElement = false;
    var targetGroupId = $('#targetGroupId');
    var targetAccountId = $('#targetAccountId');
    var lastElementId = $('#lastElementId').val();
    var groupsButton = '#groupsButton';
    var requestGroupsButton = '#requestGroupsButton';
    var createdGroupsButton = '#createdGroupsButton';
    var friendsButton = '#friendsButton';
    var requestOutFriendshipAccountsButton = '#requestOutFriendshipAccountsButton';
    var requestInFriendshipAccountsButton = '#requestInFriendshipAccountsButton';
    var groupModeratorsButton = '#groupModeratorsButton';
    var addModeratorsButton = '#addModeratorsButton';
    var groupsContainer = '#groups';
    var requestGroupsContainer = '#requestGroups';
    var createdGroupsContainer = '#createdGroups';
    var friendsContainer = '#friends';
    var requestOutFriendshipAccountsContainer = '#requestOutFriendshipAccounts';
    var requestInFriendshipAccountsContainer = '#requestInFriendshipAccounts';
    var groupModeratorsContainer = '#groupModerators';
    var groupUsersContainer = '#groupUsers';
    var typePageFriends = 'friends';
    var typePageGroups = 'groups';
    var typePageGroupModerator = 'moderators';
    var accountType = 'account';
    var groupType = 'group';

    $(groupsButton).click(function () {
        showPage({
            type: groupType,
            typePage: typePageGroups,
            url: getPrefix() + '/groupsPagination',
            action: 'outOfGroup',
            button: groupsButton,
            container: groupsContainer,
            ajaxData: {id: targetAccountId.val()},
            title: 'Группы',
            altTitle: 'Групп не найдено'
        });
    });

    $(requestGroupsButton).click(function () {
        showPage({
            type: groupType,
            typePage: typePageGroups,
            url: getPrefix() + '/requestGroupsPagination',
            action: 'removeRequestToGroup',
            button: requestGroupsButton,
            container: requestGroupsContainer,
            ajaxData: {id: targetAccountId.val()},
            title: 'Заявки в группы',
            altTitle: 'Заявок в группы не найдено'
        });
    });

    $(createdGroupsButton).click(function () {
        showPage({
            type: groupType,
            typePage: typePageGroups,
            url: getPrefix() + '/createdGroupsPagination',
            button: createdGroupsButton,
            container: createdGroupsContainer,
            ajaxData: {id: targetAccountId.val()},
            title: 'Созданные группы',
            altTitle: 'Созданных групп не найдено'
        });
    });

    $(friendsButton).click(function () {
        showPage({
            type: accountType,
            typePage: typePageFriends,
            url: getPrefix() + '/friendsPagination',
            action: 'removeFriend',
            button: friendsButton,
            container: friendsContainer,
            ajaxData: {id: targetAccountId.val()},
            title: 'Друзья',
            altTitle: 'Друзья не найдены'
        });
    });

    $(requestOutFriendshipAccountsButton).click(function () {
        showPage({
            type: accountType,
            typePage: typePageFriends,
            url: getPrefix() + '/requestOutFriendshipAccountsPagination',
            action: 'removeFriendshipRequest',
            button: requestOutFriendshipAccountsButton,
            container: requestOutFriendshipAccountsContainer,
            ajaxData: {id: targetAccountId.val()},
            title: 'Исходящие заявки в друзья',
            altTitle: 'Исходящих заявок в друзья не найдено'
        });
    });

    $(requestInFriendshipAccountsButton).click(function () {
        showPage({
            type: accountType,
            typePage: typePageFriends,
            url: getPrefix() + '/requestInFriendshipAccountsPagination',
            action: 'acceptFriendship',
            button: requestInFriendshipAccountsButton,
            container: requestInFriendshipAccountsContainer,
            ajaxData: {id: targetAccountId.val()},
            title: 'Входящие заявки в друзья',
            altTitle: 'Входящих заявок в друзья не найдено'
        });
    });

    $(groupModeratorsButton).click(function () {
        showPage({
            type: accountType,
            typePage: typePageGroupModerator,
            url: getPrefix() + '/groupModeratorsPagination',
            action: 'removeGroupModerator',
            button: groupModeratorsButton,
            container: groupModeratorsContainer,
            ajaxData: {groupId: targetGroupId.val()},
            title: 'Модераторы',
            altTitle: 'Модераторов не найдено'
        });
    });

    $(addModeratorsButton).click(function () {
        showPage({
            type: accountType,
            typePage: typePageGroupModerator,
            url: getPrefix() + '/groupUsersPagination',
            action: 'addGroupModerator',
            button: addModeratorsButton,
            container: groupUsersContainer,
            ajaxData: {groupId: targetGroupId.val()},
            title: 'Участники группы с привилегиями пользователя',
            altTitle: 'Участников группы с привилегиями пользователя не найдено'
        });
    });

    function showPage(details) {
        endElement = false;
        $.get(details.url, details.ajaxData).then(function (elements) {
            var title = '';
            if (elements.length) {
                title = details.title;
                lastElementId = elements[elements.length - 1].id;
            } else {
                title = details.altTitle;
            }
            var html = '';
            $.each(elements, function (index, value) {
                html += generateHtml(value, details.action, details.type);
            });
            var container = details.container;
            var button = details.button;
            var typePage = details.typePage;
            $(container).empty();
            $(container).append('<h6 class="mt-3">' + title + '</h6>');
            $(container).append(html);
            if (typePage === typePageFriends) {
                showFriendsPage($(button), $(container));
            } else if (typePage === typePageGroups) {
                showGroupsPage($(button), $(container));
            } else if (typePage === typePageGroupModerator) {
                showModeratorsPage($(button), $(container));
            }
        });
    }

    function showGroupsPage(button, page) {
        $(groupsButton).removeClass('active');
        $(requestGroupsButton).removeClass('active');
        $(createdGroupsButton).removeClass('active');
        $(groupsContainer).addClass('d-none');
        $(requestGroupsContainer).addClass('d-none');
        $(createdGroupsContainer).addClass('d-none');
        button.addClass('active');
        page.removeClass('d-none')
    }

    function showFriendsPage(button, page) {
        $(friendsButton).removeClass('active');
        $(requestOutFriendshipAccountsButton).removeClass('active');
        $(requestInFriendshipAccountsButton).removeClass('active');
        $(friendsContainer).addClass('d-none');
        $(requestOutFriendshipAccountsContainer).addClass('d-none');
        $(requestInFriendshipAccountsContainer).addClass('d-none');
        button.addClass('active');
        page.removeClass('d-none')
    }

    function showModeratorsPage(button, page) {
        $(groupModeratorsButton).removeClass('active');
        $(addModeratorsButton).removeClass('active');
        $(groupModeratorsContainer).addClass('d-none');
        $(groupUsersContainer).addClass('d-none');
        button.addClass('active');
        page.removeClass('d-none')
    }

    browserWindow.scroll(function () {
        if (browserWindow.scrollTop() === $(document).height() - browserWindow.height() && endElement === false && lock === false) {
            var ajaxDetails = getAjaxDetailsForScrollPagination();
            loadAndAppendElements(ajaxDetails);
        }
    });

    function getAjaxDetailsForScrollPagination() {
        var ajaxDetails = {};
        $.each(getAjaxDetailsCases(), function (index, value) {
            if ($(value.container).length && !$(value.container).hasClass('d-none')) {
                ajaxDetails = value;
                return false;
            }
        });
        return ajaxDetails;
    }

    function loadAndAppendElements(ajaxDetails) {
        $.ajax({
            url: ajaxDetails.url,
            data: {
                lastElementId: lastElementId
            },
            beforeSend: function () {
                $('.preloader').append('<div class="spinner-border text-primary mt-2" role="status">' +
                    '<span class="sr-only">Loading...</span>' +
                    '</div>');
                lock = true;
            }
        }).then(function (elements) {
            $('.preloader').empty();
            if (elements.length !== 0) {
                appendElements(elements, ajaxDetails);
                lastElementId = elements[elements.length - 1].id;
            } else {
                endElement = true;
            }
            lock = false;
        })
    }

    function appendElements(elements, ajaxDetails) {
        var html = '';
        $.each(elements, function (index, value) {
            html += generateHtml(value, ajaxDetails.action, ajaxDetails.type);
        });
        $(ajaxDetails.container).append(html);
    }

    function getAjaxDetailsCases() {
        return [
            {
                container: '#dialogs',
                url: getPrefix() + '/openedDialogsPagination'
            },
            {
                container: '#messagesOnAccountWall',
                url: getPrefix() + '/getMessagesOnWallPagination?id=' + targetAccountId.val() + '&messageType=ACCOUNT_WALL'
            },
            {
                container: '#messagesOnGroupWall',
                url: getPrefix() + '/getMessagesOnWallPagination?groupId=' + targetGroupId.val() + '&messageType=GROUP_WALL'
            },
            {
                container: '#groupJoinRequestAccounts',
                url: getPrefix() + '/groupJoinRequestsPagination',
                action: 'acceptRequestInGroup',
                type: accountType
            },
            {
                container: '#groupMembers',
                url: getPrefix() + '/groupMembersPagination?groupId=' + targetGroupId.val(),
                action: 'removeFromGroup',
                type: accountType
            },
            {
                container: '#groups',
                url: getPrefix() + '/groupsPagination?id=' + targetAccountId.val(),
                action: 'outOfGroup',
                type: groupType
            },
            {
                container: '#requestGroups',
                url: getPrefix() + '/requestGroupsPagination',
                action: 'removeRequestToGroup',
                type: groupType
            },
            {
                container: '#createdGroups',
                url: getPrefix() + '/createdGroupsPagination'
            },
            {
                container: '#friends',
                url: getPrefix() + '/friendsPagination?id=' + targetAccountId.val(),
                action: 'removeFriend'
            },
            {
                container: '#requestOutFriendshipAccounts',
                url: getPrefix() + '/requestOutFriendshipAccountsPagination',
                action: 'removeFriendshipRequest'
            },
            {
                container: '#requestInFriendshipAccounts',
                url: getPrefix() + '/requestInFriendshipAccountsPagination',
                action: 'acceptFriendship'
            },
            {
                container: groupModeratorsContainer,
                url: getPrefix() + '/groupModeratorsPagination',
                action: 'removeGroupModerator',
                type: accountType
            },
            {
                container: groupUsersContainer,
                url: getPrefix() + '/groupUsersPagination',
                action: 'addGroupModerator',
                type: accountType
            }
        ]
    }
});