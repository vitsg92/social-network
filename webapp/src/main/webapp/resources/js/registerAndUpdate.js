﻿$(document).ready(function () {
    var phoneFields = document.getElementById("phoneFields");
    var tmp = phoneFields.children.length;
    $("#addPhone").click(function () {
        var phoneField = '<div class="form-work row">' +
            '<div class="col-2">Телефон</div>' +
            '<div class="input-group col-4">' +
            '<div class="input-group-prepend">' +
            '<select id="type' + tmp + '" name="phones[' + tmp + '].phoneType" class="btn btn-sm btn-select">' +
            '<option value="MOBILE">Мобильный</option>' +
            '<option value="WORK">Рабочий</option>' +
            '<option value="HOME">Домашний</option>' +
            '</select>' +
            '</div>' +
            '<input type="text" id="phone' + tmp + '" name="phones[' + tmp + '].phoneNumber"' +
            ' class="form-control form-control-work"/>' +
            '<div class="input-group-append">' +
            '<button type="button" class="btn btn-sm remove-button">Удалить' +
            '</button>' +
            '</div>' +
            '</div>' +
            '<div class="col-6"></div>' +
            '</div>';
        $("#phoneFields").append(phoneField);
        $('input[id="phone' + tmp + '"]').rules("add", {
            digits: true,
            minlength: 11,
            maxlength: 11,
            messages: {
                digits: "Только цифры",
                minlength: "В телефоне должно быть 11 цифр",
                maxlength: "В телефоне должно быть 11 цифр"
            }
        });
        tmp++;
    });

    $("#phoneFields").on("click", ".remove-button", function () {
        $(this).parents()[2].remove();
    });

    $("#registrationForm").validate({
        rules: {
            firstName: {
                required: true,
                minlength: 2
            },
            lastName: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            dateOfBirth: {
                required: true,
                regex: /^[0-3][\d]\.[0-1][\d]\.[1-2][9,0][\d][\d]$/
            },
            password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            firstName: {
                required: "Введите имя",
                minlength: "Имя должно содержать не менее 2 букв"
            },
            lastName: {
                required: "Введите фамилию",
                minlength: "Фамилия должна содержать не менее 2 букв"
            },
            email: {
                required: "Введите email",
                email: "Не правильный формат email"
            },
            dateOfBirth: {
                required: "Введите дату рождения",
                regex: "Не правильный формат даты(dd.mm.yyyy)"
            },
            password: {
                required: "Введите пароль",
                minlength: "Пароль должен содержать не менее 5 символов"
            }
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().next());
        }
    });

    $("#updateAccountForm").validate({
        rules: {
            firstName: {
                required: true,
                minlength: 2
            },
            lastName: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            dateOfBirth: {
                required: true,
                regex: /^[0-3][\d]\.[0-1][\d]\.[1-2][9,0][\d][\d]$/
            },
            password: {
                minlength: 5
            }
        },
        messages: {
            firstName: {
                required: "Введите имя",
                minlength: "Имя должно содержать не менее 2 букв"
            },
            lastName: {
                required: "Введите фамилию",
                minlength: "Фамилия должна содержать не менее 2 букв"
            },
            email: {
                required: "Введите email",
                email: "Не правильный формат email"
            },
            dateOfBirth: {
                required: "Введите дату рождения",
                regex: "Не правильный формат даты(dd.mm.yyyy)"
            },
            password: {
                minlength: "Пароль должен содержать не менее 5 символов"
            }
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().next());
        }
    });

    $.validator.addMethod(
        "regex", function (value, element, regexp) {
            return regexp.test(value);
        }
    );

    $("input[name*='phones']").each(function () {
        $(this).rules("add", {
            digits: true,
            minlength: 11,
            maxlength: 11,
            messages: {
                digits: "Только цифры",
                minlength: "В телефоне должно быть 11 цифр",
                maxlength: "В телефоне должно быть 11 цифр"
            }
        });
    });

    $("#updateButton").click(function () {
        if (confirm("Вы уверены?")) {
            $('form#updateAccountForm').submit();
        }
    });

    $('.datepicker').datepicker({
        dateFormat: 'dd.mm.yy',
        changeYear: true,
        yearRange: "-80:-18",
        defaultDate: '-18y'
    });
});
