$(function () {
    var searchName = $('#search').val();
    var pageSize = 3;
    var countAccounts = $('#countAccounts').val();
    var paginationAccountsContainer = $('#paginationAccountsContainer');
    if (+countAccounts !== 0) {
        var foundAccountsContainer = $('#foundAccountsContainer');
        paginationAccountsContainer.pagination({
            dataSource: getPrefix() + '/searchAccountsPagination?searchName=' + searchName,
            locator: 'data',
            pageSize: pageSize,
            totalNumber: countAccounts,
            className: 'paginationjs-theme-blue',
            ajax: {
                beforeSend: function () {
                    foundAccountsContainer.html('<div class="ajax-preload"><div class="spinner-border text-primary" role="status">' +
                        '<span class="sr-only">Loading...</span></div></div>');
                }
            },
            callback: function (data) {
                var html = '<div class="pb-3 pl-5"><h6>Найденные акаунты</h6></div>';
                $.each(data, function (index, item) {
                    html += '<a href="' + getPrefix() + '/accountPage?id=' + item.id + '">' +
                        '<div class="media mb-2 min-h-found d-flex align-items-center">' +
                        '<div class="min-w-found d-flex justify-content-center">' +
                        '<img src="' + getPrefix() + '/accountPhoto?id=' + item.id + '" class="icon-friends">' +
                        '</div>' +
                        '<div class="media-body">' + item.firstName + ' ' + item.lastName + '</div> ' +
                        '</div>' +
                        '</a>';
                });
                foundAccountsContainer.html(html);
            }
        });
    } else {
        paginationAccountsContainer.parent().remove();
    }

    var countGroups = $('#countGroups').val();
    if (+countGroups !== 0) {
        var foundGroupsContainer = $('#foundGroupsContainer');
        $('#paginationGroupsContainer').pagination({
            dataSource: getPrefix() + '/searchGroupsPagination?searchName=' + searchName,
            locator: 'data',
            pageSize: pageSize,
            totalNumber: countGroups,
            className: 'paginationjs-theme-blue',
            ajax: {
                beforeSend: function () {
                    foundGroupsContainer.html('<div class="ajax-preload"><div class="spinner-border text-primary" role="status">' +
                        '  <span class="sr-only">Loading...</span></div></div>');
                }
            },
            callback: function (data) {
                var html = '<div class="pb-3 pl-5"><h6>Найденные группы</h6></div>';
                $.each(data, function (index, item) {
                    html += '<a href="' + getPrefix() + '/groupPage?groupId=' + item.id + '">' +
                        '<div class="media mb-2 min-h-found d-flex align-items-center">' +
                        '<div class="min-w-found d-flex justify-content-center">' +
                        '<img src="' + getPrefix() + '/groupPhoto?groupId=' + item.id + '" class="icon-friends">' +
                        '</div>' +
                        '<div class="media-body">' + item.groupName + '</div>' +
                        '</div>' +
                        '</a>';
                });
                foundGroupsContainer.html(html);
            }
        });
    }

    $('#foundAccountsButton').click(function () {
        $('#foundAccountsButton').addClass('active');
        $('#foundGroupsButton').removeClass('active');
        $('#foundAccounts').removeClass('d-none');
        $('#foundGroups').addClass('d-none');
    });

    $('#foundGroupsButton').click(function () {
        $('#foundAccountsButton').removeClass('active');
        $('#foundGroupsButton').addClass('active');
        $('#foundAccounts').addClass('d-none');
        $('#foundGroups').removeClass('d-none');
    });
});