$(function () {
    var mainAccountId = +$('#mainAccountId').val();
    var mainAccountFirstName = $('#mainAccountFirstName').val();
    var mainAccountLastName = $('#mainAccountLastName').val();
    var messageTextarea = $('#messageOnWallTextarea');
    var messageForm = $('#messageOnWallForm');
    var targetGroupId = +$('#targetGroupId').val();
    var messagesOnGroupWallContainer = $('#messagesOnGroupWall');
    var stompClient;
    connect();

    function connect() {
        var socket = new SockJS('/messages');
        stompClient = Stomp.over(socket);
        stompClient.connect({},
            function () {
                stompClient.subscribe(getPrefix() + '/queue/groupMessagesOnWall/' + targetGroupId, function (response) {
                    var message = JSON.parse(response.body);
                    messagesOnGroupWallContainer.prepend(generateMessageHtml(message));
                })
            }
        )
    }

    messageTextarea.on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            messageForm.submit();
        }
    });

    messageForm.on('submit', function () {
        sendMessage();
        messageForm.trigger('reset');
        return false;
    });

    function sendMessage() {
        var messageContent = messageTextarea.val();
        var message = {
            writer: {
                id: mainAccountId,
                firstName: mainAccountFirstName,
                lastName: mainAccountLastName
            },
            destinationId: targetGroupId,
            messageContent: messageContent,
            dateOfWriting: getCurrentTime(),
            messageType: "GROUP_WALL"
        };
        stompClient.send(getPrefix() + '/message/sendMessageOnGroupWall', {}, JSON.stringify(message));
    }
});