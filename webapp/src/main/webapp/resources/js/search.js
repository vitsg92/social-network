$(function () {
        $("#searchLine").autocomplete({
            source: function (request, response) {
                var data = {searchName: request.term};
                $.when(
                    $.get(getPrefix() + "/searchAccountsAjax", data),
                    $.get(getPrefix() + "/searchGroupsAjax", data)
                ).then(function (accounts, groups) {
                    response($.map(accounts[0], function (account) {
                        var accountId = account.id;
                        return {
                            value: accountId,
                            label: account.firstName + " " + account.lastName,
                            urlRedirect: getPrefix() + "/accountPage?id=" + accountId,
                            urlPhoto: getPrefix() + "/accountPhoto?id=" + accountId
                        }
                    }).concat($.map(groups[0], function (group) {
                        var groupId = group.id;
                        return {
                            value: groupId,
                            label: group.groupName,
                            urlRedirect: getPrefix() + "/groupPage?groupId=" + groupId,
                            urlPhoto: getPrefix() + "/groupPhoto?groupId=" + groupId
                        }
                    })));
                })
            },
            minLength: 2,
            select: function (event, ui) {
                location.href = ui.item.urlRedirect;
                return false;
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            var searchItemHtml = "<div>" + "<img src='" + item.urlPhoto + "' class='icon-autocomplete'/>" +
                "<div class='text-autocomplete'>" + "<a>" + item.label + "</a>" + "</div>" + "</div>";
            return $("<li>")
                .append(searchItemHtml)
                .appendTo(ul);
        };
    }
);