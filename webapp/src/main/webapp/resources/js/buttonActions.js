$(function () {
    $("#joinOnGroup").click(function () {
        var joinOnGroupButton = $(this);
        $.get(joinOnGroupButton.attr('href')).then(
            function (data) {
                var relation;
                if (data === "REQUEST_OUT") {
                    relation = 'Заявка отправлена';
                }
                joinOnGroupButton.replaceWith('<span class="badge badge-success">' + relation + '</span>');
            },
            function () {
                joinOnGroupButton.replaceWith('<span class="badge badge-danger">Ошибка</span>');
            });
        return false;
    });

    $('#groupMembers').on('click', '.remove-account-from-group', function () {
        var removeAccountFromGroupButton = $(this);
        $.get(removeAccountFromGroupButton.attr('href')).then(function (data) {
                var relation;
                if (data === "EMPTY") {
                    relation = 'Удален из группы';
                }
                removeAccountFromGroupButton.replaceWith('<div><span class="badge badge-success">' + relation + '</span></div>');
            },
            function () {
                removeAccountFromGroupButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $('#groupJoinRequestAccounts').on('click', '.accept-account-to-group', function () {
        var removeAccountFromGroupButton = $(this);
        $.get(removeAccountFromGroupButton.attr('href')).then(function (data) {
                var relation;
                if (data === "FULL") {
                    relation = 'Принят в группу';
                }
                removeAccountFromGroupButton.replaceWith('<div><span class="badge badge-success">' + relation + '</span></div>');
            },
            function () {
                removeAccountFromGroupButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $('#groups').on('click', '.out-of-group', function () {
        var outOfGroupButton = $(this);
        $.get(outOfGroupButton.attr('href')).then(function (data) {
                var relation;
                if (data === "EMPTY") {
                    relation = 'Вы вышли из группы';
                }
                outOfGroupButton.replaceWith('<div><span class="badge badge-success">' + relation + '</span></div>');
            },
            function () {
                outOfGroupButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $('#requestGroups').on('click', '.remove-request-to-group', function () {
        var removeRequestToGroupButton = $(this);
        $.get(removeRequestToGroupButton.attr('href')).then(function (data) {
                var relation;
                if (data === "EMPTY") {
                    relation = 'Заявка отозвана';
                }
                removeRequestToGroupButton.replaceWith('<div><span class="badge badge-success">' + relation + '</span></div>');
            },
            function () {
                removeRequestToGroupButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $("#requestInFriendshipAccounts, #friendshipInfo").on('click', '.add-friend', function () {
        var addFriendButton = $(this);
        $.get(addFriendButton.attr('href')).then(
            function (data) {
                var relation;
                if (data === "FULL") {
                    relation = 'Ваш друг';
                } else {
                    relation = 'Заявка рассматривается';
                }
                addFriendButton.replaceWith('<div><span class="badge badge-success">' + relation + '</span></div>');
            },
            function () {
                addFriendButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $("#friends, #requestOutFriendshipAccounts").on('click', '.remove-friend, .remove-out-request-friendship', function () {
        var removeFriendButton = $(this);
        $.get(removeFriendButton.attr('href')).then(function () {
                var result;
                if (removeFriendButton.hasClass('remove-out-request-friendship')) {
                    result = 'Заявка отозвана';
                } else {
                    result = 'Удален из друзей';
                }
                removeFriendButton.replaceWith('<div><span class="badge badge-success">' + result + '</span></div>');
            },
            function () {
                removeFriendButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $("#groupModerators").on('click', '.remove-group-moderator', function () {
        var removeGroupModeratorButton = $(this);
        $.get(removeGroupModeratorButton.attr('href')).then(function () {
                removeGroupModeratorButton.replaceWith('<div><span class="badge badge-success">Модератор удален</span></div>');
            },
            function () {
                removeGroupModeratorButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $("#groupUsers").on('click', '.add-group-moderator', function () {
        var addGroupModeratorButton = $(this);
        $.get(addGroupModeratorButton.attr('href')).then(function () {
                addGroupModeratorButton.replaceWith('<div><span class="badge badge-success">Модератор добавлен</span></div>');
            },
            function () {
                addGroupModeratorButton.replaceWith('<div><span class="badge badge-danger">Ошибка</span></div>');
            });
        return false;
    });

    $('#makeAdminButton').click(function () {
        var makeAdminButton = $(this);
        $.get(makeAdminButton.attr('href')).then(function (value) {
            if (value === 'ADMIN') {
                makeAdminButton.replaceWith('<span class="badge badge-success">Администратор добавлен</span>');
            }
        }, function () {
            makeAdminButton.replaceWith('<span class="badge badge-danger">Ошибка</span>');
        });
        return false;
    })
});