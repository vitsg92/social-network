$(function () {
    var mainAccountId = +$('#mainAccountId').val();
    var mainAccountFirstName = $('#mainAccountFirstName').val();
    var mainAccountLastName = $('#mainAccountLastName').val();
    var messageTextarea = $('#messageOnWallTextarea');
    var messageForm = $('#messageOnWallForm');
    var targetAccountId = +$('#targetAccountId').val();
    var messagesOnAccountWallContainer = $('#messagesOnAccountWall');
    var stompClient;
    connect();

    function connect() {
        var socket = new SockJS(getPrefix() + '/messages');
        stompClient = Stomp.over(socket);
        stompClient.connect({},
            function () {
                stompClient.subscribe(getPrefix() + '/queue/accountMessagesOnWall/' + targetAccountId, function (response) {
                    var message = JSON.parse(response.body);
                    messagesOnAccountWallContainer.prepend(generateMessageHtml(message));
                })
            }
        )
    }

    messageTextarea.on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            messageForm.submit();
        }
    });

    messageForm.on('submit', function () {
        sendMessage();
        messageForm.trigger('reset');
        return false;
    });

    function sendMessage() {
        var messageContent = messageTextarea.val();
        var message = {
            writer: {
                id: mainAccountId,
                firstName: mainAccountFirstName,
                lastName: mainAccountLastName
            },
            destinationId: targetAccountId,
            messageContent: messageContent,
            dateOfWriting: getCurrentTime(),
            messageType: "ACCOUNT_WALL"
        };
        stompClient.send(getPrefix() + '/message/sendMessageOnAccountWall', {}, JSON.stringify(message));
    }
});