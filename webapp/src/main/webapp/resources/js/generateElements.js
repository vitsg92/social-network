function getPrefix() {
    return $('#prefix').val();
}

function generateHtml(element, action, type) {
    if (element.messageContent !== undefined) {
        return generateMessageHtml(element);
    } else if (element.lastMessageInDialog !== undefined) {
        return generateDialogHtml(element);
    } else if (element.group !== undefined && type === 'group') {
        return generateGroupHtml(element.group, action);
    } else if (element.groupName !== undefined) {
        return generateGroupHtml(element, action);
    } else if (element.account !== undefined && type === 'account') {
        return generateAccountHtml(element.account, action);
    } else if (element.account2 !== undefined) {
        return generateAccountHtml(element.account2, action);
    } else if (element.firstName !== undefined) {
        return generateAccountHtml(element, action);
    }
}

function generateMessageHtml(element) {
    var writerId = element.writer.id;
    return '<div class="message-on-wall shadow list-group-item">' +
        '<div class="media m-ng-2">' +
        '<img src="' + getPrefix() + '/accountPhoto?id=' + writerId + '" class="icon-on-wall d-flex align-self-center">' +
        '<div class="media-body">' +
        '<div>' +
        '<a class="message-on-wall-writer-ref" href="' + getPrefix() + '/accountPage?id=' + writerId + '">' +
        element.writer.firstName + ' ' + element.writer.lastName + '</a>' +
        '</div>' +
        '<div class="message-on-wall-content">' +
        '<span>' + element.messageContent + '</span>' +
        '</div>' +
        '<div class="message-on-wall-time d-flex justify-content-end">' +
        '<span>' + element.dateOfWriting + '</span>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
}

function generateDialogHtml(element) {
    var companionId = element.companion.id;
    var html = '<a id="' + companionId + '"' +
        ' href="' + getPrefix() + '/chat?id=' + companionId + '"' +
        ' class="list-group-item list-group-item-action shadow dialog">' +
        '<div class="media m-ng-2">' +
        '<img src="' + getPrefix() + '/accountPhoto?id=' + companionId + '" class="icon-dialog d-flex align-self-center">' +
        '<div class="media-body">' +
        '<div class="dialogHead d-flex justify-content-between align-items-center">' +
        '<h6>' + element.companion.firstName + ' ' + element.companion.lastName + '</h6>';
    var countUnacceptedMessages = +element.countUnacceptedMessages;
    if (countUnacceptedMessages !== 0) {
        html += '<span class="badge badge-info">' + countUnacceptedMessages + '</span>';
    }
    html += '</div>' +
        '<div>';
    var messageContent = element.lastMessageInDialog.messageContent;
    if (element.lastMessageInDialog.writer.id === mainAccountId) {
        html += '<span class="message-content">Вы: ' + messageContent + '</span>';
    } else {
        html += '<span class="message-content">' + element.lastMessageInDialog.writer.firstName + ': ' + messageContent + '</span>'
    }
    html += '</div>' +
        '<div class="d-flex justify-content-end">' +
        '<span class="dialog-last-message-time">' + element.lastMessageInDialog.dateOfWriting + '</span>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</a>';
    return html;
}

function generateGroupHtml(element, action) {
    var groupId = element.id;
    var html = '<div class="list-group-item item shadow">' +
        '<div class="media min-h">' +
        '<img src="' + getPrefix() + '/groupPhoto?groupId=' + groupId + '" ' +
        'class="icon-list d-flex align-self-center" alt="photo">' +
        '<div class="media-body d-flex justify-content-between align-self-center">' +
        '<a href="' + getPrefix() + '/groupPage?groupId=' + groupId + '">' + element.groupName + '</a>';
    html += insertHtmlActionWithGroup(element, action);
    html += '</div>' +
        '</div>' +
        '</div>';
    return html;
}

function insertHtmlActionWithGroup(element, action) {
    var html = '';
    var mainAccountId = +$('#mainAccountId').val();
    switch (action) {
        case 'outOfGroup':
            if (+$('#targetAccountId').val() === mainAccountId) {
                if (+element.creator.id === mainAccountId) {
                    html = '<div><span class="badge badge-success">Вы создатель группы</span></div>';
                } else {
                    html = '<a href="' + getPrefix() + '/outOfGroup?groupId=' + element.id + '" ' +
                        'class="btn btn-sm btn-work out-of-group">Покинуть группу</a>';
                }
            }
            break;
        case 'removeRequestToGroup':
            html = '<a href="' + getPrefix() + '/outOfGroup?groupId=' + element.id + '" ' +
                'class="btn btn-sm btn-work remove-request-to-group">Отозвать заявку</a>';
            break;
    }
    return html;
}

function generateAccountHtml(element, action) {
    var html = '<div class="list-group-item item shadow">' +
        '<div class="media min-h">' +
        '<img src="' + getPrefix() + '/accountPhoto?id=' + element.id + '" class="icon-list d-flex align-self-center" alt="photo">' +
        '<div class="media-body d-flex justify-content-between align-self-center">' +
        '<a href="' + getPrefix() + '/accountPage?id=' + element.id + '">' + element.firstName + ' ' + element.lastName + '</a>';
    html += insertHtmlActionWithAccount(element, action);
    html += '</div>' +
        '</div>' +
        '</div>';
    return html;
}

function insertHtmlActionWithAccount(element, action) {
    var html = '';
    var targetGroupIdField = $('#targetGroupId');
    var creatorId = +$('#creatorId').val();
    var mainAccountId = +$('#mainAccountId').val();
    var id = +element.id;
    switch (action) {
        case 'removeFriend':
            if (mainAccountId === +$('#targetAccountId').val()) {
                html = '<a href="' + getPrefix() + '/removeFriend?id=' + element.id + '" ' +
                    'class="btn btn-sm btn-work remove-friend">Удалить из друзей</a>';
            }
            break;
        case 'removeFriendshipRequest':
            html = '<a href="' + getPrefix() + '/removeFriend?id=' + element.id + '" ' +
                'class="btn btn-sm btn-work remove-out-request-friendship">Отозвать заявку</a>';
            break;
        case 'acceptFriendship':
            html = '<a href="' + getPrefix() + '/addFriend?id=' + element.id + '" ' +
                'class="btn btn-sm btn-work add-friend">Принять заявку</a>';
            break;
        case 'acceptRequestInGroup':
            html = '<a href="' + getPrefix() + '/acceptAccountToGroup?groupId=' + targetGroupIdField.val() + '&id=' + element.id + '" ' +
                'class="btn btn-sm btn-work accept-account-to-group">Принять в группу</a>';
            break;
        case 'removeFromGroup':
            if (id === creatorId && id !== mainAccountId) {
                html = '<div><span class="badge badge-success">Создатель группы</span></div>';
            } else if (id === mainAccountId) {
                html = '<div><span class="badge badge-success">Это вы</span></div>';
            } else if (id !== creatorId && id !== mainAccountId && $('#accessToRemoveMembers').val() === true) {
                html = '<a href="' + getPrefix() + '/removeAccountFromGroup?groupId=' + targetGroupIdField.val() + '&id=' + element.id + '" ' +
                    'class="btn btn-sm btn-work remove-account-from-group">Удалить из группы</a>';
            }
            break;
        case 'addGroupModerator':
            html = '<a href="' + getPrefix() + '/addGroupModerator?groupId=' + targetGroupIdField.val() + '&id=' + element.id + '"' +
                ' class="btn btn-sm btn-work add-group-moderator">Добавить модератора</a>';
            break;
        case 'removeGroupModerator':
            if (id !== mainAccountId && id !== creatorId) {
                html = '<a href="' + getPrefix() + '/removeGroupModerator?groupId=' + targetGroupIdField.val() + '&id=' + element.id + '"' +
                    ' class="btn btn-sm btn-work remove-group-moderator">Удалить из модераторов</a>';
            } else if (id === mainAccountId) {
                html = '<div><span class="badge badge-success">Это вы</span></div>';
            } else if (id === creatorId) {
                html = '<div><span class="badge badge-success">Создатель группы</span></div>';
            }
            break;
    }
    return html;
}

function getCurrentTime() {
    var currentTime = new Date();
    var day = currentTime.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var month = currentTime.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var year = currentTime.getFullYear();
    var hours = currentTime.getHours();
    if (hours < 10) {
        hours = '0' + hours;
    }
    var minutes = currentTime.getMinutes();
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    var seconds = currentTime.getSeconds();
    if (seconds < 10) {
        seconds = '0' + seconds
    }
    return day + '.' + month + '.' + year + ' ' + hours + ':' + minutes + ':' + seconds;
}