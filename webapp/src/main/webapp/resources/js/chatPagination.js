$(function () {
    var chatContainer = $('.chat-container');
    var companionId = +$('#companionId').val();
    var mainAccountId = +$('#mainAccountId').val();
    var endMessagesInDialog = false;
    var lastElementId = $('#lastElementId').val();

    chatContainer.scroll(function () {
        if (+chatContainer.scrollTop() === 0 && endMessagesInDialog === false) {
            $.ajax({
                url: getPrefix() + '/getChatMessagesPagination',
                data: {
                    id: companionId,
                    lastElementId: lastElementId
                },
                beforeSend: function () {
                    chatContainer.prepend('<div class="ajax-preload-chat"><div class="spinner-border text-primary" role="status">' +
                        '<span class="sr-only">Loading...</span></div></div>');
                }
            }).then(function (messages) {
                $('.ajax-preload-chat').remove();
                if (messages.length !== 0) {
                    prependMessagesAndReturnScrollPosition(messages);
                    lastElementId = messages[0].id;
                } else {
                    endMessagesInDialog = true;
                }
            })
        }
    });

    function prependMessagesAndReturnScrollPosition(messages) {
        var lastScrollHeight = chatContainer.prop('scrollHeight');
        chatContainer.prepend(generationHtmlPaginationMessages(messages));
        chatContainer.scrollTop(chatContainer.prop('scrollHeight') - lastScrollHeight);
    }

    function generationHtmlPaginationMessages(messages) {
        var messagesPaginationHtml = '';
        $.each(messages, function (index, value) {
            if (+value.writer.id === mainAccountId) {
                messagesPaginationHtml +=
                    '<div>' +
                    '<div class="message shadow-lg">' +
                    '<div>' +
                    '<a class="message-writer-ref" href="' + getPrefix() + '/accountPage?id=' + value.writer.id + '">' +
                    value.writer.firstName + '</a>' +
                    '</div>' +
                    '<div class="message-content">' +
                    '<span>' + value.messageContent + '</span>' +
                    '</div>' +
                    '<div class="message-time">' +
                    '<span>' + value.dateOfWriting + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            } else {
                messagesPaginationHtml += '<div>' +
                    '<div class="message-companion shadow-lg">' +
                    '<div>' +
                    '<a class="message-writer-ref-companion" href="' + getPrefix() + '/accountPage?id=' + value.writer.id + '">' + value.writer.firstName + '</a>\n' +
                    '</div>' +
                    '<div class="message-content-companion">' +
                    '<span>' + value.messageContent + '</span>' +
                    '</div>' +
                    '<div class="message-time-companion">' +
                    '<span>' + value.dateOfWriting + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }
        });
        return messagesPaginationHtml;
    }
});