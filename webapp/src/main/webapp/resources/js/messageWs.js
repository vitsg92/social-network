$(function () {
    var mainAccountId = +$('#mainAccountId').val();
    var mainAccountFirstName = $('#mainAccountFirstName').val();
    var mainAccountLastName = $('#mainAccountLastName').val();
    var companionFirstNameField = $('#companionFirstName');
    var companionIdField = $('#companionId');
    var chatContainer = $('.chat-container');
    var messageTextarea = $('#messageTextarea');
    var messageForm = $('#messageForm');
    var stompClient;

    connect();
    chatContainerScrollDown();

    function connect() {
        var socket = new SockJS(getPrefix() + '/messages');
        stompClient = Stomp.over(socket);
        stompClient.connect({},
            function () {
                stompClient.subscribe(getPrefix() + '/queue/privateMessages/' + mainAccountId, function (response) {
                    var message = JSON.parse(response.body);
                    if (chatContainer.length && +companionIdField.val() === message.writer.id) {
                        $.get('/acceptMessage?id=' + message.id).then(function () {
                            appendMessageOnChatContainer(message);
                            chatContainerScrollDown();
                        })
                    } else {
                        $.post(getPrefix() + '/addMessageToUnaccepted', JSON.stringify(message)).then(function () {
                            notificationNewUnacceptedMessage();
                            notificationOnDialogs(message);
                        })
                    }
                })
            }
        )
    }

    function sendMessage() {
        var messageContent = messageTextarea.val();
        var message = {
            writer: {
                id: mainAccountId,
                firstName: mainAccountFirstName,
                lastName: mainAccountLastName
            },
            reader: {
                id: companionIdField.val()
            },
            messageContent: messageContent,
            dateOfWriting: getCurrentTime()
        };
        stompClient.send(getPrefix() + '/message/sendMessage', {}, JSON.stringify(message));
        appendMessageOnChatContainer(message);
        chatContainerScrollDown();
    }

    $.ajaxSetup({
        contentType: "application/json; charset=utf-8"
    });

    function chatContainerScrollDown() {
        if (chatContainer.length) {
            chatContainer.scrollTop(chatContainer.prop('scrollHeight'));
        }
    }

    function appendMessageOnChatContainer(message) {
        var writerName;
        var messageHtml;
        if (message.writer.id === mainAccountId) {
            writerName = mainAccountFirstName;
            messageHtml =
                '<div>' +
                '<div class="message shadow-lg">' +
                '<div>' +
                '<a class="message-writer-ref" href="' + getPrefix() + '/accountPage?id=' +
                message.writer.id + '">' + writerName + '</a>' +
                '</div>' +
                '<div class="message-content">' +
                '<span>' + message.messageContent + '</span>' +
                '</div>' +
                '<div class="message-time">' +
                '<span>' + message.dateOfWriting + '</span>' +
                '</div>' +
                '</div>' +
                '</div>';
        } else {
            writerName = companionFirstNameField.val();
            messageHtml =
                '<div>' +
                '<div class="message-companion shadow-lg">' +
                '<div>' +
                '<a class="message-writer-ref-companion" href="' + getPrefix() + '/accountPage?id=' +
                message.writer.id + '">' + writerName + '</a>' +
                '</div>' +
                '<div class="message-content-companion">' +
                '<span>' + message.messageContent + '</span>' +
                '</div>' +
                '<div class="message-time-companion">' +
                '<span>' + message.dateOfWriting + '</span>' +
                '</div>' +
                '</div>' +
                '</div>';
        }
        chatContainer.append(messageHtml);
    }

    messageTextarea.on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $('#messageForm').submit();
        }
    });

    messageForm.on('submit', function () {
        sendMessage();
        messageForm.trigger('reset');
        return false;
    });

    function notificationNewUnacceptedMessage() {
        var countMessagesNotification = $('#countMessagesNotification');
        if (countMessagesNotification.length) {
            countMessagesNotification.html(+countMessagesNotification.html() + 1);
        } else {
            var messageNotificationHtml = '<div><span id="countMessagesNotification" class="badge badge-info">1</span></div>';
            $('#messageMenuLine').append(messageNotificationHtml);
        }
    }

    function notificationOnDialogs(message) {
        var dialogs = $('#dialogs');
        if (dialogs.length) {
            var dialog = dialogs.find('#' + message.writer.id + '.list-group-item');
            if (dialog.length) {
                var notification = $(dialog).find('.badge');
                if (notification.length) {
                    $(notification).html(+$(notification).html() + 1);
                } else {
                    var notificationHtml = '<span class="badge badge-info">1</span>';
                    $(dialog).find('.dialog-head').append(notificationHtml);
                }
                $(dialog).find('span.message-content').html(message.writer.firstName + ': ' + message.messageContent);
                $(dialog).find('span.dialog-last-message-time').html(message.dateOfWriting);
                dialogs.prepend(dialog);
            } else {
                if (dialogs.children().length === 0) {
                    $('h6').html('Найденные диалоги');
                }
                var companionId = message.writer.id;
                var dialogHtml =
                    '<a id="' + companionId + '" href="' + getPrefix() + '/chat?id=' + companionId + '" ' +
                    'class="list-group-item list-group-item-action shadow dialog">' +
                    '<div class="row">' +
                    '<div class="col-1">' +
                    '<div class="img-dialog-col">' +
                    '<img src="' + getPrefix() + '/accountPhoto?id=' + companionId + '" class="icon-dialog">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-11">' +
                    '<div class="content-dialog-col">' +
                    '<div class="dialog-head d-flex justify-content-between align-items-center">' +
                    '<h6>' + message.writer.firstName + ' ' + message.writer.lastName + '</h6>' +
                    '<span class="badge badge-info">1</span>' +
                    '</div>' +
                    '<div>' +
                    '<span class="message-content">' + message.writer.firstName + ': ' + message.messageContent + '</span>' +
                    '</div>' +
                    '<div class="d-flex justify-content-end">' +
                    '<span class="dialog-last-message-time">' + message.dateOfWriting + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</a>';
                dialogs.prepend(dialogHtml);
            }
        }
    }
});