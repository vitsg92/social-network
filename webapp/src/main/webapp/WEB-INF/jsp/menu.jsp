<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 27.03.19
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Menu</title>
</head>
<body>
<sec:authentication var="mainAccount" property="principal"/>
<c:set var="mainAccountId" value="${mainAccount.id}"/>
<input type="hidden" id="mainAccountId" value="${mainAccountId}">
<input type="hidden" id="mainAccountFirstName" value="${mainAccount.firstName}">
<input type="hidden" id="mainAccountLastName" value="${mainAccount.lastName}">
<input type="hidden" id="prefix" value="${pageContext.request.contextPath}">
<div class="col-2">
    <div class="nav flex-column nav-tab">
        <div class="mt-3 ml-2">
            <a class="nav-link" href="${pageContext.request.contextPath}/accountPage?id=${mainAccountId}">
                <span class="text-menu">Моя страница</span></a>
            <a class="nav-link" href="${pageContext.request.contextPath}/friends?id=${mainAccountId}">
                <span class="text-menu">Мои друзья</span>
            </a>
            <a id="messageMenuLine" class="nav-link d-flex justify-content-between"
               href="${pageContext.request.contextPath}/openedDialogs">
                <span class="text-menu">Сообщения</span>
                <c:set var="unacceptedMessageCount" value="${mainAccount.unacceptedMessages.size()}"/>
                <c:if test="${unacceptedMessageCount != 0}">
                    <div>
                        <span id="countMessagesNotification" class="badge badge-info">${unacceptedMessageCount}</span>
                    </div>
                </c:if>
            </a>
            <a class="nav-link" href="${pageContext.request.contextPath}/groups?id=${mainAccountId}">
                <span class="text-menu">Мои группы</span></a>
        </div>
    </div>
</div>
</body>
</html>
