<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 05.03.19
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/pagination.min.css" rel="stylesheet">
    <title>Найденные елементы</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-8 pl-5 mt-2">
                <ul class="nav nav-tabs mt-2">
                    <li class="nav-item">
                        <button id="foundAccountsButton" type="button" class="nav-link active">Аккаунты</button>
                    </li>
                    <li class="nav-item">
                        <button id="foundGroupsButton" type="button" class="nav-link">Группы</button>
                    </li>
                </ul>
                <form action="${pageContext.request.contextPath}/search" method="get">
                    <input id="search" type="text" value="${requestScope.searchName}" name="searchName"
                           class="form-control-found ml-2 mt-4">
                </form>
                <input type="hidden" id="countAccounts" value="${requestScope.countAccounts}">
                <input type="hidden" id="countGroups" value="${requestScope.countGroups}">
                <div class="row">
                    <div class="col-6">
                        <div id="foundAccounts">
                            <div id="foundAccountsContainer" class="found-container"></div>
                            <div class="pl-5 pt-3" id="paginationAccountsContainer"></div>
                        </div>
                        <div id="foundGroups" class="d-none">
                            <div id="foundGroupsContainer" class="found-container"></div>
                            <div class=" pl-5 pt-3" id="paginationGroupsContainer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/plugins/pagination.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/searchPagination.js"></script>
</body>
</html>
