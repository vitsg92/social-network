<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 04.03.19
  Time: 19:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/messagesOnWall.css" rel="stylesheet">
    <title>Group page</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="lastElementId"
       value="${messagesOnGroupWall.size() == 0 ? 0 : messagesOnGroupWall.get(messagesOnGroupWall.size() - 1).id}">
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-3">
                <div class="card mt-4">
                    <div class="card-body">
                        <img class="img-fluid photo"
                             src="${pageContext.request.contextPath}/groupPhoto?groupId=${requestScope.targetGroup.id}">
                    </div>
                </div>
                <c:if test="${groupRelation.relationStatus == 'FULL' || mainAccount.role == 'ADMIN'}">
                    <div class="mt-2">
                        <a class="btn btn-sm btn-work w-100"
                           href="${pageContext.request.contextPath}/groupMembers?groupId=${targetGroup.id}">Участники</a>
                    </div>
                </c:if>
                <c:if test="${groupRelation.groupPrivilege == 'MODER' || mainAccount.role == 'ADMIN'}">
                    <div class="btn-group w-100 mt-2">
                        <a class="btn btn-sm btn-work"
                           href="${pageContext.request.contextPath}/groupJoinRequests?groupId=${targetGroup.id}">Заявки
                            на вступление</a>
                        <a class="btn btn-sm btn-work"
                           href="${pageContext.request.contextPath}/groupModerators?groupId=${targetGroup.id}">Модераторы
                            группы</a>
                    </div>
                </c:if>
            </div>
            <div class="col-6">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="card-title"><h6>${targetGroup}</h6></div>
                        <table class="table-borderless table-sm text-body">
                            <c:if test="${not empty requestScope.targetGroup.description}">
                                <tr>
                                    <td>Описание:</td>
                                    <td>${requestScope.targetGroup.description}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <td>Дата создания:</td>
                                <td>${requestScope.targetGroup.dateRegistration}</td>
                            </tr>
                            <tr>
                                <td>Создатель:</td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/accountPage?id=${targetGroup.creator.id}">
                                        ${targetGroup.creator}</a>
                                </td>
                            </tr>
                        </table>
                        <div class="d-flex justify-content-between mt-1">
                            <div>
                                <c:choose>
                                    <c:when test="${groupRelation.relationStatus == 'EMPTY'}">
                                        <a id="joinOnGroup"
                                           href="${pageContext.request.contextPath}/joinOnGroup?groupId=${targetGroup.id}"
                                           class="btn btn-sm btn-work">Вступить в группу</a>
                                    </c:when>
                                    <c:when test="${groupRelation.relationStatus == 'REQUEST_OUT'}">
                                        <span class="badge badge-success">Заявка отправлена</span>
                                    </c:when>
                                    <c:when test="${groupRelation.relationStatus == 'FULL'}">
                                        <span class="badge badge-success">
                                            <c:if test="${targetGroup.creator.id == mainAccountId}">
                                                Вы создатель группы
                                            </c:if>
                                            <c:if test="${groupRelation.groupPrivilege == 'USER'}">
                                                Вы участник
                                            </c:if>
                                            <c:if test="${groupRelation.groupPrivilege == 'MODER' && targetGroup.creator.id != mainAccountId}">
                                                Вы модератор
                                            </c:if>
                                        </span>
                                    </c:when>
                                </c:choose>
                            </div>
                            <div>
                                <c:if test="${groupRelation.groupPrivilege == 'MODER' || mainAccount.role == 'ADMIN'}">
                                    <a href="${pageContext.request.contextPath}/updateGroup?groupId=${targetGroup.id}"
                                       class="btn btn-sm btn-work">Редактировать</a>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
                <c:if test="${groupRelation.relationStatus == 'FULL' || mainAccount.role == 'ADMIN'}">
                    <div class="card mt-4">
                        <div class="card-header">
                            <form id="messageOnWallForm" class="mt-3">
                                <div class="form-inline">
                            <textarea id="messageOnWallTextarea" class="message-on-wall-textarea"
                                      placeholder="Введите сообщение..." autofocus></textarea>
                                    <button type="submit" id="sendMessageOnWall" class="btn btn-sm btn-send-msg">
                                        Отправить
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <c:if test="${messagesOnGroupWall.size() != 0}">
                                <div class="card-title">
                                    <h6>Сообщения на стене</h6>
                                </div>
                            </c:if>
                            <c:if test="${messagesOnGroupWall.size() == 0}">
                                <div class="card-title">
                                    <h6>Сообщений не найдено</h6>
                                </div>
                            </c:if>
                            <div id="messagesOnGroupWall">
                                <c:forEach var="message" items="${messagesOnGroupWall}">
                                    <div class="message-on-wall shadow list-group-item">
                                        <div class="media m-ng-2">
                                            <img src="${pageContext.request.contextPath}/accountPhoto?id=${message.writer.id}"
                                                 class="icon-on-wall d-flex align-self-center">
                                            <div class="media-body">
                                                <div>
                                                    <a class="message-on-wall-writer-ref"
                                                       href="${pageContext.request.contextPath}/accountPage?id=${message.writer.id}">
                                                            ${message.writer}</a>
                                                </div>
                                                <div class="message-on-wall-content">
                                                    <span>${message.messageContent}</span>
                                                </div>
                                                <div class="message-on-wall-time d-flex justify-content-end">
                                                    <span>${message.dateOfWriting}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="targetGroupId" value="${targetGroup.id}"/>
<script src="${pageContext.request.contextPath}/resources/js/groupMessageWs.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
