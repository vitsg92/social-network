<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 02.03.19
  Time: 14:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/messagesOnWall.css" rel="stylesheet">
    <title>Страница аккаунта</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="targetAccountId" value="${targetAccount.id}"/>
<input type="hidden" id="lastElementId"
       value="${messagesOnWall.size() == 0 ? 0 : messagesOnWall.get(messagesOnWall.size() - 1).id}">
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-3">
                <div class="card mt-4">
                    <div class="card-body">
                        <img class="img-fluid photo"
                             src="${pageContext.request.contextPath}/accountPhoto?id=${requestScope.targetAccount.id}">
                    </div>
                </div>
                <c:if test="${targetAccount.id != mainAccountId}">
                    <div class="mt-2">
                        <a href="${pageContext.request.contextPath}/chat?id=${targetAccount.id}"
                           class="btn btn-sm btn-work w-100">Открыть чат</a>
                    </div>
                    <div class="btn-group w-100 mt-2">
                        <a href="${pageContext.request.contextPath}/friends?id=${targetAccount.id}"
                           class="btn btn-sm btn-work">Друзья</a>
                        <a href="${pageContext.request.contextPath}/groups?id=${targetAccount.id}"
                           class="btn btn-sm btn-work">Группы</a>
                    </div>
                </c:if>
            </div>
            <div class="col-6">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="card-title"><h6>${targetAccount}</h6></div>
                        <table class="table table-borderless table-sm text-body">
                            <c:if test="${not empty targetAccount.patronymic}">
                                <tr>
                                    <td>Отчество</td>
                                    <td>${targetAccount.patronymic}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <td>Email</td>
                                <td>${targetAccount.email}</td>
                            </tr>
                            <tr>
                                <td>Дата рождения</td>
                                <td>${targetAccount.dateOfBirth}</td>
                            </tr>
                            <c:forEach var="phone" items="${targetAccount.phones}">
                                <c:if test="${not empty phone.phoneNumber && phone.phoneType.localName.equals('Мобильный')}">
                                    <tr>
                                        <td>Мобильный телефон</td>
                                        <td>${phone.phoneNumber}</td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty phone.phoneNumber && phone.phoneType.localName.equals('Рабочий')}">
                                    <tr>
                                        <td>Рабочий телефон</td>
                                        <td>${phone.phoneNumber}</td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty phone.phoneNumber && phone.phoneType.localName.equals('Домашний')}">
                                    <tr>
                                        <td>Домашний телефон</td>
                                        <td>${phone.phoneNumber}</td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                            <c:if test="${not empty targetAccount.icq}">
                                <tr>
                                    <td>Icq</td>
                                    <td>${targetAccount.icq}</td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty targetAccount.skype}">
                                <tr>
                                    <td>Skype</td>
                                    <td>${targetAccount.skype}</td>
                                </tr>
                            </c:if>
                        </table>
                        <div class="d-flex justify-content-between mt-1">
                            <div id="friendshipInfo">
                                <c:choose>
                                    <c:when test="${requestScope.relationStatus == 'EMPTY' || requestScope.relationStatus == 'REQUEST_IN'}">
                                        <a href="${pageContext.request.contextPath}/addFriend?id=${targetAccount.id}"
                                           class="btn btn-sm btn-work add-friend">Добавить в друзья</a>
                                    </c:when>
                                    <c:when test="${requestScope.relationStatus == 'FULL'}">
                                        <span class="badge badge-success">Ваш друг</span>
                                    </c:when>
                                    <c:when test="${requestScope.relationStatus == 'REQUEST_OUT'}">
                                        <span class="badge badge-success">Заявка рассматривается</span>
                                    </c:when>
                                </c:choose>
                            </div>
                            <div>
                                <c:if test="${empty requestScope.relationStatus || mainAccount.role == 'ADMIN'}">
                                    <a href="${pageContext.request.contextPath}/update?id=${targetAccount.id}"
                                       class="btn btn-sm btn-work">Редактировать</a>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-4">
                    <div class="card-header">
                        <form id="messageOnWallForm" class="d-flex justify-content-between">
                                    <textarea id="messageOnWallTextarea" class="message-on-wall-textarea"
                                              placeholder="Введите сообщение..." autofocus></textarea>
                            <button type="submit" id="sendMessageOnWall" class="btn btn-sm btn-send-msg">
                                Отправить
                            </button>
                        </form>
                    </div>
                    <div class="card-body">
                        <c:if test="${messagesOnWall.size() != 0}">
                            <div class="card-title">
                                <h6>Сообщения на стене</h6>
                            </div>
                        </c:if>
                        <c:if test="${messagesOnWall.size() == 0}">
                            <div class="card-title">
                                <h6>Сообщений не найдено</h6>
                            </div>
                        </c:if>
                        <div id="messagesOnAccountWall" class="list-group">
                            <c:forEach var="message" items="${requestScope.messagesOnWall}">
                                <div class="message-on-wall shadow list-group-item">
                                    <div class="media m-ng-2">
                                        <img src="${pageContext.request.contextPath}/accountPhoto?id=${message.writer.id}"
                                             class="icon-on-wall d-flex align-self-center">
                                        <div class="media-body">
                                            <div>
                                                <a class="message-on-wall-writer-ref"
                                                   href="${pageContext.request.contextPath}/accountPage?id=${message.writer.id}">
                                                        ${message.writer}</a>
                                            </div>
                                            <div class="message-on-wall-content">
                                                <span>${message.messageContent}</span>
                                            </div>
                                            <div class="message-on-wall-time d-flex justify-content-end">
                                                <span>${message.dateOfWriting}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/accountMessageWs.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
