<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 04.03.19
  Time: 20:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Редактирование группы</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-10">
                <div class="row mt-2 mb-2">
                    <div class="col-4">
                        <h6>Редактирование группы</h6>
                    </div>
                    <c:if test="${mainAccount.id == updatedGroup.creator.id || mainAccount.role == 'ADMIN'}">
                        <div class="col-2 offset-6">
                            <a href="${pageContext.request.contextPath}/removeGroup?groupId=${updatedGroup.id}"
                               class="btn btn-sm btn-work">Удалить группу</a>
                        </div>
                    </c:if>
                </div>
                <form:form modelAttribute="updatedGroup"
                           action="${pageContext.request.contextPath}/updateGroup?groupId=${updatedGroup.id}"
                           method="post" enctype="multipart/form-data">
                    <form:hidden path="id"/>
                    <div class="form-work row">
                        <div class="col-2">Название группы</div>
                        <div class="col-4"><form:input path="groupName" class="form-control form-control-work"/></div>
                        <div class="col-6 error-status">${requestScope.errors.get("errorGroupName")}</div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Описание</div>
                        <div class="col-4"><form:input path="description" class="form-control form-control-work"/></div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Фото</div>
                        <div class="col-4">
                            <label class="btn btn-sm btn-form">
                                <span id="inputFileMsg">Выберите файл</span>
                                <input id="inputFile" type="file" name="groupPhoto" class="d-none">
                            </label>
                        </div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">
                            <input type="submit" class="btn btn-sm btn-work" value="Сохранить"/>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
