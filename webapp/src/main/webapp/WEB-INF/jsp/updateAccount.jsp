<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 03.03.19
  Time: 16:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Редактирование профиля</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-10">
                <div class="form-work row mt-2">
                    <div class="col-4">
                        <h6>Редактирование профиля</h6>
                    </div>
                    <div class="col-3 offset-3">
                        <a href="${pageContext.request.contextPath}/updateAccountXml?id=${updatedAccount.id}">Редактирование
                            в XML</a>
                    </div>
                    <div class="col-2">
                        <c:if test="${mainAccountId == updatedAccount.id || mainAccount.role == 'ADMIN'}">
                            <a href="${pageContext.request.contextPath}/removeAccount?id=${updatedAccount.id}"
                               class="btn btn-sm btn-work">Удалить аккаунт</a>
                        </c:if>
                    </div>
                </div>
                ${requestScope.get("errorSQL")}
                ${requestScope.get("errorXml")}
                <div class="text-body">
                    <form:form id="updateAccountForm" modelAttribute="updatedAccount"
                               action="${pageContext.request.contextPath}/update?id=${requestScope.updatedAccount.id}"
                               method="post"
                               enctype="multipart/form-data">
                        <form:hidden path="id"/>
                        <form:hidden path="dateRegistration"/>
                        <div class="form-work row">
                            <div class="col-2">Имя</div>
                            <div class="col-4"><form:input type="text" path="firstName"
                                                           class="form-control form-control-work"/>
                            </div>
                            <div class="col-6 error-status">${requestScope.errors.get("errorFirstName")}</div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Фамилия</div>
                            <div class="col-4"><form:input type="text" path="lastName"
                                                           class="form-control form-control-work"/>
                            </div>
                            <div class="col-6 error-status">${requestScope.errors.get("errorLastName")}</div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Отчество</div>
                            <div class="col-4"><form:input type="text" path="patronymic"
                                                           class="form-control form-control-work"/>
                            </div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Email</div>
                            <div class="col-4"><form:input type="text" path="email"
                                                           class="form-control form-control-work"/>
                            </div>
                            <div class="col-3 error-status">${requestScope.errors.get("errorEmail")}</div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Дата рождения</div>
                            <div class="col-4"><form:input type="text" path="dateOfBirth"
                                                           cssClass="form-control form-control-work datepicker"
                                                           autocomplete="false"/></div>
                            <div class="col-3 error-status"></div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Skype</div>
                            <div class="col-4"><form:input type="text" path="skype"
                                                           class="form-control form-control-work"/>
                            </div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Icq</div>
                            <div class="col-4"><form:input type="text" path="icq"
                                                           class="form-control form-control-work"/>
                            </div>
                        </div>
                        <div id="phoneFields">
                            <c:choose>
                                <c:when test="${empty requestScope.updatedAccount || requestScope.updatedAccount.phones.size() == 0}">
                                    <div class="form-work row">
                                        <div class="col-2">Телефон</div>
                                        <div class="input-group col-4">
                                            <div class="input-group-prepend">
                                                <form:select id="type0" path="phones[0].phoneType"
                                                             cssClass="btn btn-sm btn-select">
                                                    <option value="MOBILE">Мобильный</option>
                                                    <option value="WORK">Рабочий</option>
                                                    <option value="HOME">Домашний</option>
                                                </form:select>
                                            </div>
                                            <form:input type="text" id="phone0" path="phones[0].phoneNumber"
                                                        class="form-control form-control-work"/>
                                            <form:hidden path="phones[0].id"/>
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-sm remove-button">Удалить</button>
                                            </div>
                                        </div>
                                        <div class="col-6"></div>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="phone" items="${requestScope.updatedAccount.phones}"
                                               varStatus="status">
                                        <c:set var="index" value="${status.index}"/>
                                        <div class="form-work row">
                                            <div class="col-2">Телефон</div>
                                            <div class="col-4 input-group">
                                                <div class="input-group-prepend">
                                                    <form:select id="type${index}"
                                                                 path="phones[${index}].phoneType"
                                                                 class="btn btn-sm btn-select">
                                                        <option value="MOBILE"
                                                                <c:if test="${'Мобильный'.equals(phone.phoneType.localName)}">selected</c:if>>
                                                            Мобильный
                                                        </option>
                                                        <option value="WORK"
                                                                <c:if test="${'Рабочий'.equals(phone.phoneType.localName)}">selected</c:if>>
                                                            Рабочий
                                                        </option>
                                                        <option value="HOME"
                                                                <c:if test="${'Домашний'.equals(phone.phoneType.localName)}">selected</c:if>>
                                                            Домашний
                                                        </option>
                                                    </form:select>
                                                </div>
                                                <form:input id="phone${index}" path="phones[${index}].phoneNumber"
                                                            value="${phone.phoneNumber}"
                                                            class="form-control form-control-work"/>
                                                <form:hidden path="phones[${index}].id"/>
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-sm remove-button">Удалить
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-6"></div>
                                        </div>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">
                                <button id="addPhone" class="btn btn-sm btn-work" type="button">Добавить телефон
                                </button>
                            </div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Задать новый пароль</div>
                            <div class="col-4"><input name="password" type="password"
                                                      class="form-control form-control-work"/>
                            </div>
                            <div class="col-3"></div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Загрузить новое фото</div>
                            <div class="col-4">
                                <label class="btn btn-sm btn-form">
                                    <span id="inputFileMsg">Выберите файл</span>
                                    <input id="inputFile" type="file" name="photo" class="d-none">
                                </label>
                            </div>
                        </div>

                        <div class="form-work row">
                            <div class="col-2">
                                <button id="updateButton" type="button" class="btn btn-sm btn-work">Сохранить</button>
                            </div>
                            <c:if test="${mainAccount.role == 'ADMIN' && mainAccountId != updatedAccount.id
                            && updatedAccount.role == 'USER'}">
                                <div class="col-2">
                                    <a href="${pageContext.request.contextPath}/makeAdmin?id=${updatedAccount.id}"
                                       id="makeAdminButton" class="btn btn-sm btn-work">Сделать
                                        администратором
                                    </a>
                                </div>
                            </c:if>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/registerAndUpdate.js"></script>
</body>
</html>
