<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 26.04.19
  Time: 14:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Account XML</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-10 mt-2">
                <h4>Редактирование профиля В XML</h4>
                ${requestScope.get("errorXml")}
                <div class="text-body">
                    <form action="${pageContext.request.contextPath}/updateAccountXml?id=${updatedAccountId}"
                          method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="${updatedAccountId}"/>
                        <div class="form-work row">
                            <div class="col-2">
                                <a href="${pageContext.request.contextPath}/downloadAccountXml?id=${updatedAccountId}"
                                   id="downloadXml" class="btn btn-sm btn-form">Скачать XML</a>
                            </div>
                            <div class="col-2">
                                <label class="btn btn-sm btn-form">
                                    <span id="inputFileMsg">Загрузить XML</span>
                                    <input id="inputFile" type="file" name="accountXml" class="d-none">
                                </label>
                            </div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">
                                <input id="updateXmlButton" type="submit" class="btn btn-sm btn-work"
                                       value="Сохранение изменений"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/chainForInputFileClick.js"></script>
</body>
</html>
