<%--
  Created by IntelliJ IDEA.
  User: Виталий Гончаренко
  Date: 30.05.2019
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Участники группы</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="lastElementId"
       value="${groupRelationMembers.size() == 0 ? 0 : groupRelationMembers.get(groupRelationMembers.size() - 1).id}">
<c:set var="creatorId" value="${targetGroup.creator.id}"/>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-8">
                <div class="text-body">
                    <h6 class="mt-3">
                        <c:if test="${not empty groupRelationMembers}">
                            Участники
                        </c:if>
                        <c:if test="${empty groupRelationMembers}">
                            Участников не найдено
                        </c:if>
                    </h6>
                    <div id="groupMembers" class="list-group">
                        <c:forEach var="groupRelationMember" items="${groupRelationMembers}">
                            <div class="list-group-item item shadow">
                                <div class="media min-h">
                                    <img src="${pageContext.request.contextPath}/accountPhoto?id=${groupRelationMember.account.id}"
                                         class="icon-list d-flex align-self-center">
                                    <div class="media-body d-flex justify-content-between align-self-center">
                                        <a href="${pageContext.request.contextPath}/accountPage?id=${groupRelationMember.account.id}">
                                                ${groupRelationMember.account}</a>
                                        <c:if test="${groupRelationMember.account.id != creatorId
                                            && groupRelationMember.account.id != mainAccountId
                                            && (groupRelation.groupPrivilege == 'MODER' || mainAccount.role == 'ADMIN')}">
                                            <a href="${pageContext.request.contextPath}/removeAccountFromGroup?groupId=${targetGroup.id}&id=${groupRelationMember.account.id}"
                                               class="btn btn-sm btn-work remove-account-from-group">Удалить из
                                                группы</a>
                                        </c:if>
                                        <c:if test="${groupRelationMember.account.id == creatorId && groupRelationMember.account.id != mainAccountId}">
                                            <div>
                                                <span class="badge badge-success">Создатель группы</span>
                                            </div>
                                        </c:if>
                                        <c:if test="${groupRelationMember.account.id == mainAccountId}">
                                            <div>
                                                <span class="badge badge-success">Это вы</span>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <div class="preloader d-flex justify-content-center"></div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="creatorId" value="${creatorId}">
<input type="hidden" id="targetGroupId" value="${targetGroup.id}">
<input type="hidden" id="accessToRemoveMembers"
       value="${groupRelation.groupPrivilege == 'MODER' || mainAccount.role == 'ADMIN'}">
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
