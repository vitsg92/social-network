<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 27.03.19
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Группы</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="lastElementId"
       value="${groupRelations.size() == 0 ? 0 : groupRelations.get(groupRelations.size() - 1).id}">
<input type="hidden" id="targetAccountId" value="${targetAccountId}">
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-8">
                <c:if test="${mainAccountId == targetAccountId}">
                    <ul class="nav nav-tabs mt-4">
                        <li class="nav-item">
                            <button id="groupsButton" type="button" class="nav-link active">Группы</button>
                        </li>
                        <li class="nav-item">
                            <button id="requestGroupsButton" type="button" class="nav-link">Заявки в группы</button>
                        </li>
                        <li class="nav-item">
                            <button id="createdGroupsButton" type="button" class="nav-link">Созданные группы</button>
                        </li>
                    </ul>
                    <div id="groups" class="list-group">
                        <h6 class="mt-3">
                            <c:if test="${not empty groupRelations}">
                                Группы
                            </c:if>
                            <c:if test="${empty groupRelations}">
                                Групп не найдено
                            </c:if>
                        </h6>
                        <c:forEach var="groupRelation" items="${groupRelations}">
                            <div class="list-group-item item shadow">
                                <div class="media min-h">
                                    <img src="${pageContext.request.contextPath}/groupPhoto?groupId=${groupRelation.group.id}"
                                         class="icon-list d-flex align-self-center" alt="photo">
                                    <div class="media-body d-flex justify-content-between align-self-center">
                                        <a href="${pageContext.request.contextPath}/groupPage?groupId=${groupRelation.group.id}">${groupRelation.group}</a>
                                        <c:if test="${groupRelation.group.creator.id != mainAccount.id}">
                                            <a href="${pageContext.request.contextPath}/outOfGroup?groupId=${groupRelation.group.id}"
                                               class="btn btn-sm btn-work out-of-group">Покинуть группу</a>
                                        </c:if>
                                        <c:if test="${groupRelation.group.creator.id == mainAccount.id}">
                                            <div>
                                                <span class="badge badge-success">Вы создатель группы</span>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div id="requestGroups" class="list-group d-none"></div>
                    <div id="createdGroups" class="list-group d-none"></div>
                </c:if>
                <c:if test="${mainAccountId != targetAccountId}">
                    <div id="groups" class="list-group">
                        <h6 class="mt-3">
                            <c:if test="${not empty groupRelations}">
                                Группы
                            </c:if>
                            <c:if test="${empty groupRelations}">
                                Групп не найдено
                            </c:if>
                        </h6>
                        <c:forEach var="groupRelation" items="${groupRelations}">
                            <div class="list-group-item item shadow">
                                <div class="media min-h">
                                    <img src="${pageContext.request.contextPath}/groupPhoto?groupId=${groupRelation.group.id}"
                                         class="icon-list d-flex align-self-center" alt="photo">
                                    <div class="media-body d-flex justify-content-between align-self-center">
                                        <a href="${pageContext.request.contextPath}/groupPage?groupId=${groupRelation.group.id}">${groupRelation.group}</a>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </c:if>
                <div class="preloader d-flex justify-content-center"></div>
            </div>
            <div class="col-2">
                <c:if test="${mainAccount.id == targetAccountId}">
                    <a href="${pageContext.request.contextPath}/createGroup" class="btn btn-sm btn-work mt-4">
                        Создать группу </a>
                </c:if>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
