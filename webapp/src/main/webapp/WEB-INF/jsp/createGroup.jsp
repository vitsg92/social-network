<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 04.03.19
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Создание группы</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-10">
                <h6 class="mt-2">Создание группы</h6>
                <div class="text-body mt-3">
                    <form:form modelAttribute="createdGroup" action="${pageContext.request.contextPath}/createGroup"
                               method="post" enctype="multipart/form-data">
                        <input type="hidden" name="dateRegistration"
                               value="<%=new SimpleDateFormat(" dd.MM.yyyy hh:mm:ss").format(new Date())%>"/>
                        <div class="form-work row">
                            <div class="col-2">Название группы</div>
                            <div class="col-4">
                                <form:input type="text" path="groupName" class="form-control form-control-work"/>
                            </div>
                            <div class="col-6 error-status">${requestScope.errors.get("errorGroupName")}</div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Описание</div>
                            <div class="col-4">
                                <form:input type="text" path="description" class="form-control form-control-work"/>
                            </div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">Фото</div>
                            <div class="col-4">
                                <label class="btn btn-sm btn-form">
                                    <span id="inputFileMsg">Выберите файл</span>
                                    <input id="inputFile" type="file" name="groupPhoto" class="d-none">
                                </label>
                            </div>
                        </div>
                        <div class="form-work row">
                            <div class="col-2">
                                <input type="submit" class="btn btn-sm btn-work" value="Создать"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
