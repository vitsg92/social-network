<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 02.03.19
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/auth.css" rel="stylesheet">
    <title>Авторизация</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-4 offset-4">
            <div class="card text-center auth-form">
                <div class="card-body">
                    <c:if test="${param.error == true}">
                        <span class="ui-state-error-text">Неправильный логин или пароль</span>
                    </c:if>
                    <div class="card-title">
                        <h2 class="h3 mb-3 font-weight-normal text-work">Авторизация</h2>
                    </div>
                    <form class="form-group" action="${pageContext.request.contextPath}/login" method="post">
                        <input id="inputEmail" type="text" name="email" class="form-control form-group form-control-sm"
                               placeholder="Email"/>
                        <input id="inputPassword" type="password" name="password"
                               class="form-control form-group form-control-sm"
                               placeholder="Password"/>
                        <label class="text-work" for="remember">Запомнить меня</label>
                        <input id="remember" type="checkbox" name="rememberMe">
                        <div class="col-8 offset-2">
                            <button class="btn btn-outline-work" type="submit">Войти</button>
                        </div>
                    </form>
                    <a href="${pageContext.request.contextPath}/registration" class="text-work">Зарегистрироваться</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
