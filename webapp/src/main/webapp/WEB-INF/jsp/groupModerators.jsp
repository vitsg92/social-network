<%--
  Created by IntelliJ IDEA.
  User: Виталий Гончаренко
  Date: 02.06.2019
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Модераторы группы</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<c:set var="creatorId" value="${targetGroup.creator.id}"/>
<input type="hidden" id="lastElementId"
       value="${groupRelationModerators.size() == 0 ? 0 : groupRelationModerators.get(groupRelationModerators.size() - 1).id}">
<input type="hidden" id="creatorId" value="${creatorId}">
<input type="hidden" id="targetGroupId" value="${targetGroup.id}">
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-8">
                <ul class="nav nav-tabs mt-4">
                    <li class="nav-item">
                        <button id="groupModeratorsButton" type="button" class="nav-link active">Модераторы группы
                        </button>
                    </li>
                    <li class="nav-item">
                        <button id="addModeratorsButton" type="button" class="nav-link">Добавить модератора</button>
                    </li>
                </ul>
                <div id="groupModerators" class="list-group">
                    <h6 class="mt-3">
                        <c:if test="${not empty groupRelationModerators}">
                            Модераторы
                        </c:if>
                        <c:if test="${empty groupRelationModerators}">
                            Модераторов не найдено
                        </c:if>
                    </h6>
                    <c:forEach var="groupRelationModerator" items="${groupRelationModerators}">
                        <div class="list-group-item item shadow">
                            <div class="media min-h">
                                <img src="${pageContext.request.contextPath}/accountPhoto?id=${groupRelationModerator.account.id}"
                                     class="icon-list d-flex align-self-center" alt="photo">
                                <div class="media-body d-flex justify-content-between align-self-center">
                                    <a href="${pageContext.request.contextPath}/accountPage?id=${groupRelationModerator.account.id}">${groupRelationModerator.account}</a>
                                    <c:if test="${mainAccount.id != groupRelationModerator.account.id
                                    && targetGroup.creator.id != groupRelationModerator.account.id}">
                                        <a href="${pageContext.request.contextPath}/removeGroupModerator?groupId=${targetGroup.id}&id=${groupRelationModerator.account.id}"
                                           class="btn btn-sm btn-work remove-group-moderator">Удалить из
                                            модераторов</a>
                                    </c:if>
                                    <c:if test="${mainAccount.id == groupRelationModerator.account.id}">
                                        <div>
                                            <span class="badge badge-success">Это вы</span>
                                        </div>
                                    </c:if>
                                    <c:if test="${creatorId == groupRelationModerator.account.id && creatorId != mainAccount.id}">
                                        <div>
                                            <span class="badge badge-success">Создатель группы</span>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <div id="groupUsers" class="list-group d-none"></div>
                <div class="preloader d-flex justify-content-center"></div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
