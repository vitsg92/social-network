<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 01.03.19
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/register.css" rel="stylesheet">
    <title>Регистрация</title>
</head>
<body>
<div class="container">
    <div class="main-bg h-100">
        <div class="ml-4 mt-3">
            <h5 class="pt-3">Регистрация</h5>
            <div class="text-body">
                <form:form id="registrationForm" modelAttribute="createdAccount"
                           action="${pageContext.request.contextPath}/registration" method="post"
                           enctype="multipart/form-data">
                    <input type="hidden" name="dateRegistration"
                           value="<%=new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(new Date())%>"/>
                    <div class="form-work row">
                        <div class="col-2">Имя</div>
                        <div class="col-4">
                            <form:input type="text" path="firstName" cssClass="form-control form-control-work"/>
                        </div>
                        <div class="col-6">${requestScope.errors.get("errorFirstName")}</div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Фамилия</div>
                        <div class="col-4"><form:input type="text" path="lastName"
                                                       cssClass="form-control form-control-work"/></div>
                        <div class="col-6">${requestScope.errors.get("errorLastName")}</div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Отчество</div>
                        <div class="col-4"><form:input type="text" path="patronymic"
                                                       cssClass="form-control form-control-work"/></div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Email</div>
                        <div class="col-4"><form:input type="text" path="email"
                                                       cssClass="form-control form-control-work"/></div>
                        <div class="col-3 error-status">${requestScope.errors.get("errorEmail")}</div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Дата рождения</div>
                        <div class="col-4"><form:input type="text" path="dateOfBirth"
                                                       cssClass="form-control form-control-work datepicker"
                                                       autocomplete="false"/></div>
                        <div class="col-3"></div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Skype</div>
                        <div class="col-4"><form:input type="text" path="skype"
                                                       cssClass="form-control form-control-work"/></div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Icq</div>
                        <div class="col-4"><form:input type="text" path="icq"
                                                       cssClass="form-control form-control-work"/></div>
                    </div>
                    <div id="phoneFields">
                        <c:choose>
                            <c:when test="${empty createdAccount.phones || createdAccount.phones.size() == 0}">
                                <div class="form-work row">
                                    <div class="col-2">Телефон</div>
                                    <div class="input-group col-4">
                                        <div class="input-group-prepend">
                                            <form:select id="type0" path="phones[0].phoneType"
                                                         cssClass="btn btn-sm btn-select">
                                                <option value="MOBILE">Мобильный</option>
                                                <option value="WORK">Рабочий</option>
                                                <option value="HOME">Домашний</option>
                                            </form:select>
                                        </div>
                                        <form:input type="text" id="phone0" path="phones[0].phoneNumber"
                                                    cssClass="form-control form-control-work"/>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-sm remove-button">Удалить</button>
                                        </div>
                                    </div>
                                    <div class="col-6"></div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="phone" items="${requestScope.createdAccount.phones}" varStatus="status">
                                    <c:set var="index" value="${status.index}"/>
                                    <div class="form-work row">
                                        <div class="col-2">Телефон</div>
                                        <div class="col-4 input-group">
                                            <div class="input-group-prepend">
                                                <form:select id="type${index}" path="phones[${index}].phoneType"
                                                             cssClass="btn btn-sm btn-select">
                                                    <option value="MOBILE"
                                                            <c:if test="${phone.phoneType == 'MOBILE'}">selected</c:if>>
                                                        Мобильный
                                                    </option>
                                                    <option value="WORK"
                                                            <c:if test="${phone.phoneType == 'WORK'}">selected</c:if>>
                                                        Рабочий
                                                    </option>
                                                    <option value="HOME"
                                                            <c:if test="${phone.phoneType == 'HOME'}">selected</c:if>>
                                                        Домашний
                                                    </option>
                                                </form:select>
                                            </div>
                                            <form:input type="text" id="phone${index}"
                                                        path="phones[${index}].phoneNumber"
                                                        cssClass="form-control form-control-work"
                                                        value="${phone.phoneNumber}"/>
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-sm remove-button">Удалить</button>
                                            </div>
                                        </div>
                                        <div class="col-6"></div>
                                    </div>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">
                            <button id="addPhone" class="btn btn-sm btn-work" type="button">Добавить телефон</button>
                        </div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Пароль</div>
                        <div class="col-4"><input name="password" type="password"
                                                  class="form-control form-control-work"/>
                        </div>
                        <div class="col-3">${requestScope.errors.get("errorPassword")}</div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">Фото</div>
                        <div class="col-4">
                            <label class="btn btn-sm btn-form">
                                <span id="inputFileMsg">Выберите файл</span>
                                <input id="inputFile" type="file" name="photo" class="d-none"
                                       value="${createdAccount.photo}">
                            </label>
                        </div>
                    </div>
                    <div class="form-work row">
                        <div class="col-2">
                            <input type="submit" class="btn btn-sm btn-work" value="Зарегистрироваться"/>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/registerAndUpdate.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/chainForInputFileClick.js"></script>
</body>
</html>