<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 18.04.19
  Time: 14:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Друзья</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="lastElementId"
       value="${friendRelations.size() == 0 ? 0 : friendRelations.get(friendRelations.size() - 1).id}">
<input type="hidden" id="targetAccountId" value="${targetAccount.id}">
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-8">
                <c:if test="${mainAccount.id == targetAccount.id}">
                    <ul class="nav nav-tabs mt-4">
                        <li class="nav-item">
                            <button id="friendsButton" type="button" class="nav-link active">Друзья</button>
                        </li>
                        <li class="nav-item">
                            <button id="requestOutFriendshipAccountsButton" type="button" class="nav-link">Исходящие
                                заявки
                            </button>
                        </li>
                        <li class="nav-item">
                            <button id="requestInFriendshipAccountsButton" type="button" class="nav-link">Входящие
                                заявки
                            </button>
                        </li>
                    </ul>
                    <div id="friends" class="list-group">
                        <h6 class="mt-3">
                            <c:if test="${not empty friendRelations}">
                                Друзья
                            </c:if>
                            <c:if test="${empty friendRelations}">
                                Друзья не найдены
                            </c:if>
                        </h6>
                        <c:forEach var="friendRelation" items="${friendRelations}">
                            <div class="list-group-item item shadow">
                                <div class="media min-h">
                                    <img src="${pageContext.request.contextPath}/accountPhoto?id=${friendRelation.account2.id}"
                                         class="icon-list d-flex align-self-center" alt="photo">
                                    <div class="media-body d-flex justify-content-between align-self-center">
                                        <a href="${pageContext.request.contextPath}/accountPage?id=${friendRelation.account2.id}">${friendRelation.account2}</a>
                                        <a href="${pageContext.request.contextPath}/removeFriend?id=${friendRelation.account2.id}"
                                           class="btn btn-sm btn-work remove-friend">Удалить из друзей</a>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div id="requestOutFriendshipAccounts" class="list-group d-none"></div>
                    <div id="requestInFriendshipAccounts" class="list-group d-none"></div>
                </c:if>
                <c:if test="${mainAccount.id != targetAccount.id}">
                    <div id="friends" class="text-body">
                        <h6 class="mt-3">
                            <c:if test="${not empty friendRelations}">
                                Друзья
                            </c:if>
                            <c:if test="${empty friendRelations}">
                                Друзья не найдены
                            </c:if>
                        </h6>
                        <div class="list-group">
                            <c:forEach var="friendRelation" items="${friendRelations}">
                                <div class="list-group-item item shadow">
                                    <div class="media min-h">
                                        <img src="${pageContext.request.contextPath}/accountPhoto?id=${friendRelation.account2.id}"
                                             class="icon-list d-flex align-self-center" alt="photo">
                                        <div class="media-body d-flex justify-content-between align-self-center">
                                            <a href="${pageContext.request.contextPath}/accountPage?id=${friendRelation.account2.id}">${friendRelation.account2}</a>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </c:if>
                <div class="preloader d-flex justify-content-center"></div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
