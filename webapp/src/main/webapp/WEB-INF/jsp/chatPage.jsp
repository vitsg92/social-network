<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 21.05.19
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/chatPage.css" rel="stylesheet">
    <title>Чат</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="lastElementId" value="${messages.size() == 0 ? 0 : messages.get(0).id}">
<c:set var="companionId" value="${companion.id}"/>
<sec:authentication var="mainAccountId" property="principal.id"/>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-5 offset-2 mt-4">
                <a href="${pageContext.request.contextPath}/accountPage?id=${companionId}"
                   class="d-flex justify-content-center min-h-chat-header">
                    <div class="media d-flex align-items-center">
                        <img src="${pageContext.request.contextPath}/accountPhoto?id=${companionId}"
                             class="icon-chat">
                        <div class="media-body chat-header">
                            <span>${companion}</span>
                        </div>
                    </div>
                </a>
                <div class="chat-container">
                    <c:forEach var="message" items="${messages}">
                        <c:if test="${message.writer.id == mainAccountId}">
                            <div>
                                <div class="message shadow-lg">
                                    <div>
                                        <a class="message-writer-ref"
                                           href="${pageContext.request.contextPath}/accountPage?id=${message.writer.id}">
                                                ${message.writer.firstName}</a>
                                    </div>
                                    <div class="message-content">
                                        <span>${message.messageContent}</span>
                                    </div>
                                    <div class="message-time">
                                        <span>${message.dateOfWriting}</span>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${message.writer.id == companionId}">
                            <div>
                                <div class="message-companion shadow-lg">
                                    <div>
                                        <a class="message-writer-ref-companion"
                                           href="${pageContext.request.contextPath}/accountPage?id=${message.writer.id}">
                                                ${message.writer.firstName}</a>
                                    </div>
                                    <div class="message-content-companion">
                                        <span>${message.messageContent}</span>
                                    </div>
                                    <div class="message-time-companion">
                                        <span>${message.dateOfWriting}</span>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>
                </div>
                <form id="messageForm" class="d-flex justify-content-between">
                            <textarea id="messageTextarea" class="message-textarea"
                                      placeholder="Введите сообщение..." autofocus></textarea>
                    <button type="submit" id="sendMessage" class="btn btn-sm btn-send-msg">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="companionId" value="${companionId}">
<input type="hidden" id="companionFirstName" value="${companion.firstName}">
<script src="${pageContext.request.contextPath}/resources/js/chatPagination.js"></script>
</body>
</html>
