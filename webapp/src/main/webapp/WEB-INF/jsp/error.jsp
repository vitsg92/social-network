<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 03.04.19
  Time: 11:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error Page</title>
</head>
<body>
<h4>Error!</h4>
<div>
    <div>${requestScope.error}</div>
</div>
</body>
</html>
