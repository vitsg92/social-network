<%--
  Created by IntelliJ IDEA.
  User: Виталий Гончаренко
  Date: 30.05.2019
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <title>Заявки на вступление в группу</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="lastElementId"
       value="${groupRelationJoinRequestAccounts.size() == 0 ? 0 : groupRelationJoinRequestAccounts
       .get(groupRelationJoinRequestAccounts.size() - 1).id}">
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-8">
                <div class="text-body">
                    <h6 class="mt-3">
                        <c:if test="${not empty groupRelationJoinRequestAccounts}">
                            Заявки в группу
                        </c:if>
                        <c:if test="${empty groupRelationJoinRequestAccounts}">
                            Заявок не найдено
                        </c:if>
                    </h6>
                    <div id="groupJoinRequestAccounts" class="list-group">
                        <c:forEach var="groupRelationJoinRequestAccount" items="${groupRelationJoinRequestAccounts}">
                            <div class="list-group-item item shadow">
                                <div class="media min-h">
                                    <img src="${pageContext.request.contextPath}/accountPhoto?id=${groupRelationJoinRequestAccount.account.id}"
                                         class="icon-list d-flex align-self-center">
                                    <div class="media-body d-flex justify-content-between align-self-center">
                                        <a href="${pageContext.request.contextPath}/accountPage?id=${groupRelationJoinRequestAccount.account.id}">
                                                ${groupRelationJoinRequestAccount.account}</a>
                                        <a href="${pageContext.request.contextPath}/acceptAccountToGroup?groupId=${targetGroup.id}&id=${groupRelationJoinRequestAccount.account.id}"
                                           class="btn btn-sm btn-work accept-account-to-group">Принять в группу</a>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <div class="preloader d-flex justify-content-center"></div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="targetGroupId" value="${targetGroup.id}">
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
