<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vgoncharenko
  Date: 05.03.19
  Time: 10:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Поиск</title>
    <link href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/sockjs-client/1.1.2/sockjs.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/generateElements.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/messageWs.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/buttonActions.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/chainForInputFileClick.js"></script>
</head>
<body>
<nav class="navbar navbar-dark nav-bg fixed-top">
    <a class="navbar-brand text-shadow"
       href="${pageContext.request.contextPath}/accountPage?id=<sec:authentication property="principal.id"/>">
        <sec:authentication property="principal"/></a>
    <form action="${pageContext.request.contextPath}/search" method="get" class="search-form">
        <div class="input-group">
            <input id="searchLine" class="form-control form-control-work bg-light" name="searchName" type="text"
                   placeholder="Поиск">
            <div class="input-group-append">
                <button class="btn btn-sm btn-search-panel" type="submit">Найти</button>
            </div>
        </div>
    </form>
    <a href="${pageContext.request.contextPath}/logout" class="btn btn-sm btn-search-panel">Выйти</a>
</nav>
</body>
</html>
