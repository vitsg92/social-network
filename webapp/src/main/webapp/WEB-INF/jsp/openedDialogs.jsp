<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Виталий Гончаренко
  Date: 25.05.2019
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/page.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/openedDialogs.css" rel="stylesheet">
    <title>Dialogs</title>
</head>
<body>
<%@include file="searchForm.jsp" %>
<input type="hidden" id="startMessageIdPagination" value="${dialogs.size() == 0 ? 0 : dialogs.get(0).lastMessageInDialog.id}">
<sec:authentication var="mainAccountId" property="principal.id"/>
<div class="container">
    <div class="main-bg">
        <div class="row">
            <%@include file="menu.jsp" %>
            <div class="col-7">
                <c:if test="${empty requestScope.dialogs}">
                    <h6 class="mt-3">Диалогов не найдено</h6>
                </c:if>
                <c:if test="${not empty requestScope.dialogs}">
                    <h6 class="mt-3">Найденные диалоги</h6>
                </c:if>
                <div id="dialogs" class="list-group mt-2">
                    <c:forEach var="dialog" items="${requestScope.dialogs}">
                        <a id="${dialog.companion.id}"
                           href="${pageContext.request.contextPath}/chat?id=${dialog.companion.id}"
                           class="list-group-item list-group-item-action shadow dialog">
                            <div class="media m-ng-2">
                                <img src="${pageContext.request.contextPath}/accountPhoto?id=${dialog.companion.id}"
                                     class="icon-dialog d-flex align-self-center">
                                <div class="media-body">
                                    <div class="dialog-head d-flex justify-content-between align-items-center">
                                        <h6>${dialog.companion}</h6>
                                        <c:if test="${dialog.countUnacceptedMessages != 0}">
                                            <span class="badge badge-info">${dialog.countUnacceptedMessages}</span>
                                        </c:if>
                                    </div>
                                    <c:if test="${dialog.lastMessageInDialog.writer.id == mainAccountId}">
                                        <div>
                                            <span class="message-content">
                                                Вы: ${dialog.lastMessageInDialog.messageContent}</span>
                                        </div>
                                    </c:if>
                                    <c:if test="${dialog.lastMessageInDialog.writer.id != mainAccountId}">
                                        <div>
                                            <span class="message-content">${dialog.lastMessageInDialog.writer.firstName}: ${dialog.lastMessageInDialog.messageContent}</span>
                                        </div>
                                    </c:if>
                                    <div class="d-flex justify-content-end">
                                        <span class="dialog-last-message-time">${dialog.lastMessageInDialog.dateOfWriting}</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </div>
                <div class="preloader d-flex justify-content-center"></div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resources/js/singlePageAndPagination.js"></script>
</body>
</html>
