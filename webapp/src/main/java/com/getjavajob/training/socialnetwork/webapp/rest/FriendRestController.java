package com.getjavajob.training.socialnetwork.webapp.rest;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.ralation.FriendRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.service.FriendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.getCurrentAccount;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

@RestController
public class FriendRestController {
    private static final Logger logger = LoggerFactory.getLogger(FriendRestController.class);
    private final FriendService friendService;

    public FriendRestController(FriendService friendService) {
        this.friendService = friendService;
    }

    @RequestMapping(value = "/addFriend", method = RequestMethod.GET)
    public RelationStatus addFriend(@RequestParam("id") int id, HttpServletResponse response) {
        Account mainAccount = requireNonNull(getCurrentAccount());
        try {
            logger.debug("Запрос на добавления друга от аккаунта с id = {} к id = {}", mainAccount.getId(), id);
            RelationStatus relationStatus = friendService.addFriend(mainAccount.getId(), id);
            logger.debug("Добавление друга прошло успешно");
            return relationStatus;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при добавлении друга", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/removeFriend", method = RequestMethod.GET)
    public RelationStatus removeFriend(int id, HttpServletResponse response) {
        Account mainAccount = requireNonNull(getCurrentAccount());
        try {
            logger.debug("Запрос на удаление друга от аккаунта с id = {} к id = {}", mainAccount.getId(), id);
            RelationStatus relationStatus = friendService.removeFriend(mainAccount.getId(), id);
            logger.debug("Добавление друга прошло успешно");
            return relationStatus;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при удалении друга", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/friendsPagination")
    public List<FriendRelation> getFriendsPagination(int id, @RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            if (isNull(lastElementId)) {
                return friendService.getAccountFriends(id);
            } else {
                return friendService.getAccountFriends(id, lastElementId);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к друзьям аккаунта", e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/requestOutFriendshipAccountsPagination")
    public List<FriendRelation> getRequestOutFriendshipAccountsPagination(@RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            logger.debug("Доступ к исходящим заявкам аккаунта в друзья, id {}", mainAccount.getId());
            if (isNull(lastElementId)) {
                return friendService.getRequestOutFriendshipAccounts(mainAccount.getId());
            } else {
                return friendService.getRequestOutFriendshipAccounts(mainAccount.getId(), lastElementId);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к исходящим заявкам аккаунта в друзья", e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/requestInFriendshipAccountsPagination")
    public List<FriendRelation> getRequestInFriendshipAccountsPagination(@RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            logger.debug("Доступ к входящим заявкам аккаунта в друзья, id {}", mainAccount.getId());
            if (isNull(lastElementId)) {
                return friendService.getRequestInFriendshipAccounts(mainAccount.getId());
            } else {
                return friendService.getRequestInFriendshipAccounts(mainAccount.getId(), lastElementId);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к входящим заявкам аккаунта в друзья", e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }
}
