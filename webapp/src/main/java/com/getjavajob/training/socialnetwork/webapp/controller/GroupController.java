package com.getjavajob.training.socialnetwork.webapp.controller;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import com.getjavajob.training.socialnetwork.common.ralation.GroupPrivilege;
import com.getjavajob.training.socialnetwork.common.ralation.GroupRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.service.GroupService;
import com.getjavajob.training.socialnetwork.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import java.util.Map;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.*;
import static java.util.Objects.requireNonNull;

@Controller
public class GroupController {
    private static final String ERROR_MSG_ACCESS_DENIED = "Доступ запрещен";
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private final GroupService groupService;
    private final MessageService messageService;

    @Autowired
    public GroupController(GroupService groupService, MessageService messageService) {
        this.groupService = groupService;
        this.messageService = messageService;
    }

    @RequestMapping(value = "/groupPage", method = RequestMethod.GET)
    public String getGroupPage(@RequestParam("groupId") int groupId, Model model) {
        Account mainAccount = requireNonNull(getCurrentAccount());
        try {
            logger.debug("Запрос страницы группы c groupId = {}, запрашивает аккаунт с id = {}", groupId, mainAccount.getId());
            Group group = groupService.getGroupById(groupId);
            model.addAttribute("targetGroup", group);
            model.addAttribute("groupRelation", groupService.getGroupRelation(mainAccount.getId(), groupId));
            model.addAttribute("messagesOnGroupWall", messageService.getMessageOnWall(groupId, MessageType.GROUP_WALL));
            return "groupPage";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка доступа к группе", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.GET)
    public String createGroup(Model model) {
        model.addAttribute("createdGroup", new Group());
        return "createGroup";
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST)
    public String createGroup(@ModelAttribute("createdGroup") Group createdGroup, Model model) {
        Account mainAccount = requireNonNull(getCurrentAccount());
        try {
            logger.debug("Запрос на создание группы");
            createdGroup.setCreator(mainAccount);
            model.addAttribute("createdGroup", createdGroup);
            Map<String, String> errors = groupService.groupValidate(createdGroup);
            if (!errors.isEmpty()) {
                logger.debug("Группа не прошла валидацию");
                model.addAttribute("errors", errors);
                return "createGroup";
            } else {
                logger.debug("Группа прошла валидацию");
                int groupId = groupService.addGroup(mainAccount, createdGroup).getId();
                logger.debug("Создана группа с groupId = {}", groupId);
                return "redirect: /groupPage?groupId=" + groupId;
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при создании группы", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.GET)
    public String updateGroup(@RequestParam("groupId") int groupId, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка доступа к группе {}", groupId);
                model.addAttribute("error", ERROR_MSG_ACCESS_DENIED);
                return "error";
            }
            logger.debug("Запрос на обновление группы {}", groupId);
            model.addAttribute("updatedGroup", groupService.getGroupById(groupId));
            return "updateGroup";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка доступа к группе", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.POST)
    public String updateGroup(@ModelAttribute("updatedGroup") Group updatedGroup, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            int updatedGroupId = updatedGroup.getId();
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), updatedGroupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при обновлении группы {}", updatedGroupId);
                model.addAttribute("error", ERROR_MSG_ACCESS_DENIED);
                return "error";
            }
            logger.debug("Запрос на обновление группы с groupId = {}, обновляет аккаунт с id = {}", updatedGroupId, mainAccount.getId());
            groupService.setMissingFieldsIntoUpdatedGroup(updatedGroup, groupService.getGroupById(updatedGroupId));
            model.addAttribute("updatedGroup", updatedGroup);
            Map<String, String> errors = groupService.groupValidate(updatedGroup);
            if (errors.isEmpty()) {
                logger.debug("Группа прошла валидацию");
                groupService.updateGroup(updatedGroup);
                logger.debug("Группа с groupId = {} обновлена", updatedGroupId);
                return "redirect: /groupPage?groupId=" + updatedGroupId;
            } else {
                logger.debug("Группа не прошла валидацию");
                model.addAttribute("errors", errors);
                return "updateGroup";
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при обновлении группы", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/groups")
    public String getGroups(int id, Model model) {
        try {
            logger.debug("Запрос групп аккаунта с id = {}", id);
            model.addAttribute("groupRelations", groupService.getAccountGroups(id, RelationStatus.FULL));
            model.addAttribute("targetAccountId", id);
            return "accountGroupsPage";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к группа аккаунта", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/removeGroup")
    public String removeGroup(int groupId, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при удалении группы {}", groupId);
                model.addAttribute("error", ERROR_MSG_ACCESS_DENIED);
                return "error";
            }
            groupService.removeGroup(groupId);
            return "redirect: /accountPage?id=" + mainAccount.getId();
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при удалении группы", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/groupMembers", method = RequestMethod.GET)
    public String getGroupMembers(int groupId, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkUserPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при доступе к участникам группы {}", groupId);
                model.addAttribute("error", ERROR_MSG_ACCESS_DENIED);
                return "error";
            }
            model.addAttribute("groupRelation", groupRelation);
            model.addAttribute("groupRelationMembers", groupService.getGroupMembers(groupId));
            model.addAttribute("targetGroup", groupService.getGroupById(groupId));
            return "groupMembers";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к участникам группы", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/groupJoinRequests", method = RequestMethod.GET)
    public String getGroupJoinRequests(int groupId, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при доступе к заявкам в группу {}", groupId);
                model.addAttribute("error", ERROR_MSG_ACCESS_DENIED);
                return "error";
            }
            model.addAttribute("groupRelationJoinRequestAccounts", groupService.getJoinRequestAccounts(groupId));
            model.addAttribute("targetGroup", groupService.getGroupById(groupId));
            return "groupJoinRequest";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к заявкам в группу", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/groupModerators", method = RequestMethod.GET)
    public String getGroupModerators(int groupId, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при доступе к модераторам группы {}", groupId);
                model.addAttribute("error", ERROR_MSG_ACCESS_DENIED);
                return "error";
            }
            model.addAttribute("groupRelationModerators", groupService.getGroupMembersOnPrivilege(groupId, GroupPrivilege.MODER));
            model.addAttribute("targetGroup", groupService.getGroupById(groupId));
            return "groupModerators";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к модераторам группы", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }
}
