package com.getjavajob.training.socialnetwork.webapp.rest;

import com.getjavajob.training.socialnetwork.common.AccountRole;
import com.getjavajob.training.socialnetwork.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.getDefaultImage;
import static java.util.Objects.isNull;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

@RestController
public class AccountRestController {
    private static final Logger logger = LoggerFactory.getLogger(AccountRestController.class);

    private final AccountService accountService;

    public AccountRestController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/accountPhoto", method = RequestMethod.GET)
    public byte[] getAccountPhoto(@RequestParam("id") int id, HttpServletResponse response) {
        try {
            logger.debug("Запрос фото аккаунта с id = {}", id);
            byte[] photo = accountService.getAccountPhoto(id);
            if (isNull(photo) || photo.length == 0) {
                return getDefaultImage();
            } else {
                return photo;
            }
        } catch (IOException | RuntimeException e) {
            logger.error("Произошла ошибка доступа к фото аккаунта", e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/makeAdmin")
    public AccountRole makeAdmin(int id, HttpServletResponse response) {
        try {
            logger.debug("Добавление администратора id {}", id);
            return accountService.makeAdmin(id);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка добавления администратора", e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }
}
