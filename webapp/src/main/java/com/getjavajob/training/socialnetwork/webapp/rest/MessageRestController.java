package com.getjavajob.training.socialnetwork.webapp.rest;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.dto.UserDialogDetail;
import com.getjavajob.training.socialnetwork.common.message.Message;
import com.getjavajob.training.socialnetwork.common.message.MessageOnWall;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;
import com.getjavajob.training.socialnetwork.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.getCurrentAccount;
import static java.util.Objects.requireNonNull;

@RestController
public class MessageRestController {
    private static final Logger logger = LoggerFactory.getLogger(MessageRestController.class);
    private final MessageService messageService;

    public MessageRestController(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping(value = "/getChatMessagesPagination")
    public List<PrivateMessage> getChatMessagesPagination(int id, int lastElementId, HttpServletResponse response) {
        int mainAccountId = requireNonNull(getCurrentAccount()).getId();
        try {
            logger.debug("Загрузка сообщений в чате аккаунта {} с аккаунтом {}", mainAccountId, id);
            return messageService.getMessagesFromDialog(mainAccountId, id, lastElementId);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при загрузке сообщений аккаунта {}", mainAccountId, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/getMessagesOnWallPagination")
    public List<MessageOnWall> getMessagesOnWallPagination(int id, MessageType messageType, int lastElementId, HttpServletResponse response) {
        try {
            logger.debug("Загрузка сообщений на стене {} id {}", messageType, id);
            return messageService.getMessageOnWall(id, messageType, lastElementId);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при загрузке сообщений на стене {} id {}", messageType, id, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/acceptMessage")
    public Message acceptMessage(int id, HttpServletResponse response) {
        try {
            logger.debug("Чтение сообщения {}", id);
            return messageService.acceptMessage(id);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при обновлении сообщения {}", id, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/addMessageToUnaccepted", method = RequestMethod.POST)
    public Message addMessageToUnaccepted(@RequestBody PrivateMessage message) {
        Account mainAccount = requireNonNull(getCurrentAccount());
        logger.debug("Добавление сообщения {} в список непрочитанных к аккаунту {}", message.getId(), mainAccount.getId());
        mainAccount.getUnacceptedMessages().add(message);
        return message;
    }

    @RequestMapping(value = "/openedDialogsPagination")
    public List<UserDialogDetail> getOpenDialogsPagination(int lastElementId, HttpServletResponse response) {
        Account mainAccount = requireNonNull(getCurrentAccount());
        int mainAccountId = mainAccount.getId();
        try {
            logger.debug("Загрузка диалогов аккаунта {}", mainAccountId);
            return messageService.getUserDialogDetails(mainAccount, lastElementId);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при обновлении сообщения {}", mainAccountId, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }
}
