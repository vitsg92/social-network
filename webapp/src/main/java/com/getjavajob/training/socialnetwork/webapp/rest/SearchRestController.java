package com.getjavajob.training.socialnetwork.webapp.rest;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

@RestController
public class SearchRestController {
    private static final String ERROR_MSG_SEARCH_ACCOUNT = "Произошла ошибка при поиске аккаунта";
    private static final String ERROR_MSG_SEARCH_GROUP = "Произошла ошибка при поиске группы";
    private static final Logger logger = LoggerFactory.getLogger(SearchRestController.class);
    private final AccountService accountService;
    private final GroupService groupService;

    public SearchRestController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @RequestMapping(value = "/searchAccountsAjax")
    public List<Account> searchAccountsAjax(String searchName, HttpServletResponse response) {
        try {
            logger.debug("Autocomplete аккаунтов, содержащих строку = {}", searchName);
            return accountService.searchAccountByString(searchName);
        } catch (RuntimeException e) {
            logger.error(ERROR_MSG_SEARCH_ACCOUNT, e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/searchGroupsAjax")
    public List<Group> searchGroupAjax(String searchName, HttpServletResponse response) {
        try {
            logger.debug("Autocomplete групп, содержащих строку = {}", searchName);
            return groupService.searchGroupByString(searchName);
        } catch (RuntimeException e) {
            logger.error(ERROR_MSG_SEARCH_GROUP, e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/searchAccountsPagination")
    public List<Account> searchAccountPagination(String searchName, HttpServletResponse response, int pageNumber, int pageSize) {
        try {
            logger.debug("Поиск аккаунтов, содержащих строку = {}", searchName);
            return accountService.searchAccountByStringPagination(searchName, pageNumber, pageSize);
        } catch (RuntimeException e) {
            logger.error(ERROR_MSG_SEARCH_ACCOUNT, e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/searchGroupsPagination")
    public List<Group> searchGroupPagination(String searchName, HttpServletResponse response, int pageNumber, int pageSize) {
        try {
            logger.debug("Поиск групп, содержащих строку = {}", searchName);
            return groupService.searchGroupByStringPagination(searchName, pageNumber, pageSize);
        } catch (RuntimeException e) {
            logger.error(ERROR_MSG_SEARCH_GROUP, e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }
}
