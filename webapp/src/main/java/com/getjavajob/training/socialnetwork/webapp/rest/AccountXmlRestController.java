package com.getjavajob.training.socialnetwork.webapp.rest;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

@RestController
public class AccountXmlRestController {
    private static final Logger logger = LoggerFactory.getLogger(AccountXmlRestController.class);
    private final AccountService accountService;

    public AccountXmlRestController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PreAuthorize("hasRole('ADMIN') or authentication.principal.id == #id")
    @RequestMapping(value = "/downloadAccountXml", method = RequestMethod.GET)
    public Account downloadAccountXml(int id, HttpServletResponse response) {
        try {
            logger.debug("Запрос на выгрузку XML аккаунта с id = {}", id);
            response.setHeader("Content-Disposition", "attachment; filename=account_id" + id);
            return accountService.getAccountById(id);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка доступа к аккаунту", e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }
}
