package com.getjavajob.training.socialnetwork.webapp.config;

import com.getjavajob.training.socialnetwork.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final AccountService accountService;

    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public WebSecurityConfig(AccountService accountService, BCryptPasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/login", "/registration", "/resources/**", "/webjars/**").permitAll()
                .antMatchers("/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN");
        http.formLogin().loginPage("/login").usernameParameter("email").failureUrl("/login?error=true");
        http.csrf().disable();
        http.rememberMe().rememberMeParameter("rememberMe").tokenValiditySeconds(86400).key("socialNetwork")
                .rememberMeCookieName("socialNetwork");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountService::authorization).passwordEncoder(passwordEncoder);
    }

}
