package com.getjavajob.training.socialnetwork.webapp.controller;

import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.FriendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@SessionAttributes("mainAccount")
@Controller
public class FriendController {
    private static final Logger logger = LoggerFactory.getLogger(FriendController.class);
    private final AccountService accountService;
    private final FriendService friendService;

    @Autowired
    public FriendController(AccountService accountService, FriendService friendService) {
        this.accountService = accountService;
        this.friendService = friendService;
    }

    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public String showFriends(int id, Model model) {
        try {
            logger.debug("Запрос на просмотр друзей аккаунта с id = {}", id);
            model.addAttribute("friendRelations", friendService.getAccountFriends(id));
            model.addAttribute("targetAccount", accountService.getAccountById(id));
            return "friendsPage";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к друзьям аккаунта", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}
