package com.getjavajob.training.socialnetwork.webapp.utils;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRole;
import com.getjavajob.training.socialnetwork.common.ralation.GroupPrivilege;
import com.getjavajob.training.socialnetwork.common.ralation.GroupRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.service.AccountService;
import org.apache.commons.io.IOUtils;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.io.InputStream;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

public class Utils {
    public static Account getCurrentAccount() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth instanceof AnonymousAuthenticationToken ? null : (Account) auth.getPrincipal();
    }

    public static void updateCurrentAccountInSession(Account account) {
        Authentication currentAuth = SecurityContextHolder.getContext().getAuthentication();
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(account, currentAuth.getCredentials(),
                account.getAuthorities());
        auth.setDetails(currentAuth.getDetails());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    public static byte[] getDefaultImage() throws IOException {
        InputStream is = AccountService.class.getClassLoader().getResourceAsStream("defaultImage.jpg");
        return IOUtils.toByteArray(requireNonNull(is));
    }

    public static boolean checkModerPrivilege(GroupRelation groupRelation, Account mainAccount) {
        if (nonNull(groupRelation) && groupRelation.getGroupPrivilege().equals(GroupPrivilege.MODER)) {
            return true;
        } else {
            return mainAccount.getRole().equals(AccountRole.ADMIN);
        }
    }

    public static boolean checkUserPrivilege(GroupRelation groupRelation, Account mainAccount) {
        if (nonNull(groupRelation) && groupRelation.getRelationStatus().equals(RelationStatus.FULL)) {
            return true;
        } else {
            return mainAccount.getRole().equals(AccountRole.ADMIN);
        }
    }
}
