package com.getjavajob.training.socialnetwork.webapp.controller;

import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchController {
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private AccountService accountService;
    private GroupService groupService;

    @Autowired
    public SearchController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @RequestMapping(value = "/search")
    public String getSearchElements(@RequestParam("searchName") String searchName, Model model) {
        try {
            logger.debug("Поиск аккаунтов и групп, содержащих строку = {}", searchName);
            model.addAttribute("searchName", searchName);
            model.addAttribute("countAccounts", accountService.getCountAccountsWithSearchByString(searchName));
            model.addAttribute("countGroups", groupService.getCountGroupsWithSearchByString(searchName));
            return "foundElements";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при поиске", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}
