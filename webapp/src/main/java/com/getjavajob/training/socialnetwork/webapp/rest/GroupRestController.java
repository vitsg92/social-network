package com.getjavajob.training.socialnetwork.webapp.rest;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.ralation.GroupPrivilege;
import com.getjavajob.training.socialnetwork.common.ralation.GroupRelation;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.*;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

@RestController
public class GroupRestController {
    private static final Logger logger = LoggerFactory.getLogger(GroupRestController.class);
    private final GroupService groupService;

    public GroupRestController(GroupService groupService) {
        this.groupService = groupService;
    }

    @RequestMapping(value = "/joinOnGroup", method = RequestMethod.GET)
    public RelationStatus joinOnGroup(int groupId, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            logger.debug("Запрос на вступление в группу от аккаунта с id = {} к groupId = {}", mainAccount.getId(), groupId);
            RelationStatus relationStatus = groupService.joinOnGroup(mainAccount.getId(), groupId);
            logger.debug("Заявка на вступление выслана");
            return relationStatus;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при вступлении в группу", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/outOfGroup", method = RequestMethod.GET)
    public RelationStatus outOfGroup(int groupId, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            logger.debug("Запрос на выход из группы от аккаунта с id = {} к groupId = {}", mainAccount.getId(), groupId);
            RelationStatus relationStatus = groupService.outOfGroup(mainAccount.getId(), groupId);
            logger.debug("Выход из группы успешен");
            return relationStatus;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при выходе из группы", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/acceptAccountToGroup", method = RequestMethod.GET)
    public RelationStatus acceptAccountToGroup(int groupId, @RequestParam("id") int acceptedAccountId, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при добавлении аккаунта в группу {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            logger.debug("Добавление аккаунта с id = {} в группу groupId = {}", mainAccount.getId(), groupId);
            RelationStatus relationStatus = groupService.acceptAccountToGroup(groupId, acceptedAccountId);
            logger.debug("Добавление аккаунта в группу успешно");
            return relationStatus;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при добавлении аккаунта в группу", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/removeAccountFromGroup", method = RequestMethod.GET)
    public RelationStatus removeAccountFromGroup(int groupId, @RequestParam("id") int removedAccountId, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при удалении аккаунта из группы {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            logger.debug("Удаление аккаунта с id = {} из группы groupId = {}", mainAccount.getId(), groupId);
            RelationStatus relationStatus = groupService.removeAccountFromGroup(groupId, removedAccountId);
            logger.debug("Удаление аккаунта из группы успешно");
            return relationStatus;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при удалении аккаунта из группы", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/removeGroupModerator")
    public GroupPrivilege removeGroupModerator(int groupId, int id, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при удалении модератора {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            logger.debug("Запрос на удаление модератора с id = {} из группы = {}", id, groupId);
            GroupPrivilege groupPrivilege = groupService.removeModerator(groupId, id);
            logger.debug("Удаление модератора прошло успешно");
            return groupPrivilege;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при удалении модератора", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/addGroupModerator")
    public GroupPrivilege addGroupModerator(int groupId, int id, HttpServletResponse response) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при добавлении модератора {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            logger.debug("Запрос на добавление модератора с id = {} в группе = {}", id, groupId);
            GroupPrivilege groupPrivilege = groupService.addModerator(groupId, id);
            logger.debug("Добавление модератора прошло успешно");
            return groupPrivilege;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при добавлении модератора", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/groupsPagination")
    public List<GroupRelation> groupsPagination(int id, @RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            logger.debug("Доступ к группам аккаунта {}", id);
            if (isNull(lastElementId)) {
                return groupService.getAccountGroups(id, RelationStatus.FULL);
            } else {
                return groupService.getAccountGroups(id, RelationStatus.FULL, lastElementId);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к группам аккаунта", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/requestGroupsPagination")
    public List<GroupRelation> requestGroupsPagination(@RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            int mainAccountId = requireNonNull(getCurrentAccount()).getId();
            logger.debug("Доступ к исходящим заявкам в группы аккаунта {}", mainAccountId);
            if (isNull(lastElementId)) {
                return groupService.getAccountGroupsRequests(mainAccountId, RelationStatus.REQUEST_OUT);
            } else {
                return groupService.getAccountGroupsRequests(mainAccountId, RelationStatus.REQUEST_OUT, lastElementId);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к исходящим заявкам в группы аккаунта", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/createdGroupsPagination")
    public List<Group> createdGroupsPagination(@RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            int mainAccountId = requireNonNull(getCurrentAccount()).getId();
            logger.debug("Доступ к созданным группам аккаунта {}", mainAccountId);
            if (isNull(lastElementId)) {
                return groupService.getCreatedGroups(mainAccountId);
            } else {
                return groupService.getCreatedGroups(mainAccountId, lastElementId);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к созданным группам аккаунта", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/groupModeratorsPagination", method = RequestMethod.GET)
    public List<GroupRelation> getGroupModeratorsPagination(int groupId, @RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            logger.debug("Доступ к модераторам группы {}", groupId);
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при доступе к модераторам группы {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            if (isNull(lastElementId)) {
                return groupService.getGroupMembersOnPrivilege(groupId, GroupPrivilege.MODER);
            } else {
                return groupService.getGroupMembersOnPrivilege(groupId, lastElementId, GroupPrivilege.MODER);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к модераторам группы {}", groupId, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/groupUsersPagination", method = RequestMethod.GET)
    public List<GroupRelation> getGroupUsersPagination(int groupId, @RequestParam(required = false) Integer lastElementId, HttpServletResponse response) {
        try {
            logger.debug("Доступ к юзерам группы {}", groupId);
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при доступе к юзерам группы {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            if (isNull(lastElementId)) {
                return groupService.getGroupMembersOnPrivilege(groupId, GroupPrivilege.USER);
            } else {
                return groupService.getGroupMembersOnPrivilege(groupId, lastElementId, GroupPrivilege.USER);
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к юзерам группы {}", groupId, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/groupMembersPagination", method = RequestMethod.GET)
    public List<GroupRelation> getGroupMembersPagination(int groupId, int lastElementId, HttpServletResponse response) {
        try {
            logger.debug("Доступ к участникам группы {}", groupId);
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkUserPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при доступе к участникам группы {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            return groupService.getGroupMembers(groupId, lastElementId);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к участникам группы {}", groupId, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/groupJoinRequestsPagination", method = RequestMethod.GET)
    public List<GroupRelation> getGroupJoinRequestsPagination(int groupId, int lastElementId, HttpServletResponse response) {
        try {
            logger.debug("Доступ к заявкам на вступление в группу {}", groupId);
            Account mainAccount = requireNonNull(getCurrentAccount());
            GroupRelation groupRelation = groupService.getGroupRelation(mainAccount.getId(), groupId);
            if (!checkModerPrivilege(groupRelation, mainAccount)) {
                logger.error("Произошла ошибка при доступе к заявкам на вступление в группу {}", groupId);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return null;
            }
            return groupService.getJoinRequestAccounts(groupId, lastElementId);
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к заявкам на вступление в группу {}", groupId, e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    @RequestMapping(value = "/groupPhoto", method = RequestMethod.GET)
    public byte[] getAccountPhoto(@RequestParam("groupId") int id, HttpServletResponse response) {
        try {
            logger.debug("Запрос фото группы с groupId = {}", id);
            byte[] groupPhoto = groupService.getGroupPhoto(id);
            if (isNull(groupPhoto) || groupPhoto.length == 0) {
                return getDefaultImage();
            } else {
                return groupPhoto;
            }
        } catch (IOException | RuntimeException e) {
            logger.error("Произошла ошибка доступа к фото аккаунта", e);
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            return null;
        }
    }
}
