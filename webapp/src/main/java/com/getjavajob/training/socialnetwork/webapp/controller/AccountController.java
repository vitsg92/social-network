package com.getjavajob.training.socialnetwork.webapp.controller;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.AccountRole;
import com.getjavajob.training.socialnetwork.common.message.MessageType;
import com.getjavajob.training.socialnetwork.common.ralation.RelationStatus;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.FriendService;
import com.getjavajob.training.socialnetwork.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import java.util.Map;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.getCurrentAccount;
import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.updateCurrentAccountInSession;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;
    private final MessageService messageService;
    private final FriendService friendService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JmsTemplate jmsTemplate;

    @Autowired
    public AccountController(AccountService accountService, BCryptPasswordEncoder passwordEncoder, MessageService messageService,
                             FriendService friendService, JmsTemplate jmsTemplate) {
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
        this.messageService = messageService;
        this.friendService = friendService;
        this.jmsTemplate = jmsTemplate;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "authorization";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("createdAccount", new Account());
        return "registrationAccount";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute Account createdAccount, Model model) {
        try {
            logger.debug("Запрос регистрации аккаунта");
            createdAccount.setPassword(passwordEncoder.encode(createdAccount.getPassword()));
            model.addAttribute("createdAccount", createdAccount);
            Map<String, String> errors = accountService.registrationAccountValidate(createdAccount);
            String nextPage;
            if (errors.isEmpty()) {
                logger.debug("Аккаунт прошел валидацию");
                createdAccount.setRole(AccountRole.USER);
                Account mainAccount = accountService.createAccount(createdAccount);
                int mainAccountId = mainAccount.getId();
                try {
                    jmsTemplate.convertAndSend("email", "Создан новый аккаунт с id = " + mainAccountId);
                } catch (RuntimeException e) {
                    logger.error("Не удалось отправить jms", e);
                }
                logger.debug("Аккаунт зарегистрирован, id = {}", mainAccountId);
                model.addAttribute("SPRING_SECURITY_LAST_EXCEPTION", "");
                nextPage = "redirect: /login";
            } else {
                logger.debug("Аккаунт не прошел валидацию");
                model.addAttribute("errors", errors);
                nextPage = "registrationAccount";
            }
            return nextPage;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при регистрации аккаунта", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/accountPage", method = RequestMethod.GET)
    public String getAccountPage(@RequestParam("id") int id, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            int mainAccountId = mainAccount.getId();
            logger.debug("Запрос страницы аккаунта c id = {}, запрашивает аккаунт с id + {}", id, mainAccountId);
            Account targetAccount = accountService.getAccountById(id);
            if (isNull(targetAccount)) {
                logger.error("Произошла ошибка доступа к аккаунту");
                model.addAttribute("error", "Нет такого аккаунта");
                return "error";
            }
            if (id != mainAccountId) {
                RelationStatus relationStatus = friendService.getFriendRelationStatus(mainAccount.getId(), id);
                model.addAttribute("relationStatus", relationStatus);
            }
            model.addAttribute("targetAccount", targetAccount);
            model.addAttribute("messagesOnWall", messageService.getMessageOnWall(targetAccount.getId(), MessageType.ACCOUNT_WALL));
            return "accountPage";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка доступа к аккаунту", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ADMIN') or authentication.principal.id == #id")
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String update(@RequestParam("id") int id, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            logger.debug("Запрос на обновление аккаунта id = {} с аккаунта {}", id, mainAccount.getId());
            model.addAttribute("updatedAccount", accountService.getAccountById(id));
            return "updateAccount";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка доступа к аккаунту", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ADMIN') or authentication.principal.id == #updatedAccount.id")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@ModelAttribute("updatedAccount") Account updatedAccount, Model model) {
        try {
            int updatedAccountId = updatedAccount.getId();
            logger.debug("Запрос на обновление аккаунта id = {}", updatedAccountId);
            Account mainAccount = requireNonNull(getCurrentAccount());
            Map<String, String> errors = accountService.updateAccountValidate(updatedAccount);
            String nextPage;
            if (!errors.isEmpty()) {
                logger.debug("Аккаунт не прошел валидацию");
                model.addAttribute("updatedAccount", updatedAccount);
                model.addAttribute("errors", errors);
                nextPage = "updateAccount";
            } else {
                logger.debug("Аккаунт прошел валидацию");
                accountService.setMissingFieldsIntoUpdatedAccount(updatedAccount, accountService.getAccountById(updatedAccountId));
                Account resultAccount = accountService.updateAccount(updatedAccount);
                logger.debug("Обновление аккаунта с id = {} прошло успешно", updatedAccountId);
                if (resultAccount.getId() == mainAccount.getId()) {
                    messageService.loadUnacceptedMessages(resultAccount);
                    updateCurrentAccountInSession(resultAccount);
                    logger.debug("Обновленный аккаунт принадлежит пользователю. Обневлен в контексте");
                }
                nextPage = "redirect: /accountPage?id=" + updatedAccountId;
            }
            return nextPage;
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при обновлении аккаунта", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PreAuthorize("hasRole('ADMIN') or authentication.principal.id == #id")
    @RequestMapping(value = "/removeAccount")
    public String removeAccount(int id, Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            logger.debug("Запрос на удаление аккаунта id = {}", mainAccount.getId());
            accountService.removeAccount(id);
            logger.debug("Аккаунт удален");
            if (mainAccount.getId() == id) {
                return "redirect: /logout";
            } else {
                return "redirect: /accountPage?id=" + mainAccount.getId();
            }
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при удалении аккаунта", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/")
    public String getStartPage() {
        Account mainAccount = requireNonNull(getCurrentAccount());
        return "redirect: /accountPage?id=" + mainAccount.getId();
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }
}
