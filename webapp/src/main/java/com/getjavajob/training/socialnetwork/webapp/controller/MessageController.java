package com.getjavajob.training.socialnetwork.webapp.controller;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.message.MessageOnWall;
import com.getjavajob.training.socialnetwork.common.message.PrivateMessage;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.getCurrentAccount;
import static java.util.Objects.requireNonNull;

@Controller
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final MessageService messageService;
    private final AccountService accountService;

    @Autowired
    public MessageController(SimpMessagingTemplate simpMessagingTemplate, MessageService messageService, AccountService accountService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.messageService = messageService;
        this.accountService = accountService;
    }

    @MessageMapping("/sendMessage")
    public void sendMessage(PrivateMessage message) {
        message.setAccepted(false);
        simpMessagingTemplate.convertAndSend("/queue/privateMessages/" + message.getReader().getId(), messageService.save(message));
    }

    @MessageMapping("/sendMessageOnAccountWall")
    public void sendMessageOnAccountWall(MessageOnWall message) {
        simpMessagingTemplate.convertAndSend("/queue/accountMessagesOnWall/" + message.getDestinationId(), messageService.save(message));
    }

    @MessageMapping("/sendMessageOnGroupWall")
    public void sendMessageOnGroupWall(MessageOnWall message) {
        simpMessagingTemplate.convertAndSend("/queue/groupMessagesOnWall/" + message.getDestinationId(), messageService.save(message));
    }

    @RequestMapping(value = "/openedDialogs")
    public String getOpenedDialogs(Model model) {
        try {
            Account mainAccount = requireNonNull(getCurrentAccount());
            logger.debug("Доступ к диалогам аккаунта {}", mainAccount.getId());
            model.addAttribute("dialogs", messageService.getUserDialogDetails(mainAccount));
            logger.debug("Диалоги аккаунта получены");
            return "openedDialogs";
        } catch (RuntimeException e) {
            logger.error("Произошла ошибка при доступе к диалогам аккаунта", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @RequestMapping(value = "/chat", method = RequestMethod.GET)
    public String getChat(int id, Model model) {
        Account mainAccount = requireNonNull(getCurrentAccount());
        model.addAttribute("companion", accountService.getAccountById(id));
        List<PrivateMessage> messages = messageService.getMessagesFromDialog(mainAccount.getId(), id);
        model.addAttribute("messages", messages);
        messageService.removeMessagesFromUnaccepted(mainAccount, id);
        return "chatPage";
    }
}
