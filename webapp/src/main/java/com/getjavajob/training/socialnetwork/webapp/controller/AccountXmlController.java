package com.getjavajob.training.socialnetwork.webapp.controller;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.getCurrentAccount;
import static com.getjavajob.training.socialnetwork.webapp.utils.Utils.updateCurrentAccountInSession;
import static java.util.Objects.requireNonNull;

@SessionAttributes("mainAccount")
@Controller
public class AccountXmlController {
    private static final Logger logger = LoggerFactory.getLogger(AccountXmlController.class);
    private final AccountService accountService;
    private final MessageService messageService;
    private final Jaxb2Marshaller marshaller;

    @Autowired
    public AccountXmlController(AccountService accountService, MessageService messageService, Jaxb2Marshaller marshaller) {
        this.accountService = accountService;
        this.messageService = messageService;
        this.marshaller = marshaller;
    }

    @PreAuthorize("hasRole('ADMIN') or authentication.principal.id == #id")
    @RequestMapping(value = "/updateAccountXml", method = RequestMethod.GET)
    public String updateAccountXml(int id, Model model) {
        model.addAttribute("updatedAccountId", id);
        return "updateAccountXml";
    }

    @PreAuthorize("hasRole('ADMIN') or authentication.principal.id == #id")
    @RequestMapping(value = "/updateAccountXml", method = RequestMethod.POST)
    public String updateAccountXml(MultipartFile accountXml, int id, Model model) {
        try {
            logger.debug("Запрос за обновление аккаунта с id = {} через XML файл", id);
            Account updatedAccount = (Account) marshaller.unmarshal(new StreamSource(accountXml.getInputStream()));
            logger.debug("Обьект успешно создан из XML файла");
            Map<String, String> errors = new HashMap<>();
            int updatedAccountId = updatedAccount.getId();
            if (updatedAccountId == 0 || updatedAccountId != id) {
                logger.debug("Id аккаунта в XML = {} не соответствует id владельца = {}", updatedAccountId, id);
                errors.put("errorId", "Incorrect Account Id");
            } else {
                errors.putAll(accountService.updateAccountValidate(updatedAccount));
            }
            if (errors.isEmpty()) {
                logger.debug("Аккаунт прошел валидацию");
                accountService.setMissingFieldsIntoUpdatedAccount(updatedAccount, accountService
                        .getAccountById(updatedAccount.getId()));
                Account mainAccount = requireNonNull(getCurrentAccount());
                Account resultAccount = accountService.updateAccount(updatedAccount);
                logger.debug("Обновление аккаунта с id = {} прошло успешно", updatedAccountId);
                if (id == mainAccount.getId()) {
                    messageService.loadUnacceptedMessages(resultAccount);
                    updateCurrentAccountInSession(resultAccount);
                    logger.debug("Обновленный аккаунт принадлежит пользователю. Обневлен в контексте");
                }
                return "redirect: accountPage?id=" + updatedAccountId;
            } else {
                logger.debug("Аккаунт не прошел валидацию");
                model.addAttribute("updatedAccountId", id);
                model.addAttribute("errorXml", errors);
                return "updateAccountXml";
            }
        } catch (IOException | RuntimeException e) {
            logger.error("Произошла ошибка при обновлении аккаунта из XML файла", e);
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}
